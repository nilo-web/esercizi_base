'user strict'

function getUsers() {
  
    //creo variabile deferred
  var deferred = $.Deferred();

  //la mia promessa
  $.ajax({
    type: "GET",
    datatype:"json",
    url: "https://jsonplaceholder.typicode.com/users"
  })
  .done({
      
      function (responseUsers) {
        consolel.log("promessa mantenuta");
        console.log(responseUsers);
        //la risolve
        deferred.resolve(responseUsers);
      }

    })

    .fail({
      function (response) {
        console.log("errore", response)
      }
    });
  //ritorno oggetto deferred
  return deferred;

}

function getPosts(id) {
  
      //creo variabile deferred
      var  deferred = $.Deferred();

      //la mia promessa
      $.ajax({
        type: "GET",
        datatype:"json",
        url: "https://jsonplaceholder.typicode.com/posts?userId=" +id,
      })
      //se eseguita 
        .done({
          
          function (responsePosts) {
            consolel.log("promessa mantenuta");
            console.log(responsePosts);
            //la risolve
            deferred.resolve(responsePosts);
          }
    
        })
    
        .fail({
          function (response) {
            console.log("errore", response)
          }
    
        })
    
      //ritorno oggetto deferred
      return deferred;

}
