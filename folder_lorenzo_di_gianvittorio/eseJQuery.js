"user strict"

//var httpreq = new XMLHttpRequest();


/*
httpreq.onreadystatechange = function(){
    if(httpreq.readyState === 4 && httpreq.readyStatus ===200){
        console.log(qualcosa, httpreq, httpreq)
            }  
    }


httpreq.open("GET", "https://jsonplaceholder.typicode.com/posts")

httpreq.send()

*/
/*

FACCIO IL GET CON JQUERY

var $lista = $("#mialista");
var array=[];

var chiamata = $.ajax({
    //deve avere un verbo 
    type: "GET",
    //url api
    url: "https://jsonplaceholder.typicode.com/posts"
})
//response parametro implicito
chiamata.done(function(response){
    //ottengo una risposta
    console.log(response)
    var $nuovoItem = $('<li>'+response[0].title+'</li>');
    $lista.append($nuovoItem);
})
//se non funziona il done(?)
chiamata.fail(function (error) {
    console.log("errore", error)
})
*/


/*
//POST CON JS ----> (non funziona, manca $lista.append)

var dati = {
    title:"foo",
    body: "bar",
    userId: 1
};

var httpreq = new XMLHttpRequest();

httpreq.onreadystatechange = function(){
    if(httpreq.readyState === 4  && httpreq.readyStatus===201){
        //quando il readystate = 4 significa che mi ritorna qualcosa
        console.log(123)
            }  
    }
    //è un evento di httpreq
   // con.log(123)
//creo una chiamata http
httpreq.open("POST", "https://jsonplaceholder.typicode.com/posts")

httpreq.setRequestHeader('Content-Type','application/json')
//e devo eseguirla
httpreq.send(JSON.stringify(dati));
*/



/*
//POST CON JQUERY ----> (funziona)
var chiamatainpost = $.ajax(
    {
    type: "POST",
    url: "https://jsonplaceholder.typicode.com/posts",
    dataType: "json",
    //per far diventare una stringa con stringify
    data: JSON.stringify({
        title:"bau",
        body: "bar",
        userId:1
    })
   }
);

chiamatainpost.done(function(response){
    console.log("POST SUC", response)
})

chiamatainpost.fail(function(e){
    console.log("errore", e)
})
*/

/*

ESERCIZIO CHIAMATA 

All'avvio della pagina controllare nel localStorage che sia presente il valore con chiave "mieiArticoli". 
Se non è presente recuperarli da qui https://jsonplaceholder.typicode.com/users ed inserirli nel localStorage. 
Dopo aver preso i dati (dal localStorage o dalla chiamata remota) stamparli a schermo.

if(localStorage.getItem("mieiArticoli") == true){
    console.log("c'è")
}
else {
    console.log("non c'è")
    //var $lista = $("#mialista");
    var arr=[];

    var chiamata = $.ajax({
        //deve avere un verbo 
        type: "GET",
        //url api
        url: "https://jsonplaceholder.typicode.com/users",
        dataType: "json"
    })
    //response parametro implicito
    chiamata.done(function(response){
        //ottengo una risposta
        //console.log(response);
        arr.push(response);
        console.log(arr[0]);
        //RICORDA: devi fare il JSON.stringify per parsare da json a stinga
        localStorage.setItem("mieArticoli", JSON.stringify(response[0]) );
    })
    //se non funziona il done(?)
    chiamata.fail(function (error) {
        console.log("errore", error)
    })

}
*/