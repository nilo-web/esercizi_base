"use script";

/*

Mettere tutto quello fatto nell'esercizio precedente dentro una funzione.
 Quindi invocare la funzione due volte: il risultato deve essere 
 lo stesso dell'esercizio 2.

*   Modificare l'esercizio precedente in modo inserire un altro
 pulsante: se lo schiaccio devo cancellare tutte le righe inserite.
*/
/*
var bottone = document.getElementById("bottone");
var contenitore = document.getElementById("contenitore");
var botCancella = document.getElementById("cancella");
console.log(contenitore);
var cont = 0;


function f(color1, col2){
    var frase = "sto scrivendo... ";
    var lastcolor = color1;
    var color2=col2;
    
    contenitore.addEventListener("click", function(event){
        console.log(event)
    })
    var co =0;
    bottone.addEventListener("click", function () {
        co++;
        var nuovo = document.createElement("p");
        nuovo.innerText = frase + cont;
        nuovo.style.color= lastcolor;
        nuovo.setAttribute("i", co);
        contenitore.appendChild(nuovo);
        cont++;

        if (lastcolor===color1)  {
            lastcolor=color2;
        }
        else {
            lastcolor=color1;
        } 
        
        botCancella.addEventListener("click", function () {
            nuovo.remove();
        })
    })
    
    var conto = 0;
    var cambioColore = document.createElement("button");
    cambioColore.innerText="cambia colore div";
    contenitore.appendChild(cambioColore);
    cambioColore.addEventListener("click", function () {
        conto++;
        if(conto%2) contenitore.style.backgroundColor ="red";
        else contenitore.style.backgroundColor ="white";
    })
    /*
    oppure in questo modo mettendo il button nell'html

    var conto = 0;
    var cambioColore = document.getElementById("cambiaColore");
    cambioColore.addEventListener("click", function(){
        conto++;
        if(conto%2) contenitore.style.backgroundColor ="red";
        else contenitore.style.backgroundColor ="white";
    });
    

    var reset = document.createElement("button");
    contenitore.appendChild(reset);
    reset.innerText = "autodistruzione";
    reset.addEventListener("click", function () {
        
        while(contenitore.childNodes.length>0){
            contenitore.childNodes[0].remove();
        }
        contenitore.remove();
    })

}
f("green","blue");
*/

var $contenitore = $("#contenitore");
var $h1= $("#h");
var bottone = $("<button>clicca</button>");
console.log(bottone);
$contenitore.append(bottone)

$contenitore.on("click", function (e) {
    console.log(e);
})

//e.preventDeafult
//e.stopPropagation
//e.target == l'elemento in se, con this prende il padre(?)

/*
creo evento custom
var mioEvCustom = new Event("prova"); <--costruttore

creo un <p> nel html con un id
dopo

var containerRisultato = doc.getelementbyid(quello di p)
containerrisulatao.addeventlistener("prova", function(){
    console.log("ev custom")
})

Jquery
prendere attributo
$('[data-mio-att="123"]')

var $elemento = $('#elemento')

generare elemento
var $lista = $('#lista')
var $nuovoelemento = $("<li id ="4">k</li>")
lista.append(nuovoelemento)


lista.attr("test", "asd") fa da getter e setter, dipende se specifico o meno 
il secondo parametro

come chilnode per js ho in jq
lista.find("#123")

per eventi interfaccia
invece di mettere addeventlistener --> .on()
nuovoelemento.on("click", function(){
    console.log(123)
    nuovoelemento.off() = levo l'evento
})

$.fn.tuttoInMaiuscolo = function(){
    
}


*/