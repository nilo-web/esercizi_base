'user strict'


//array vuoto è obbligatorio
var myApp = angular.module("myApp", []);


//serivi per prendere dati, si possono instanziare in tre modi: service e factory
//il ser ci da un istanza di una funzione
//con factory ritorna la risoluzione di una funzione

myApp.service("toDoListService", function () {
    //istanziamento di una funzione (che avviene implic tramite new)

    console.log("serivice sta funzionando");
    //creo una lista di dati
    this.list =[];
    //metodo che mi interessa 

    this.getlistData = function(){
        return this.list;
    }

    this.additemtolist=function(item){
        //stiamo tornando un istanza di un ogg
        this.list.push(item);
        console.log("add ...", this.list)
    }
    
});

/*
myApp.factory("sommaFactory", function(){
    return function() {
        return "risultato factory";
    };
});
*/

//richiamo il mio controller
//lo scope mi permette di gestire le variabili, con lo $scope arrivo all'html

//IMPORTANTE = passo il servizio come parametro per poterlo utilizzare
myApp.controller("controller_1", function($scope, toDoListService){
    console.log("funziona il controller");


 

    //array per salvare todo
    $scope.TODO =[];
    //funzione per aggingere cose da fare e appenderle all'array
    $scope.appendiTODO = function () {
        
        console.log("sto salvando tramite la funzione ");
        //$scope.toDoListService.getlistData();

        var c  = {
            titolo: $scope.inputTitolo,
            contenuto: $scope.inputContenuto,
        }
        console.log("creato",c);
        
        //appendo alla lista l'elemeto creato
        toDoListService.additemtolist(c);

        //questa lista non dovrebbe servirmi
        $scope.TODO.push(c);

    }  

})


myApp.controller("visualizza", function($scope, toDoListService) {

       //questo andrebbe in un altro controller perche visualizza
       $scope.todolistItem   = toDoListService.getlistData();
    
})
