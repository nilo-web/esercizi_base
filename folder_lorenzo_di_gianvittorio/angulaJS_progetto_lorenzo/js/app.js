"use strict";

//array vuoto è obbligatorio
var myApp = angular.module("myApp", []);

//QUESTO È IL PRIMO CONTROLLER

//lo scope mi permette di gestire le variabili
myApp.controller("myController", function($scope){
    console.log("funziona il controller");

  $scope.mostraMesaggio = function(){
    console.log("il bottone funziona")
};

  $scope.user = {
      name: "tizio",
      surname: "caio",
      email: "semp@yahoo.it"
  }
})

//QUESTO È IL SECONDO CONTROLLER

//con il controllo prendo i dati e lo passo alla vista
myApp.controller("myController2", function($scope){
    console.log("il secondo controller funziona");

    //parte dei filtri
    $scope.name = "peppe";
    $scope.ora = new Date();

    $scope.numeriAcaso=[4,10,1,3];

    //così la re-innizializzo (ovvero un .empty) eliminando i post vecchi
    $scope.listaPost = [];

    $scope.mostraPost = function (id) {

      var arraydi_messaggi=[];
      $scope.listaPost = [];

   
      for (let index = 0; index < post.length; index++) {

        //se il post.userId è uguale all'id della mia lista di utenti allora lo metto in un array
        if(post[index].userId == id){
        arraydi_messaggi.push(post[index].title)
        $scope.listaPost.push(post[index]);
        }
      
      }
      console.log(arraydi_messaggi);

    }

    $scope.lista = [
        {
          "id": 1,
          "name": "Leanne Graham",
          "nome utente": "Bret",
          "email": "Sincere@april.biz",
          "indirizzo": {
            "street": "Kulas Light",
            "suite": "App. 556",
            "city": "Gwenborough",
            "codice postale": "92998-3874",
            "geo": {
              "lat": "-37,3159",
              "lng": "81.1496"
            }
          },
          "telefono": "1-770-736-8031 x56442",
          "sito web": "hildegard.org",
          "azienda": {
            "name": "Romaguera-Crona",
            "catchPhrase": "Rete neurale client-server multilivello",
            "bs": "sfruttare i mercati elettronici in tempo reale"
          }
        },
        {
          "id": 2,
          "name": "Ervin Howell",
          "nome utente": "Antonette",
          "email": "Shanna@melissa.tv",
          "indirizzo": {
            "street": "Victor Plains",
            "suite": "Suite 879",
            "city": "Wisokyburgh",
            "codice postale": "90566-7771",
            "geo": {
              "lat": "-43,9509",
              "lng": "-34,4618"
            }
          },
          "telefono": "010-692-6593 x09125",
          "sito web": "anastasia.net",
          "azienda": {
            "name": "Deckow-Crist",
            "catchPhrase": "contingenza didattica proattiva",
            "bs": "sinergizza catene di approvvigionamento scalabili"
          }
        },
        {
          "id": 3,
          "name": "Clementine Bauch",
          "nome utente": "Samantha",
          "email": "Nathan@yesenia.net",
          "indirizzo": {
            "street": "Douglas Extension",
            "suite": "Suite 847",
            "city": "McKenziehaven",
            "codice postale": "59590-4157",
            "geo": {
              "lat": "-68,6102",
              "lng": "-47,0653"
            }
          },
          "telefono": "1-463-123-4447",
          "sito web": "ramiro.info",
          "azienda": {
            "name": "Romaguera-Jacobson",
            "catchPhrase": "Interfaccia biforcata faccia a faccia",
            "bs": "e-enable applicazioni strategiche"
          }
        },
        {
          "id": 4,
          "name": "Patricia Lebsack",
          "nome utente": "Karianne",
          "email": "Julianne.OConner@kory.org",
          "indirizzo": {
            "street": "Hoeger Mall",
            "suite": "App. 692",
            "city": "South Elvis",
            "codice postale": "53919-4257",
            "geo": {
              "lat": "29,4572",
              "lng": "-164.2990"
            }
          },
          "telefono": "493-170-9623 x156",
          "sito web": "kale.biz",
          "azienda": {
            "name": "Robel-Corkery",
            "catchPhrase": "Produttività a tolleranza zero su più livelli",
            "bs": "transizione di servizi web all'avanguardia"
          }
        },
        {
          "id": 5,
          "name": "Chelsey Dietrich",
          "nome utente": "Kamren",
          "email": "Lucio_Hettinger@annie.ca",
          "indirizzo": {
            "street": "Skiles Walks",
            "suite": "Suite 351",
            "city": "Roscoeview",
            "codice postale": "33263",
            "geo": {
              "lat": "-31,8129",
              "lng": "62.5342"
            }
          },
          "telefono": "(254) 954-1289",
          "sito web": "demarco.info",
          "azienda": {
            "nome": "Keebler LLC",
            "catchPhrase": "Soluzione a tolleranza d'errore incentrata sull'utente",
            "bs": "rivoluziona i sistemi end-to-end"
          }
        },
        {
          "id": 6,
          "name": "Mrs. Dennis Schulist",
          "username": "Leopoldo_Corkery",
          "email": "Karley_Dach@jasper.info",
          "indirizzo": {
            "street": "Norberto Crossing",
            "suite": "App. 950",
            "city": "South Christy",
            "codice postale": "23505-1337",
            "geo": {
              "lat": "-71,4197",
              "lng": "71.7478"
            }
          },
          "telefono": "1-477-935-8478 x6430",
          "sito web": "ola.org",
          "azienda": {
            "name": "Considine-Lockman",
            "catchPhrase": "Interfaccia della linea di fondo sincronizzata",
            "bs": "e-enable innovative applications"
          }
        },
        {
          "id": 7,
          "name": "Kurtis Weissnat",
          "nome utente": "Elwyn.Skiles",
          "email": "Telly.Hoeger@billy.biz",
          "indirizzo": {
            "street": "Rex Trail",
            "suite": "Suite 280",
            "city": "Howemouth",
            "codice postale": "58804-1099",
            "geo": {
              "lat": "24,8918",
              "lng": "21.8984"
            }
          },
          "phone": "210.067.6132",
          "sito web": "elvis.io",
          "azienda": {
            "name": "Johns Group",
            "catchPhrase": "Task force multimediale configurabile",
            "bs": "genera e-tailer aziendali"
          }
        },
        {
          "id": 8,
          "name": "Nicholas Runolfsdottir V",
          "nome utente": "Maxime_Nienow",
          "email": "Sherwood@rosamond.me",
          "indirizzo": {
            "street": "Ellsworth Summit",
            "suite": "Suite 729",
            "city": "Aliyaview",
            "codice postale": "45169",
            "geo": {
              "lat": "-14.3990",
              "lng": "-120,7677"
            }
          },
          "telefono": "586.493.6943 x140",
          "sito web": "jacynthe.com",
          "azienda": {
            "name": "Abernathy Group",
            "catchPhrase": "Concetto secondario implementato",
            "bs": "e-enable extensible e-tailers"
          }
        },
        {
          "id": 9,
          "name": "Glenna Reichert",
          "nome utente": "Delphine",
          "email": "Chaim_McDermott@dana.io",
          "indirizzo": {
            "street": "Dayna Park",
            "suite": "Suite 449",
            "city": "Bartholomebury",
            "codice postale": "76495-3109",
            "geo": {
              "lat": "24.6463",
              "lng": "-168,8889"
            }
          },
          "telefono": "(775)976-6794 x41206",
          "sito web": "conrad.com",
          "azienda": {
            "name": "Yost and Sons",
            "catchPhrase": "Progetto commutabile basato sul contesto",
            "bs": "tecnologie aggregate in tempo reale"
          }
        },
        {
          "id": 10,
          "name": "Clementina DuBuque",
          "nome utente": "Moriah.Stanton",
          "email": "Rey.Padberg@karina.biz",
          "indirizzo": {
            "street": "Kattie Turnpike",
            "suite": "Suite 198",
            "city": "Lebsackbury",
            "codice postale": "31428-2261",
            "geo": {
              "lat": "-38,2386",
              "lng": "57.2232"
            }
          },
          "telefono": "024-648-3804",
          "sito web": "ambrose.net",
          "azienda": {
            "nome": "Hoeger LLC",
            "catchPhrase": "Task force di potenziamento centralizzato",
            "bs": "modelli end-to-end target"
          }
        }
      ]

    
})

//la vista viende modificata dalle direttive 