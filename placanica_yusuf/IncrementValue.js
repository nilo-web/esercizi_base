"use strict";
// Scrivi una funzione che ritorni una funzione che ritoni il numero incrementato 
//che è stato passato alla prima funzione. Ogni volta che la si chiama il numero viene incrementato di uno

function stampNumber(a){
    var b = a;
    return function() {
        return b++;
    };
};
var seven = stampNumber(7);
console.log(seven());
console.log(seven());
console.log(seven());
