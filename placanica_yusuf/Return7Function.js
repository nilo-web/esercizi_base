"use strict";

// Scrivi una funzione che ritorna una funziona che ritorna il numero che è stato passato alla prima funzione. es test(7)() //  7.

function stampNumber(a){
    var b = a;
    return function() {
        return b;
    };
};
var seven = stampNumber(7);
console.log(seven());
