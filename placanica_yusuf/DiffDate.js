"use strict";

function fromDateToDateUtc(y, m, d){
    return Date.UTC(y, m, d)
}

function DiffDate(){
    var data1 = new Date();
    var data2 = new Date("24/01/2021");
    var data1Utc = fromDateToDateUtc(data1.getFullYear(), data1.getMonth(), data1.getDate());
    var data2Utc = fromDateToDateUtc(data2.getFullYear(), data2.getMonth(), data2.getDate());    
    var dateDifference = data1Utc -data2Utc;
    var dayInMilliseconds = 1000 * 60 * 60 * 24;
    var result = Math.floor(dateDifference / dayInMilliseconds);
    console.log(data1Utc, data2Utc, dateDifference, result);    
}
DiffDate();