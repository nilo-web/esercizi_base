"use strict";

// Scrivi una funzione che ogni volta che viene chiamata ritorni il numero successivo della serie di fibonacci

var fib = (function fibonacci(){
    var num1 = 1;
    var num2 = 1;
    return function (){
        var res = num2;
        num2 = num1;
        num1 += res;
        return num1;
    }
})();

console.log(fib());
console.log(fib());
console.log(fib());
console.log(fib());
console.log(fib());
console.log(fib());