"use strict";
function  StamDate() {
    var days = ["lunedi", "martedi", "mercoledi", "giovedi", "venerdi", "sabato", "domenica"];
    var weeks = ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"];
    var data = new Date();
    var day = days[data.getDay()-1];
    var month = weeks[data.getMonth()];
    var CurrentDate = `${day} ${data.getDate()}, ${month} ${data.getFullYear()}`
    console.log(CurrentDate)
}
StamDate();
