function maxDate(){
    var dateArray = ["02/10/2021", "03/04/2021", "05/22/2020", "07/16/2018"];
    var maxValueDate = "";
    for(var i = 0; i < dateArray.length;i++){
        if(maxValueDate === ""){
            maxValueDate = new Date(dateArray[i]);
        }else{
            let newDate = new Date(dateArray[i]);
            maxValueDate = newDate > maxValueDate ? newDate : maxValueDate;
        }
    }
    console.log(maxValueDate)
    return maxValueDate;
}