"use strict";

// Scrivi una funzione che ritorna un numero e lo incrementi ogni volta che viene chiamata (IFFE)

var a = (function(){
    var b = 7;
    return function(){
        ++b;
        return b;
    }
})();


console.log(a());
console.log(a());