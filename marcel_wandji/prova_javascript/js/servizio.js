// Servizio che recupera gli utenti e i posts per applicazione

function getUsers() {

   var deferred = $.Deferred();

    $.ajax({
        type: "GET",
        datatype: "json",
        url: "https://jsonplaceholder.typicode.com/users",
      }).done(
        function (response) {
          //console.log(response);
          deferred.resolve(response);
      }).fail(
        function (error) {
          console.log("Errore");
        }
      );
    return deferred;
}

function getPostsUtente(id) {

  var deferred = $.Deferred();

    $.ajax({
        type: "GET",
        datatype: "json",
        url: "https://jsonplaceholder.typicode.com/posts?userId=" +id,
      }).done(
        function (response) {
          //console.log(response);
          deferred.resolve(response);
      }).fail(
        function (error) {
          console.log("Errore");
        }
      );
    return deferred;
}
