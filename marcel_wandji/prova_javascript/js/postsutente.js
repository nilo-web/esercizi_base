

function postsutente(){
    //console.log("utente cliccato", this.getAttribute("id"));

    var id = this.getAttribute("id");

    var servizio = getPostsUtente(id);

    servizio.then(
        function (response) {
             console.log(response);
             console.log ( 'promessa mantenuta' );
             posts(response, id);

             $('#posts').attr("class", "col-md-4");
             $('.col-md-12').attr("class", "col-md-8");
             $('#posts').css("display", "block");
             selezioneUtente(id);
        },
        function () {
        ( 'promessa NON mantenuta' );
    });
}


// Funzione che prende in input la lista dei posts di un utente e l'id del utente e appende i post nel html
function posts(posts, id){

    //setta il titolo con il nome dell'utente
    var $titolo = $('<h2>' + $('#' + id +' h2').html() + '</h2>');

    //lista per contenere i posts
    var $postslist =  $('<ul class="list-group"></ul>');

    //appende ogni post alla lista
    for(var i = 0; i < posts.length; i++){
        var $post = $(
            `<li class="list-group-item" id="${posts[i].id}">
                <span>${posts[i].title}</span>
            </li>`
        );
        
        $postslist.append($post);
    }

    //refresh del contenitore del post
    $('#posts').empty();

    //Appende gli elementi al div di post
    $("#posts").append($titolo);
    $("#posts").append($postslist);
    
}