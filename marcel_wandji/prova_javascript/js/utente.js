
// funzione che prende in input un utente e crea un card con l'evento click
function cardUtente(utente) {
        //console.log(response);
    var $cardUtente = $(
        `<div class="card" id=${utente.id}>
            <div class="card-body">
                <h2 class="card-title">${utente.name}</h2> 
                <p class="card-subtitle mb-2 text-muted">${utente.username}</p>
                <p class="card-subtitle mb-2 text-muted">${utente.email}</p>
            </div>
        </div>`
    );

    //aggiunge l'evento click a un card
    $cardUtente.on("click", postsutente);
    
    //aggiunge un card al container
    $("#container").append($cardUtente);
    
}


function selezioneUtente(id){
    console.log('utene selezionato', id)

    //se l'elemento è gia selezionato in precedenza
    if($('.selected').attr("id") == id){
        console.log('utente gia selezionato');

        // i post vengono nascosti
        $('.selected').removeClass('selected'); 
        $('#posts').css("display", "none");
        $('.col-md-8').attr("class", "col-md-12");
    }
    else{//Altrimenti
        //tolgo l'elemento precedente
        $('.selected').removeClass('selected'); 

        //nuovo elemento selezionato
        $("#"+ id).addClass("selected");
    }
    
}