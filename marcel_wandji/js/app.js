"use strict";

//es1

// var array1 = [20, 33, 12, 1233];
// var array2 = [1, 33, 102, -12];

// var temp= [];

// temp = temp.concat(array1);

// for(var i = 0; i < array2.length; i++)
// {
//     var a = array2[i];

//     if(temp.indexOf(a) === -1)
//         temp.push(a);
// }

// var result = 
//     temp.sort(function(a,b)
//     {
//        return a - b;
//     });

// console.log(result);

//====================================

//es2

// var word = "supercalifragilistichespiralidoso";

// function reverse(word)
// {
//     return word.split("").reverse().join("");
// }

// console.log(reverse(word));
//=====================================

//ES3

// var giorniSettimana = ["lunedi", "martedi", "mercoledi", "giovedi", "venerdi", "sabato", "domenica"];
// var dataOggi = new Date();

// var indiceGiornoPrima = dataOggi.getDay() - 1;

// var giorniTraOggieDomenica = giorniSettimana.slice(indiceGiornoPrima, giorniSettimana.length);
// var gioniTraDomenicaeOggi = giorniSettimana.slice(0,indiceGiornoPrima);

// console.log(giorniTraOggieDomenica.concat(gioniTraDomenicaeOggi));
//=========================================

//ES4

// var  objectsList  =  [ 
//     {  name :  "Pino" ,  id :  66  }, 
//     {  name :  "Rino" ,  id :  16  }, 
//     {  name :  "Dino" ,  id :  6  }, 
//     {  name :  "Lino" ,  id :  96  }, 
//     {  name :  "Gino" ,  id :  26  } 
//    ];

//    objectsList.sort(function(a,b)
// {
//     return a.id - b.id;
// });

// console.log(objectsList);
// ===========================================

//ES5
// function stampaData()
// {
//     var giorni = ["lunedi","martedi","mercoledi","giovedi","venerdi","sabato","domenica",];
    
//     var mesi = 
//     [
//         "gennaio",
//         "febbraio",
//         "marzo",
//         "aprile",
//         "maggio",
//         "giugno",
//         "luglio",
//         "agosto",
//         "settembre",
//         "ottobre",
//         "novembre",
//         "dicembre"
//     ];

//     var oggi = new Date();

//     var giornoOggi = giorni[oggi.getDay() - 1];

//     return giornoOggi + " " + oggi.getDate() +  " " + mesi[oggi.getMonth()] + " " + oggi.getFullYear();
// }

// console.log(stampaData());
//=================================

//ES8
// function Esempio(numero)
// {
//     return function()
//     {
//         return numero;
//     };
// }

// var result = Esempio(123);

// console.log(result());
//==================================

//ES9

// function Esempio(n)
// {
//     return function ()
//     {
//        return ++n;
//     };
// };

// var result = Esempio(0);

// for (var i=0; i<5; i++)
// {
//     console.log(result());
// }
// =================================

//ES10
// var result = (function (n){
//     var conta = n;
//     return function(){
//         return ++conta;
//     }
// })(0);
 
// console.log(result());
// console.log(result());
// console.log(result());
// ==========================================


// ES10
// var result = function Fibonacci() {
//     var f1 = 0;
//     var f2 = 1;
//     var fib =0;
//     return function()
//     {
//         fib = f1 + f2;
//         f1 = f2;
//         f2 = fib;
//         return temp;
//     }
// }();

// console.log(result());
// ===============================================

//ES12/13/14

// var button = document.getElementById("btn");
// var div = document.getElementById("container");

// var res =
//     (
//         function(n)
//         {
//             var i = n;
//             return function()
//             {
//                 return ++i;
//             }
//         }
//     )(0);

// button.addEventListener("click", function()
// {       
//     var newTagp = document.createElement("p");

//     var r = res();
//     newTagp.innerText = "riga: " + r;

//     var colore = r%2 == 0 ? "red" : "blue";
//     newTagp.style.color = colore;
//     div.appendChild(newTagp); 
// });
//================================================0

//ES15

// var button = document.getElementById("btn");
// var div = document.getElementById("container");

// var isGreen = true;

// function toggle()
// {   
//     isGreen = isGreen ? false : true;
//     return isGreen;     
// };

// function fun(param1,param2)
// {
//     button.addEventListener("click",function()
//     {
//         var p = document.createElement("p");

//         var colore;

//         if(toggle() === true)
//             colore = param1;
//         else
//             colore = param2;

//         // imposto il colore al nuovo paragrafo creato
//         p.style.color = colore;
        
//         p.innerText = "colore paragrafo " + colore; 

//         // appeno al div1 il nuovo paragrafo creato
//         div.appendChild(p);
        
//     });
// };

// fun("green", "yellow");
// ==========================================

// ES16

// var button = document.getElementById("btn");
// var div = document.getElementById("container");

// var btn2 = document.createElement('button');
// // btn2.setAttribute('id', 'btn2');
// btn2.innerText = "cancella";
// div.appendChild(btn2);

// var isGreen = true;

// function toggle()
// {   
//     isGreen = isGreen ? false : true;
//     return isGreen;     
// };

// function fun(param1,param2)
// {
//     button.addEventListener("click",function()
//     {
//         var p = document.createElement("p");

//         var colore;

//         if(toggle() === true)
//             colore = param1;
//         else
//             colore = param2;

//         // imposto il colore al nuovo paragrafo creato
//         p.style.color = colore;
        
//         p.innerText = "colore paragrafo " + colore; 

//         // appeno al div1 il nuovo paragrafo creato
//         div.appendChild(p);
        
//     });

//     btn2.addEventListener("click",function(){
//         var elements = document.getElementsByTagName('p');

//         for(var i=0; i<elements.length; i++){
//             elements[i].remove();
//             i--;
//         }
//     });
// };

// fun("green", "yellow");
//==================================0===

//ES17

// var button = document.getElementById("btn");
// var div = document.getElementById("container");

// var btn2 = document.createElement('button');
// // btn2.setAttribute('id', 'btn2');
// btn2.innerText = "cancella";
// div.appendChild(btn2);

// var btn3 = document.createElement('button');
// btn3.innerText = "button 3";
// div.appendChild(btn3);
// var isGreen = true;

// function toggle()
// {   
//     isGreen = isGreen ? false : true;
//     return isGreen;     
// };

// function fun(param)
// {
//     button.addEventListener("click",function()
//     {
//         var p = document.createElement("p");

//         var colore;

//         if(toggle() === true)
//             colore = "green";
//         else
//             colore = "yellow";

//         // imposto il colore al nuovo paragrafo creato
//         p.style.color = colore;
        
//         p.innerText = "colore paragrafo " + colore; 

//         // appeno al div1 il nuovo paragrafo creato
//         div.appendChild(p);
        
//     });

//     btn2.addEventListener("click",function(){
//         param.innerText = '';
//     });

// };

// fun(div);

//==================================

//ESE18
// var button = document.getElementById("btn");
// var div = document.getElementById("container");

// var btn2 = document.createElement('button');
// // btn2.setAttribute('id', 'btn2');
// btn2.innerText = "cancella";
// div.appendChild(btn2);

// var btn3 = document.createElement('button');
// btn3.innerText = "button 3";
// div.appendChild(btn3);
// var isGreen = true;

// function toggle()
// {   
//     isGreen = isGreen ? false : true;
//     return isGreen;     
// };

// function fun()
// {
//     button.addEventListener("click",function()
//     {
//         var p = document.createElement("p");

//         var colore;

//         if(toggle() === true)
//             colore = "green";
//         else
//             colore = "yellow";

//         // imposto il colore al nuovo paragrafo creato
//         p.style.color = colore;
        
//         p.innerText = "colore paragrafo " + colore; 

//         // appeno al div1 il nuovo paragrafo creato
//         div.appendChild(p);
        
//     });

//     btn2.addEventListener("click",function(){
//         param.innerText = '';
//     });

//     btn3.addEventListener("click",function(){
//         div.style.backgroundColor = "white"
//     });

// };

// fun();

//=========================================

//ES19
var button = document.getElementById("btn");
var div = document.getElementById("container");
div.style.backgroundColor = "white"

var btn2 = document.createElement('button');
// btn2.setAttribute('id', 'btn2');
btn2.innerText = "cancella";
div.appendChild(btn2);

var btn3 = document.createElement('button');
btn3.innerText = "button 3";
div.appendChild(btn3);
var isGreen = true;

function toggle()
{   
    isGreen = isGreen ? false : true;
    return isGreen;     
};

function fun()
{
    button.addEventListener("click",function()
    {
        var p = document.createElement("p");

        var colore;

        if(toggle() === true)
            colore = "green";
        else
            colore = "yellow";

        // imposto il colore al nuovo paragrafo creato
        p.style.color = colore;
        
        p.innerText = "colore paragrafo " + colore; 

        // appeno al div1 il nuovo paragrafo creato
        div.appendChild(p);
        
    });

    btn2.addEventListener("click",function(){
        param.innerText = '';
    });

    btn3.addEventListener("click",function(){
        div.style.backgroundColor = div.style.backgroundColor === "" ? "white" : "";
    });

};

fun();