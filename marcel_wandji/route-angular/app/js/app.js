"use strict";

var myApp = angular.module("myApp", ["ngRoute"]);

myApp.config(function($routeProvider) {
  $routeProvider
  .when("/", {
    templateUrl: "../templates/login.html",
    controller: "loginController"
  })
  .when("/profile/:username", {
    templateUrl: "/templates/profile.html",
    controller: "profileController"
  }).when("/registrazione", {
    templateUrl: "../templates/registrazione.html",
    controller: "registrazioneController"
  })
  .otherwise({
    redirectTo: "/"
  })
});


myApp.controller("loginController", function($scope, $location) {
  $scope.loggami = function(username) {
    $location.path("/profile/" + username);
  };

  $scope.registra = function() {
    $location.path("/registrazione");
  }
});


myApp.controller("profileController", ["$scope", "$routeParams", function($scope, $routeParams) {
  $scope.username = $routeParams.username;
}]);

myApp.controller("registrazioneController", ["$scope", function($scope){
  //: nome, cognome, username, password, accettazione_privacy, sesso.
  $scope.user = {
    nome: "",
    cognome: "", 
    username: "", 
    password: "",
    accettazione_privacy: false, 
    sesso: "maschio"
  };

  $scope.registra = function(user){
     console.log(user);
  }



}]);