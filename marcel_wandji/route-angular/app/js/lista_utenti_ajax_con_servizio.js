"use strict";

var myApp = angular.module("myApp", []);

myApp.factory("servizioUtenti", function ($http, $q) {
  var listaUtenti = [];
  return {
    getUtenti: function () {
      var promessa = $q.defer();

      if (listaUtenti.length === 0) {
        $http
          .get("https://jsonplaceholder.typicode.com/users")
          .then(function (response) {
            listaUtenti = response.data;
            promessa.resolve(listaUtenti);
          }, errorCallback);
      } else {
        promessa.resolve(listaUtenti);
      }

      return promessa.promise;
    },
  };
});

myApp.factory("servizioArticoli", function ($http) {
  return {
    getArticoli: function (userId) {
      return $http.get(
        "https://jsonplaceholder.typicode.com/posts?userId=" + userId
      );
    },
  };
});

function errorCallback(error) {
  alert("ERROR");
}

myApp.controller("utentiController", [
  "$scope",
  "servizioUtenti",
  "servizioArticoli",
  function ($scope, servizioUtenti, servizioArticoli) {
    $scope.listaUtenti;
    console.log("controller");

    servizioUtenti.getUtenti().then(function (data) {
      $scope.listaUtenti = data;
    }, errorCallback);

    $scope.prendiIPost = function (userId) {
      console.log("click", userId);

      servizioArticoli.getArticoli(userId).then(function (response) {
        console.log(response);

        $scope.listaArticoli = response.data;
      }, errorCallback);
    };
  },
]);
