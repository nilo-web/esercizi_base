"use strict";

var DettaglioUtente = (function(){
    
     var instance;

     var $contenitoreDettagliUtente = $("#dettagliUtente");
     var $dettagliUtenteName = $("#nome-utente");
     var $listaPostUtente = $contenitoreDettagliUtente.find("#listaPosttUtente");

     return {
         getInstance: function(){
             if (!instance){
                 instance = {
                     render: function(userCardSelected){
                         console.log("user card selezionato", userCardSelected);
                         $dettagliUtenteName.html(userCardSelected.name);

                       //POSTS
                         var post = getPostAJAX(userCardSelected.id);
                       post.then(
                        function (responseListaPost) {
                            $listaPostUtente.html("");
                            for (var i = 0; i < responseListaPost.length; i++) {
                                var postResponse = responseListaPost[i];
              
                                var istanzaPostUtente = new UserPost(postResponse);
                                istanzaPostUtente.renderPost($listaPostUtente);
                            }
                        },
                        function (error) {
                            alert("Ops la chiamata non ha funzionato");
                        }
                      );
                     },
                 };
             }
             return instance;
            },

     };
})();