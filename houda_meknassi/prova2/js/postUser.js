"use strict";

function UserPost(postObj) {
    this.uid = postObj.userId;
    this.id = postObj.id;
    this.title = postObj.title;
    this.$tpl = $('<li class="post-utente"></li>');
}

UserPost.prototype.renderPost = function($container) {
   
    var contenutoDettPost = this.title;
    this.$tpl.html(contenutoDettPost);

   
    this.$tpl.attr("d-id-utente", this.id);

    $container.append(this.$tpl);
};
