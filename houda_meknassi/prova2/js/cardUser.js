"use strict";

function UserCard(userObj) {
    this.id = userObj.id;
    this.name = userObj.name;
    this.username = userObj.username;
    this.email = userObj.email;
    this.$tpl = $("<div class='card-utente'></div>");

}
UserCard.prototype.renderCard = function($container){
    var $contCard = this.name + " - " + this.username +": "+ this.email;

    this.$tpl.html($contCard);

    this.$tpl.attr("d-id-utente", this.id);
    this.$tpl.on("click", this.clickHandler.bind(this));

    $container.append(this.$tpl);
};
UserCard.prototype.clickHandler = function(evento){
    console.log("sono stato cliccato", this, evento);
    //this.$tpl.toggleClass("active");

    $d.trigger("userSelected", this.id);
};