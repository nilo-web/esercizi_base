'use strict';

function getUtentiAjax() {
    
    var chiamataPerUtenti = $.ajax({
    type: "GET",
    url: "https://jsonplaceholder.typicode.com/users",
});
return chiamataPerUtenti;
}


function getPostAJAX(id) {
    var chiamataPerPost = $.ajax({
      type: "GET",
      url: "https://jsonplaceholder.typicode.com/posts?userId="+id,
    });
  
    return chiamataPerPost;
  }




