'use strict'
//Scrivi una funzione che ritorna un numero e lo incrementi ogni volta che viene chiamata (IFFE)

var m = (function() {
    var a = 2;
    return function(){
        ++a;
        return a;
    }
})();         
console.log(m()); 
console.log(m());  
console.log(m());    