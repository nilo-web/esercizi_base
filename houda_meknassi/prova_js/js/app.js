"use strict";

var $contenitoreListaUtenti = $("#listaUtenti");

var $contenitoreDettagliUtente = $("#dettagliUtente");
var $dettagliUtenteName = $("#nome-utente");
var $listaPostUtenti = $contenitoreDettagliUtente.find("#listaPosttUtente");
var listaUtenti;

var chiamataPerUtenti = $.ajax({
     type: "GET",
     url: "https://jsonplaceholder.typicode.com/users",
});

chiamataPerUtenti.done(function(rispostaChiamata){
listaUtenti = rispostaChiamata;

  for (var i=0; i<listaUtenti.length; i++){
      var utenTemp = listaUtenti[i];
      var $card = $("<div class='card-utente'></div>");
      var $contCard = utenTemp.name + " - " + utenTemp.username + utenTemp.email;

      $card.html($contCard);
      $card.attr("d-id-utente", utenTemp.id);
      $contenitoreListaUtenti.append($card);

      $card.on("click", function(event){
         //console.log("i was clicked", event.target);

       var utenteSelected;

      for (var j=0; j<listaUtenti.length; j++) {
        var utenTemp = listaUtenti[j];
        if (utenTemp.id == event.target.getAttribute("d-id-utente")) {
        utenteSelected = utenTemp;
      }
  }
  $dettagliUtenteName.html(utenteSelected.name);

 var urlchiamataPerPostUtente = "https://jsonplaceholder.typicode.com/posts?userId=" + utenteSelected.id;
 var chiamataPostUtente = $.ajax({
   type: "GET",
   url: urlchiamataPerPostUtente,
   });
 
chiamataPostUtente.done(function(responsePostUtenti){
       $listaPostUtenti.html("");

     for (var a=0; a<responsePostUtenti.length; a++){
       var PostTemp = responsePostUtenti[a];
       var $postUtenti = $("<li></li>");
       $postUtenti.html(PostTemp.title);
       $listaPostUtenti.append($postUtenti);
  } 
});
  chiamataPostUtente.fail(function(errore){
      alert("errore post");
     });

   });

   }
});

chiamataPerUtenti.fail(function(error){
   alert("Qualcosa non va!");
});