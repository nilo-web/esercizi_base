'use strict';
var $lista = $("#mialista");
var chiamataAjax = $.ajax({
    type: "GET",
    url: "https://jsonplaceholder.typicode.com/posts" 
});

chiamataAjax.done(function(response) {
  console.log("response", response);
  
  var $nuovoItem = $('<li>' + response[3].title + '</li>')
  $lista.append($nuovoItem);
});

chiamataAjax.fail(function(e) {
  console.log("errore", e);
});
