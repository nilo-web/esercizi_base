"use strict";

var myApp = angular.module("myApp", []);

//controller lista inserimento
myApp.controller("myController", function ($scope, servicePosts) {
  $scope.salva = () => {
    servicePosts.add($scope);

    $scope.title = "";
    $scope.body = "";
  };
});

//controller filtro
myApp.controller("filterController", function () {});

// servizio per posts
myApp.service("servicePosts", function () {
  this.posts = [];
  this.get = () => {
    return this.posts;
  };
  this.add = (object) => {
    let tmpPost = {
      title: object.title,
      body: object.body,
    };
    this.posts.push(tmpPost);
  };
});

//servizio per i filtri
myApp.service("serviceFilter", function () {
  this.filter = "";
  this.get = () => {
    return this.filter;
  };
  this.add = (filterText) => {
    this.filter = filterText;
  };
});

// controller dei posts
myApp.controller("postController", function ($scope, servicePosts) {
  $scope.posts = servicePosts.get();
});

// myApp.factory("sommaFactory", function () {
//   return function () {
//     return "sono il risultato di sommaFactory";
//   };
// });
