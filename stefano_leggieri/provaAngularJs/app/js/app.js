"use strict";

var app = angular.module("myApp", []);

// app.config(function ($routeProvider) {
//   $routeProvider
//     .when("/", {
//       templateUrl: "/templates/login2.html",
//       controller: "loginController",
//     })
//     .when("/student/:username", {
//       templateUrl: "/templates/student.html",
//       controller: "studentController",
//     })
//     .otherwise({
//       redirectTo: "/",
//     });
// });

app.controller("loginController", function ($scope, utenteService) {
  // $scope.authenticate = function (username) {
  //   $location.path("/student/" + username);
  // };

  $scope.valid = false;
  $scope.registration = () => {
    utenteService.add($scope.user);
    $scope.valid = true;
  };
});

app.controller("studentController", function ($scope, utenteService) {
  console.log("prendo utente", utenteService.get());
});

app.service("utenteService", function () {
  this.user = [];
  this.get = function () {
    return this.user;
  };
  this.add = function (user) {
    localStorage.setItem("user", JSON.stringify(user));
    this.user.push(user);
    console.log("salvato utente");
  };
});
