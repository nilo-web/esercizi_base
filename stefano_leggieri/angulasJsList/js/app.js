"use strict";

var myApp = angular.module("myApp", []);

//i controller non devono mai manipolare il Dom, non devono formattare gli input, non devono formattare l'output
// utilizzarlo per prendere i dati passati per esempio da un servizio e passarli alla view
myApp.controller(
  "myController",
  function ($scope, getUsersFactory, getPostsFactory) {
    getUsersFactory.getUser().then(function (result) {
      $scope.users = result;
    });

    $scope.nameUser = "";
    $scope.manina = "manina";
    $scope.utenteScelto = "";
    $scope.mostraPost = function (userId, userName) {
      if ($scope.utenteScelto !== userId) {
        getPostsFactory.getPosts(userId).then(function (resultPost) {
          console.log(resultPost);
          $scope.posts = resultPost;
          $scope.nameUser = userName;
          $scope.utenteScelto = userId;
        });
      } else {
        $scope.utenteScelto = "";
        $scope.post = [];
      }
    };
    $scope.close = function () {
      $scope.utenteScelto = "";
      $scope.post = [];
    };

    // $scope.countComments = (postId) => {
    //   var count = 0;
    //   for (const n in listComments) {
    //     if (postId === listComments[n].postId) {
    //       count++;
    //     }
    //   }
    //   return count;
    // };
  }
);

myApp.factory("getUsersFactory", function ($http) {
  var getUser = function () {
    return $http.get("https://jsonplaceholder.typicode.com/users").then(
      function (responseData) {
        return responseData.data;
      },
      function (error) {}
    );
  };
  return { getUser: getUser };
});

myApp.factory("getPostsFactory", function ($http) {
  var getPosts = function (userId) {
    return $http({
      url: "https://jsonplaceholder.typicode.com/posts",
      method: "GET",
      params: { userId: userId },
    }).then(
      function (responseData) {
        return responseData.data;
      },
      function (error) {}
    );
  };
  return { getPosts: getPosts };
});
