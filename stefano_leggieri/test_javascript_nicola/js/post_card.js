"use strict";

function PostCard(postObject) {
  this.id = postObject.id;
  this.userId = postObject.userId;
  this.title = postObject.title;
  this.body = postObject.body;
  this.$tplPost = $("<li></li>");
}

PostCard.prototype.render = function ($container) {
  this.$tplPost.html("<h4>" + this.title + "</h4><p>" + this.body + "</p>");
  $container.append(this.$tplPost);
};
