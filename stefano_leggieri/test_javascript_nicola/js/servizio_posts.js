"use strict";

function getPostUtenteAJAX(userId) {
  // genero la chiamata http passandogli un oggetto di configurazione
  // con l'url e il verbo http
  var chiamataPerUtenti = $.ajax({
    type: "GET",
    url: "https://jsonplaceholder.typicode.com/posts",
    data: {
      userId: userId,
    },
  });

  return chiamataPerUtenti;
}
