function _getUsers() {
  let $users = localStorage.getItem("Users");
  let $deferred = $.Deferred();
  if (!$users) {
    $.ajax({
      type: "GET",
      url: "https://jsonplaceholder.typicode.com/users",
      success: function (response) {
        localStorage.setItem("Users", JSON.stringify(response));
        $deferred.resolve(response);
      },
    });
  } else {
    $deferred.resolve(JSON.parse($users));
  }
  return $deferred;
}

function _getPosts(userId) {
  let $posts = localStorage.getItem("Posts_User_" + userId);
  let $deferred = $.Deferred();
  if (!$posts) {
    $.ajax({
      type: "GET",
      url: "https://jsonplaceholder.typicode.com/posts",
      data: {
        userId: userId,
      },
      success: function (response) {
        localStorage.setItem("Posts_User_" + userId, JSON.stringify(response));
        $deferred.resolve(response);
      },
    });
  } else {
    $deferred.resolve(JSON.parse($posts));
  }
  return $deferred;
}

function _getComments(postId) {
  var $comments = localStorage.getItem("Comments_Post_" + postId);
  var $deferred = $.Deferred();
  if (!$comments) {
    $.ajax({
      type: "GET",
      url: "https://jsonplaceholder.typicode.com/comments",
      data: {
        postId: postId,
      },
      success: function (response) {
        localStorage.setItem(
          "Comments_Post_" + postId,
          JSON.stringify(response)
        );
        $deferred.resolve(response);
      },
    });
  } else {
    $deferred.resolve(JSON.parse($comments));
  }
  return $deferred;
}
