// Qui creo il layout per gli utenti

var getLayoutUsers = () => {
  var $containerUsers = $("#containerUsers");
  for (const index in appUsers.users) {
    var $singleUser = createLayoutUser(appUsers.users[index]);
    $containerUsers.append($singleUser);
  }
};

//qui creo ogni singola scheda dell'utente
var createLayoutUser = (user) => {
  let $divUser = $("<div  class='scheda row'></div>");
  $divUser.attr("id", "user_" + user.id);
  $divUser.attr("data-id", user.id);
  let $layoutUser = $(
    "<div class='col-md-3'><b>Name</b>:</div><div class='col-md-8'>" +
      user.name +
      "</div>" +
      "<div class='col-md-3'><b>Username</b>:</div><div class='col-md-8'>" +
      user.username +
      "</div>" +
      "<div class='col-md-3'><b>Email</b>:</div><div class='col-md-8'>" +
      user.email +
      "</div>"
  );
  $divUser.append($layoutUser);
  return $divUser;
};

// Qui creo il layout per i posts del singolo utente
var getLayoutPosts = (userId) => {
  let $containers = $("#containers");
  let $postContainer = $("<div id='listaPosts'></div>");
  $postContainer.attr("data-user", userId);
  //qui creo la x per la chiusura dei post
  let $close = createCloseLayout();
  $postContainer.append($close);
  let $titolo = $(
    "<h3>List of posts by " + appUsers.users[userId].name + "</h3><hr>"
  );
  $postContainer.append($titolo);
  $postContainer.attr("class", "col-md-4");
  // qui mi genero il layout di tutta la lista dei posts
  let $listPosts = createLayoutPost();
  $postContainer.append($listPosts);
  // quando viene creata la lista dei posts modifico il layout delle schede
  modifyClassListaUtenti();
  $containers.append($postContainer);
};

//Layout della lista dei posts
var createLayoutPost = () => {
  let $list = $("<div class='list-group list'></div>");
  for (const index in appUsers.posts) {
    let $singlePost = $(
      "<a href='#' class='list-group-item ' data-toggle='modal' data-target='#myModal' data-postId='" +
        appUsers.posts[index].id +
        "'><h4 class='list-group-item-heading'>" +
        appUsers.posts[index].title +
        "</h4><p class='list-group-item-text'>" +
        appUsers.posts[index].body +
        "</p></a>"
    );
    $list.append($singlePost);
  }
  return $list;
};

//layout della x di chiusura dei posts
var createCloseLayout = () => {
  $close = $("<div id='closeListPosts' class='close'>X</div>");
  return $close;
};

//layout che modifica le schede utente
var modifyClassListaUtenti = function () {
  let $listaUtenti = $("#listaUtenti");
  if ($listaUtenti.hasClass("col-md-12")) {
    $listaUtenti.removeClass("col-md-12").addClass("col-md-8");
  } else {
    //se sta qui stiamo chiudendo la lista posts
    $listaUtenti.removeClass("col-md-8").addClass("col-md-12");
  }
};

//Qui creo la modale dei commenti
function getModalComments(comments) {
  var $listComments = $("<ul class='list-group'></ul>");
  for (var i = 0; i < comments.length; i++) {
    let $singleComment = $(
      "<li class='list-group-item'><p><b>" +
        comments[i].name +
        "</b><br>" +
        comments[i].body +
        "</p></li>"
    );
    $listComments.append($singleComment);
  }
  //non intercettando l'evento di chiusura della modale la svuoto prima di riempirla di nuovo
  //svuoto il titolo
  $("#myModalLabel").html("");
  $("#myModalLabel").html(
    "Title post: " + appUsers.posts[comments[0].postId].title
  );
  //svuoto il corpo
  $(".modal-body").html("");
  $(".modal-body").append($listComments);
  $("body").addClass("modal-open");
  $(".modal").addClass("in").attr("style", "display:block");
}
