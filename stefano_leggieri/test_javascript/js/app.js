function appUsers() {
  this.users = {};
  this.posts = {};
}

(function () {
  //chiamata per gli utenti
  var users = _getUsers();
  users.done(function (result) {
    // mi salvo gli utenti nel mio modello
    modelUsers(result);
    // costruisco il layout degli utenti
    getLayoutUsers();
    // genero gli handler per post e commenti
    openClosePostUser();
  });
})();
