function User(object) {
  this.id = object.id;
  this.name = object.name;
  this.username = object.username;
  this.email = object.email;
}

function modelUsers(userList) {
  for (var i = 0; i < userList.length; i++) {
    appUsers = {
      ...appUsers,
      users: {
        ...appUsers.users,
        [userList[i].id]: new User(userList[i]),
      },
    };
  }
}

function Post(object) {
  this.id = object.id;
  this.title = object.title;
  this.body = object.body;
  this.userId = object.userId;
}

function modelPosts(listPosts) {
  for (var i = 0; i < listPosts.length; i++) {
    appUsers = {
      ...appUsers,
      posts: {
        ...appUsers.posts,
        [listPosts[i].id]: new Post(listPosts[i]),
      },
    };
  }
}

function resetPosts() {
  appUsers.posts = {};
}
