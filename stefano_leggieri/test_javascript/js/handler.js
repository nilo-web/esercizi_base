var $currentUser = "";

function openClosePostUser() {
  //genero un handle per ogni scheda
  var $cardUser = $(".scheda");
  $cardUser.on("click", function (e) {
    //mi prendo il la scheda cliccata
    var $evento = $(e.currentTarget);

    //controllo se ha la classe ""schedaActive""
    // se la classe è gia presente la scheda è già stata cliccata
    // altrimenti la rendo attiva e lancio la chiamata ai post
    if ($evento.hasClass("schedaActive")) {
      //se sono entrato la scheda era già attiva
      //chiudo i post
      closeListPosts($evento);
      //resetto l'ogetto che li contiene
      resetPosts();
      //la scheda corrente è svuotata
      $currentUser = "";
    } else {
      // faccio la chiamata ai post per la nuova scheda
      var posts = _getPosts(e.currentTarget.getAttribute("data-id"));
      posts.done(function (result) {
        //controllo se la scheda cliccata è diversa da quella precedente
        if ($currentUser !== "" && $evento !== $currentUser) {
          // se la scheda è diversa chiudo i post e li resetto
          closeListPosts($currentUser);
          resetPosts();
        }
        //aggiorno la scheda corrente
        $currentUser = $evento;
        // aggiungo la classe attiva
        $evento.addClass("schedaActive");
        //quando la chiamata dei post torna li aggiungo al mio modello posts
        modelPosts(result);
        // mi creo il laypout dei posts
        getLayoutPosts(e.currentTarget.getAttribute("data-id"));
        //genero l'hendler di chiusura dei post
        handleClose($evento);
        //genero l'evento per la chiamata dei commenti e la sua modale
        openCloseModalComments();
      });
    }
  });
}

// Creo l'evento per la chiusura dei posts
function handleClose($evento) {
  let $closeList = $("#closeListPosts");
  $closeList.on("click", () => {
    closeListPosts($evento);
  });
}

//Quanbdo chiudo i post modifico anche il layout delle schede utente
function closeListPosts($evento) {
  let $listaPost = $("#listaPosts");
  $listaPost.remove();
  modifyClassListaUtenti();
  $evento.removeClass("schedaActive");
}

//Questa funziona ha l'evento per la chiamata hai post e la creazione della modale
function openCloseModalComments() {
  var $singlePost = $(".list-group-item");
  $singlePost.on("click", function (e) {
    var comments = _getComments(e.currentTarget.getAttribute("data-postid"));
    comments.done(function (result) {
      getModalComments(result);
    });
  });
}
