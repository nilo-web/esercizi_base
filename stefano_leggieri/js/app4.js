"use strict";

// var myProgram = () => {
//   var singlePost = localStorage.getItem("mieiArticoli");

//   if (singlePost === null) {
//     myCall();
//   } else {
//     saveLayout(JSON.parse(singlePost));
//   }
// };

// var saveLayout = (post) => {
//   var $container = $("#container");
//   var $layoutPost = $("<h1>" + post.title + "</h1><p>" + post.body + "</p>");
//   $container.append($layoutPost);
// };

// var myCall = () => {
//   $.ajax({
//     type: "GET",
//     url: "https://jsonplaceholder.typicode.com/posts/1",
//   })
//     .done(function (response) {
//       saveLayout(response);
//       localStorage.setItem("mieiArticoli", JSON.stringify(response));
//     })
//     .fail(function (error) {
//       console.log(error);
//     });
// };

// myProgram();

var saveDelayLayout = (post) => {
  let $divPost = $("<div></div>");
  $divPost.attr("id", post.id);
  let $layoutPost = $("<h1>" + post.title + "</h1><p>" + post.body + "</p>");
  return $layoutPost;
};

var saveLayout = (posts) => {
  var $container = $("#container");
  for (var i = 0; i < posts.length; i++) {
    var $singlePost = saveDelayLayout(posts[i]);
    $container.append($singlePost);
  }
};

(function () {
  var post = _getPost();
  post.done(function (result) {
    saveLayout(result);
  });
})();
