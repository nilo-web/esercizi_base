"use strict";

/*
Concatena, senza duplicati, i seguenti array e ordinali per valore crescente
```javascript
var array1 = [20, 33, 12, 1233];
vararray2 = [1, 33, 102, -12];
*/
var array1 = [20, 33, 12, 1233];
var array2 = [1, 33, 102, -12];
for (var i = 0; i < array1.length; i++) {
  if (array2.indexOf(array1[i]) > -1) {
    var pos = array2.indexOf(array1[i]);
    array2.splice(pos, 1);
  }
}
var array3 = array1.concat(array2).sort((a, b) => {
  return a - b;
});
console.log(array3);

//Fai il reverse di questa string: “supercalifragilistichespiralidoso”

var reversString = "supercalifragilistichespiralidoso";
reversString = reversString.split("").reverse().join("");
console.log(reversString);

/*

 Ordina il seguente array con i giorni della settimana in base al giorno della corrente:
```javascript
var giorniSettimana = ["lunedi", "martedi", "mercoledi", "giovedi", "venerdi", "sabato", "domenica"];*/

var giorniSettimana = [
  "lunedi",
  "martedi",
  "mercoledi",
  "giovedi",
  "venerdi",
  "sabato",
  "domenica",
];
var oggi = new Date();
var currentDay = oggi.getDay();
var posCurrentDay = currentDay - 1;
var updateGiorniSettimana = giorniSettimana.slice(
  posCurrentDay,
  giorniSettimana.length
);
var oldGiorniSettimana = giorniSettimana.slice(0, posCurrentDay);
for (var i = 0; i < oldGiorniSettimana.length; i++) {
  updateGiorniSettimana.push(oldGiorniSettimana[i]);
}
console.log(updateGiorniSettimana);

/**
 * Ordina il seguente array di oggetti in base all’id:
```javascript
var objectsList = [
 { name: "Pino", id: 66 },
 { name: "Rino", id: 16 },
 { name: "Dino", id: 6 },
 { name: "Lino", id: 96 },
 { name: "Gino", id: 26 }
];
 */

var objectsList = [
  { name: "Pino", id: 66 },
  { name: "Rino", id: 16 },
  { name: "Dino", id: 6 },
  { name: "Lino", id: 96 },
  { name: "Gino", id: 26 },
];

var newSortObject = objectsList.sort((a, b) => {
  var pA = a.id;
  var pB = b.id;
  return pA - pB;
});

console.log(newSortObject);

//Crea una funzione che stampi la data corrente in questo formato, es: "martedi 22, febbraio 2021"
function formatDate() {
  var giorni = [
    "lunedi",
    "martedi",
    "mercoledi",
    "giovedi",
    "venerdi",
    "sabato",
    "domenica",
  ];
  var mesi = [
    "Gennaio",
    "Febbraio",
    "Marzo",
    "Aprile",
    "Maggio",
    "Giugno",
    "Luglio",
    "Agosto",
    "Settembre",
    "Ottobre",
    "Novembre",
    "Dicembre",
  ];
  var data = new Date();
  var day = giorni[data.getDay() - 1];
  var month = mesi[data.getMonth()];
  var newFormatData = `${day} ${data.getDate()}, ${month} ${data.getFullYear()}`;
  console.log(newFormatData);
}
formatDate();

// Crea una funzione che ritorni la differenza tra 2 date in giorni (usare Date.UTC())

function diffDate() {
  var data1 = new Date(Date.UTC(21, 2, 2));
  var data2 = new Date(Date.UTC(20, 2, 16));
  var dateDiff = data2 - data1;
  var dayTime = 1000 * 60 * 60 * 24;
  console.log(dateDiff / dayTime);
  return dateDiff / dayTime;
}
diffDate();

// crea una funzione che data un array di date ritorni la data più alta

function maxDate() {
  var dateArray = ["02/10/2021", "03/04/2021", "05/22/2020", "07/16/2018"];
  var maxValueDate = "";
  for (var i = 0; i < dateArray.length; i++) {
    if (maxValueDate === "") {
      maxValueDate = new Date(dateArray[i]);
    } else {
      let newDate = new Date(dateArray[i]);
      maxValueDate = newDate > maxValueDate ? newDate : maxValueDate;
    }
  }
  console.log(maxValueDate);
  return maxValueDate;
}
maxDate();
