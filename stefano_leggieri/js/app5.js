function myFunction(firstColor, secondColor) {
  // container dei bottoni
  var myContainer = document.getElementById("container");

  // container dei p
  var myTextContainer = document.getElementById("textContainer");

  // genero il primo bottone
  var myButton = document.createElement("button");
  myButton.innerText = "Click here";
  myContainer.appendChild(myButton);

  // gestione colori
  let myLastColor = firstColor;

  var c = 0;

  // eventListener per aggiungere un elemento p
  myButton.addEventListener("click", function () {
    c++;

    // creo l'elemento
    var myParagraph = document.createElement("p");
    myParagraph.innerText = "Hello world! " + c;
    myParagraph.setAttribute("data-id", c);
    myParagraph.style.color = myLastColor;

    myTextContainer.appendChild(myParagraph);

    // gestione colori
    if (myLastColor === firstColor) {
      myLastColor = secondColor;
    } else {
      myLastColor = firstColor;
    }
  });

  myTextContainer.addEventListener("click", function (event) {
    alert("elemento cliccato " + event.target.getAttribute("data-id"));
  });
}

myFunction("green", "yellow");
