"use strict";

// Esercizio 1 (Inizialmente la pagina è vuota, ma ogni volta che clicco sul pulsante compare una nuova riga di testo. "sono la frase aggiunta numero [n]")

var bottone1 = document.getElementById("btn1");
var container1 = document.getElementById("container1");
var count1 = 1;
bottone1.addEventListener("click", function () {
  var newParagraph = document.createElement("p");
  newParagraph.innerText = "sono la frase aggiunta numero " + count1;
  container1.appendChild(newParagraph);
  count1++;
});

// Esercizio 2 (Inizialmente la pagina è vuota, ma ogni volta che clicco sul pulsante compare una nuova riga di testo, ma le righe saranno una volta rosse e una volta blu.)

var bottone2 = document.getElementById("btn2");
var container2 = document.getElementById("container2");
var count2 = 1;
bottone2.addEventListener("click", function () {
  var newParagraph = document.createElement("p");
  var classColor = count2 % 2 ? "red" : "blue";
  newParagraph.setAttribute("class", classColor);
  newParagraph.innerText = "sono la frase aggiunta numero " + count2;
  container2.appendChild(newParagraph);
  count2++;
});

// Esercizio 3 (Mettere tutto quello fatto nell'esercizio precedente dentro una funzione. Quindi invocare la funzione due volte: il risultato deve essere lo stesso dell'esercizio 2.)

var contatore3 = ((n) => {
  return () => {
    return ++n;
  };
})(0);

var esercizio3Function = () => {
  var container3 = document.getElementById("container3");
  var count = contatore3();
  var newParagraph = document.createElement("p");
  var classColor = count % 2 ? "red" : "blue";
  newParagraph.setAttribute("class", classColor);
  newParagraph.innerText = "sono la frase aggiunta numero " + count;
  container3.appendChild(newParagraph);
};

var bottone3 = document.getElementById("btn3");
bottone3.addEventListener("click", function () {
  esercizio3Function();
});

//Esercizio 4 (Fare in modo che la funzione creata nel punto precedente prenda in ingresso due parametri: devono essere entrambi delle stringhe che rappresenteranno i due colori.
// Quindi invocare la funzione una unica volta passandogli i colori "green" e "yellow".)

var contatore4 = ((n) => {
  return () => {
    return ++n;
  };
})(0);

var esercizio4Function = (color1, color2) => {
  var container4 = document.getElementById("container4");
  var count = contatore4();
  var newParagraph = document.createElement("p");
  // Qui utilizzo un metodo di assegnazione dei colori senza usare la classe
  newParagraph.style.color = count % 2 ? color1 : color2;
  newParagraph.innerText = "sono la frase aggiunta numero " + count;
  container4.appendChild(newParagraph);
};

var bottone4 = document.getElementById("btn4");
bottone4.addEventListener("click", function () {
  esercizio4Function("yellow", "green");
});
