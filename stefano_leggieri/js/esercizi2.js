//*   Scrivi una funzione che ritorna una funziona che ritorna il numero che è stato passato alla prima funzione.
//es test(7)() //  7.

var simpleNum = Math.random();

function genericNumber(num) {
  return () => {
    return num.toFixed(2);
  };
}

var newNum = genericNumber(simpleNum);

console.log(newNum());

/**
 * Scrivi una funzione che ritorni una funzione che ritoni il numero incrementato che è stato passato alla prima funzione.
 * Ogni volta che la si chiama il numero viene incrementato di uno
 */

var simpleNum2 = 5;

function autoIncrementNumber(num) {
  return () => {
    return ++num;
  };
}

var autoNum = autoIncrementNumber(simpleNum2);

for (var i = 0; i < 5; i++) {
  console.log(autoNum());
}

/**
 * Scrivi una funzione che ritorna un numero e lo incrementi ogni volta che viene chiamata (IFFE)
 */

var incrementNum = ((n) => {
  var simpleNum3 = n;
  return () => {
    return ++simpleNum3;
  };
})(3);

console.log(incrementNum());
console.log(incrementNum());
console.log(incrementNum());

/**
 * Scrivi una funzione che ogni volta che viene chiamata ritorni il numero successivo della serie di fibonacci
 */

var getFibonaciNumber = ((index) => {
  var simpleNum4 = [
    0,
    1,
    1,
    2,
    3,
    5,
    8,
    13,
    21,
    34,
    55,
    89,
    144,
    233,
    377,
    610,
    987,
    1597,
    2584,
    4181,
    6765,
    10946,
    17711,
    28657,
    46368,
    75025,
    121393,
    196418,
    317811,
  ]; // sequenza fibonacci
  return () => {
    var newNum = simpleNum4[index];
    index++;
    return newNum;
  };
})(0);

console.log(getFibonaciNumber());
console.log(getFibonaciNumber());
console.log(getFibonaciNumber());
