"use strict";

// Modificare l'esercizio precedente in modo inserire un altro pulsante: se lo schiaccio devo cancellare tutte le righe inserite.

/*
var container = document.getElementById("container");

var contatore = ((n) => {
  return () => {
    return ++n;
  };
})(0);

var esercizio4Function = (color1, color2) => {
  var count = contatore();
  var newParagraph = document.createElement("p");
  newParagraph.style.color = count % 2 ? color1 : color2;
  newParagraph.innerText = "sono la frase aggiunta numero " + count;
  container.appendChild(newParagraph);
};

var button = document.getElementById("btn");
button.addEventListener("click", function () {
  esercizio4Function("yellow", "green");
});
var containerNode = document.getElementById("container").parentNode;

var newButton = document.createElement("button");
newButton.innerText = "Cancella";
containerNode.insertBefore(newButton, container);

newButton.addEventListener("click", () => {
  var listP = document.getElementsByTagName("p");
  for (var i = listP.length - 1; i > -1; i--) {
    container.removeChild(listP[i]);
  }
});

// creare una funzione che prende in ingresso un container e lo pulisce da tutti i suoi figli. Invocarla nell'esercizio precedente quando ci serve pulire il div che contiene le scritte.

var container = document.getElementById("container");

var contatore = ((n) => {
  return () => {
    return ++n;
  };
})(0);

var esercizio4Function = (color1, color2) => {
  var count = contatore();
  var newParagraph = document.createElement("p");
  newParagraph.style.color = count % 2 ? color1 : color2;
  newParagraph.innerText = "sono la frase aggiunta numero " + count;
  container.appendChild(newParagraph);
};

var button = document.getElementById("btn");
button.addEventListener("click", function () {
  esercizio4Function("yellow", "green");
});
var containerNode = document.getElementById("container").parentNode;

var newButton = document.createElement("button");
newButton.innerText = "Cancella";
containerNode.insertBefore(newButton, container);

function deleteContainer(parentContainer) {
  var listP = parentContainer.childNodes;
  for (var i = listP.length - 1; i > -1; i--) {
    let singleChild = listP[i];
    singleChild.remove();
  }
}

newButton.addEventListener("click", () => {
  deleteContainer(container);
});

//Aggiungere un terzo pulsante. Questo pulsante, quando calcato, cambierà il colore di sfondo del div che contiene il testo.


var container = document.getElementById("container");

var contatore = ((n) => {
  return () => {
    return ++n;
  };
})(0);

var esercizio4Function = (color1, color2) => {
  var count = contatore();
  var newParagraph = document.createElement("p");
  newParagraph.style.color = count % 2 ? color1 : color2;
  newParagraph.innerText = "sono la frase aggiunta numero " + count;
  container.appendChild(newParagraph);
};

var button = document.getElementById("btn");
button.addEventListener("click", function () {
  esercizio4Function("yellow", "green");
});
var containerNode = document.getElementById("container").parentNode;

var newButton = document.createElement("button");
newButton.innerText = "Cancella";
containerNode.insertBefore(newButton, container);

function deleteContainer(parentContainer) {
  var listP = parentContainer.childNodes;
  for (var i = listP.length - 1; i > -1; i--) {
    let singleChild = listP[i];
    singleChild.remove();
  }
}

newButton.addEventListener("click", () => {
  deleteContainer(container);
});

var newButtonBG = document.createElement("button");
newButtonBG.innerText = "Cambia Sfondo";
containerNode.insertBefore(newButtonBG, container);

newButtonBG.addEventListener("click", () => {
  container.style.backgroundColor = "#ff0000";
});
*/

//Fare in modo che il terzo pulsante appena aggiunto, se ricalcato una seconda volta, ripristini il colore di sfondo del testo a bianco.

/*
var container = document.getElementById("container");

var contatore = ((n) => {
  return () => {
    return ++n;
  };
})(0);

var esercizio4Function = (color1, color2) => {
  var count = contatore();
  var newParagraph = document.createElement("p");
  newParagraph.style.color = count % 2 ? color1 : color2;
  newParagraph.innerText = "sono la frase aggiunta numero " + count;
  container.appendChild(newParagraph);
};

var button = document.getElementById("btn");
button.addEventListener("click", function () {
  esercizio4Function("yellow", "green");
});
var containerNode = document.getElementById("container").parentNode;

var newButton = document.createElement("button");
newButton.innerText = "Cancella";
containerNode.insertBefore(newButton, container);

function deleteContainer(parentContainer) {
  var listP = parentContainer.childNodes;
  for (var i = listP.length - 1; i > -1; i--) {
    let singleChild = listP[i];
    singleChild.remove();
  }
}

newButton.addEventListener("click", () => {
  deleteContainer(container);
});

var newButtonBG = document.createElement("button");
newButtonBG.innerText = "Cambia Sfondo";
containerNode.insertBefore(newButtonBG, container);

newButtonBG.addEventListener("click", () => {
  if (container.classList.contains("bgColorRed")) {
    container.classList.remove("bgColorRed");
  } else {
    container.classList.add("bgColorRed");
  }
});

*/

// Inserire un quarto pulsante "Autodistruzione". Questo pulsante elimina tutto il contenuto della pagina, compreso se stesso.

var container = document.getElementById("container");

var contatore = ((n) => {
  return () => {
    return ++n;
  };
})(0);

var esercizio4Function = (color1, color2) => {
  var count = contatore();
  var newParagraph = document.createElement("p");
  newParagraph.style.color = count % 2 ? color1 : color2;
  newParagraph.innerText = "sono la frase aggiunta numero " + count;
  container.appendChild(newParagraph);
};

var button = document.getElementById("btn");
button.addEventListener("click", function () {
  esercizio4Function("yellow", "green");
});
var containerNode = document.getElementById("container").parentNode;

var newButton = document.createElement("button");
newButton.innerText = "Cancella";
containerNode.insertBefore(newButton, container);

function deleteContainer(parentContainer) {
  var listP = parentContainer.childNodes;
  for (var i = listP.length - 1; i > -1; i--) {
    let singleChild = listP[i];
    singleChild.remove();
  }
}

newButton.addEventListener("click", () => {
  deleteContainer(container);
});

var newButtonBG = document.createElement("button");
newButtonBG.innerText = "Cambia Sfondo";
containerNode.insertBefore(newButtonBG, container);

newButtonBG.addEventListener("click", () => {
  if (container.classList.contains("bgColorRed")) {
    container.classList.remove("bgColorRed");
  } else {
    container.classList.add("bgColorRed");
  }
});

var newButtonDestroy = document.createElement("button");
newButtonDestroy.innerText = "Distruggi tutto";
containerNode.insertBefore(newButtonDestroy, container);

newButtonDestroy.addEventListener("click", () => {
  while (document.body.childNodes.length > 0) {
    document.body.childNodes[0].remove();
  }
});
