function _getPost() {
  var $posts = localStorage.getItem("mieiArticoli");

  if (!$posts) {
    var $call = $.ajax({
      type: "GET",
      url: "https://jsonplaceholder.typicode.com/posts/",
      success: function (response) {
        localStorage.setItem("mieiArticoli", JSON.stringify(response));
      },
    });

    return $call;
  } else {
    var $deferred = $.Deferred();
    $deferred.resolve(JSON.parse($posts));
    return $deferred;
  }
}
