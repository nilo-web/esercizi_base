"use strict";

var myApp = angular.module("myApp", []);

myApp.factory("salutaFactory", function () {
  return {
    saluta: function () {
      return "ciao Pino factory";
    },
  };
});

myApp.service("salutaService", function () {
  this.saluta = function () {
    return "ciao pino service";
  };
});

myApp.provider("salutaPrv", function () {
  this.$get = function () {
    var name = this.name;
    return {
      saluta: function () {
        console.log("ciao " + name + " provider");
        return "ciao " + name + " provider";
      },
    };
  };
  this.name = "Default";
  this.setName = function (name) {
    this.name = name;
  };
});

myApp.config(function (salutaPrvProvider) {
  salutaPrvProvider.setName("Pino");
});

myApp.controller(
  "myController",
  function ($scope, salutaFactory, salutaService, salutaPrv) {
    $scope.salutaF = salutaFactory.saluta();
    $scope.salutaS = salutaService.saluta();
    $scope.salutaP = salutaPrv.saluta();

    $scope.persona = {
      nome: "Pino",
    };

    $scope.$watch(
      "persona",
      function (newValue, oldValue) {
        console.log("watch persona", newValue, oldValue);
      },
      true
    );

    // $scope.user = {
    //   name: "Pino",
    //   surname: "Pini",
    // };

    // $scope.raddoppiaAction = () => {
    //   $scope.risultato = $scope.numero * 2;
    // };

    // $scope.$watch("risultato", function (newValue, oldValue) {
    //   if (newValue !== oldValue) {
    //     console.log("watch risultato", newValue, oldValue);
    //   }
    // });

    // setTimeout(() => {
    //   $scope.user.name = "Giacomo";
    //   console.log("timeout", $scope.user);

    //   $scope.$apply(function () {
    //     $scope.user.name = "Giacomo";
    //   });
    // }, 3000);
  }
);

myApp.controller("salutaCtrl", function ($scope, $rootScope) {
  console.log($rootScope);
  $scope.saluta = (user) => {
    return "Ciao " + user.name + " " + user.surname;
  };
});
