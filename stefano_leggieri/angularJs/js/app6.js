"use strict";

var myApp = angular.module("myApp", []);

myApp.directive("titolo", function () {
  return {
    restirct: "E",
    template: "<h1>Sono un titolo</h1>",
  };
});
myApp.directive("mySelectCity", function () {
  return {
    restirct: "E",
    templateUrl: "/mySelectCityTemplate.html",
    scope: {
      elencoCitta: "=cityList",
    },
  };
});

myApp.controller("myController", function ($scope) {
  $scope.elencoCitta = [
    { nome: "Roma", regione: "Lazio" },
    { nome: "Latina", regione: "Lazio" },
    { nome: "Milano", regione: "Lombardia" },
    { nome: "Napoli", regione: "Campania" },
    { nome: "Como", regione: "Lombardia" },
    { nome: "Palermo", regione: "Sicilia" },
    { nome: "Caserta", regione: "Campania" },
    { nome: "Avellino", regione: "Campania" },
    { nome: "Trapani", regione: "Sicilia" },
    { nome: "Agrigento", regione: "Sicilia" },
  ];
});
