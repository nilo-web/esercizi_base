"use strict";

var myApp = angular.module("myApp", []);
//i controller non devono mai manipolare il Dom, non devono formattare gli input, non devono formattare l'output
// utilizzarlo per prendere i dati passati per esempio da un servizio e passarli alla view
myApp.controller("formController", function ($scope, $rootScope) {
  console.log($scope, $rootScope);
  $scope.user = {
    name: "Stefano",
    surname: "Leggieri",
    username: "Sayrus",
    email: "sleveris@gmail.com",
  };

  $scope.saluta = function () {
    return "ciao";
  };

  $scope.mostramiMessaggio = function (name) {
    alert("messaggio" + name);
  };

  $scope.listaColori = ["rosso", "verde", "giallo"];
});
myApp.controller("textController", function ($scope) {
  console.log("entrato nel textController");
  $scope.text = "testo già bello che scritto";

  $scope.colore = "rosso";
});
