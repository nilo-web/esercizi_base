"use strict";

var myApp = angular.module("myApp", []);

//i controller non devono mai manipolare il Dom, non devono formattare gli input, non devono formattare l'output
// utilizzarlo per prendere i dati passati per esempio da un servizio e passarli alla view
myApp.controller("myController", function ($scope) {
  $scope.users = listaUtenti;
  $scope.nameUser = "";
  $scope.manina = "manina";
  $scope.utenteScelto = "";
  $scope.mostraPost = function (userId, userName) {
    if ($scope.utenteScelto !== userId) {
      $scope.posts = [];
      for (const index in listPosts) {
        if (listPosts[index].userId === userId) {
          var tmpPost = {
            id: listPosts[index].id,
            title: listPosts[index].title,
          };
          $scope.posts.push(tmpPost);
        }
      }
      $scope.nameUser = userName;
      $scope.utenteScelto = userId;
    } else {
      $scope.utenteScelto = "";
      $scope.posts = [];
    }
  };
  $scope.close = function () {
    $scope.utenteScelto = "";
    $scope.posts = [];
  };

  $scope.countComments = (postId) => {
    var count = 0;
    for (const n in listComments) {
      if (postId === listComments[n].postId) {
        count++;
      }
    }
    return count;
  };
});
