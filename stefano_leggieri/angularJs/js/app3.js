"use strict";

var myApp = angular.module("myApp", []);
myApp.controller("myController", function ($scope) {
  $scope.numero = 3.14568;
  $scope.adesso = new Date();
  $scope.numeriACaso = [4, 10, 1, 3];
  $scope.paroleACaso = ["sabbia", "arancia", "cassa", "barca"];

  $scope.elencoCitta = [
    { nome: "Napoli", regione: "Campania" },
    { nome: "Milano", regione: "Lombardia" },
    { nome: "Lecce", regione: "Puglia" },
    { nome: "Roma", regione: "Lazio" },
    { nome: "Firenze", regione: "Toscana" },
    { nome: "Aquila", regione: "Abbruzzo" },
  ];
});
