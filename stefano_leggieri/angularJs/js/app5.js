"use strict";

var myApp = angular.module("myApp", []);

myApp.filter("capitalize", function () {
  return function (value) {
    console.log("valore filtro", value);
    var result = value;
    if (value && isNaN(value)) {
      result = value.charAt(0).toUpperCase() + value.substr(1);
    }
    return result;
  };
});

myApp.factory("servizioPromise", function ($q) {
  var objectDeferred = $q.defer();

  setTimeout(() => {
    objectDeferred.resolve("Pino");
  }, 2000);

  return objectDeferred.promise;
});

myApp.controller("myController", function ($scope, $http, servizioPromise) {
  $scope.utente = { nome: "mario", cognome: "Rossi" };

  $scope.listaPost;

  $http.get("https://jsonplaceholder.typicode.com/users").then(
    function (responseData) {
      console.log(responseData);
      $scope.listaPost = responseData.data;
    },
    function (error) {}
  );

  servizioPromise.then(
    function (data) {
      console.log("Promessa ", data);
    },
    function (error) {}
  );
});
