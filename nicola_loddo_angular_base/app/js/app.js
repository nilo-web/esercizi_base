"use strict";

var myApp = angular.module("myApp", ["ngRoute"]);

myApp.config(function($routeProvider) {
  $routeProvider
  .when("/", {
    templateUrl: "../templates/login.html",
    controller: "loginController"
  })
  .when("/profile/:username", {
    templateUrl: "/templates/profile.html",
    controller: "profileController"
  })
  .otherwise({
    redirectTo: "/"
  })
});


myApp.controller("loginController", function($scope, $location) {
  $scope.loggami = function(username) {
    $location.path("/profile/" + username);
  }
});


myApp.controller("profileController", ["$scope", "$routeParams", function($scope, $routeParams) {
  $scope.username = $routeParams.username;
}]);