'use strict';

// concatenare ed ordinare i seguenti array, in ordine crescente. (ignorando i duplicati)

let array1 = [20, 33, 12, 1233];
let array2 = [1, 33, 102, -12];

// creo una funzione per ciclare tra i valori dell'array ed eliminare eventuali duplicati con il metodo split.

function eliminaDuplicati(array1, array2) {
    for (let i = 0; i <= array1.length; i++) {
        for (let j = 0; j <= array2.length; j++) {
            if (array1[i] == array2[j]) {
                array1 = array1.splice(i, 1);
            } 
        }
    }
}

// richiamo la funziona sopradescritta per eliminare i duplicati.

eliminaDuplicati(array1,array2);

//utilizzo il metodo concat e sort per ordinare ed unire i due array in uno solo.

let arraySorted = array1.concat(array2).sort(function(a, b) {
    return a - b;
});

/*
SECONDO ESERCIZIO
*/


// fai il reverse di questa string
let string = "supercalifragilistichespiralidoso";
    var newString = "";
    var y = string.length - 1;
    for (let i = y; i > 0; i-- ) {
        newString += string.charAt(i);
    }


/*
TERZO ESERCIZIO
*/

// ordina il seguente array con i giorni della settimana in base al giorno corrente

let giorniSettimana = ["lunedi", "martedi", "mercoledi", "giovedi", "venerdi", "sabato", "domenica"];

let giornoCorrente = new Date().getDay() - 1;

    var giorniOrdinati = [];
    for (let i = giornoCorrente; i < giorniSettimana.length; i++) {
        giorniOrdinati.push(giorniSettimana[i]);
    }
    for (let i = 0; i < giornoCorrente; i++) {
        giorniOrdinati.push(giorniSettimana[i]);
    } 

/*
QUARTO ESERCIZIO
*/

// Ordina il seguente array di oggetti in base all'id

let objectsList = [
    { name: "Pino", id: 66 },
    { name: "Rino", id: 16 },
    { name: "Dino", id: 6 },
    { name: "Lino", id: 96 },
    { name: "Gino", id: 26 }
   ];

   objectsList.sort(function(a,b) {
       return a.id - b.id
   });


   /*
   QUINTO ESERCIZIO
   */ 

   // crea una funziona che stampi la data corrente in questo formato, es: "martedi 22, febbraio 2021"

   let giorni = ["Domenica", "Lunedi", "Martedi", "Mercoledi", "Giovedi", "Venerdi", "Sabato"];
   let mesi = ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"];
   let d = new Date();
   let data = giorni[d.getDay()] + " " + d.getDate() + ", " + mesi[d.getMonth()] + " " + d.getFullYear();

   /*
   SESTO ESERCIZIO
   */

   // crea una funzione che ritorni la differenza tra 2 date in giorni (usare la date.UTC())

   const data1 = new Date(Date.UTC(2002, 10, 25, 14, 47, 54, 574));
   const data2 = new Date(Date.UTC(92, 6, 3, 6, 28, 31, 213));

   function dateDiff(date1, date2) {
    let diff;
    if (data1.getFullYear < data2.getFullYear) {
        diff = date2 - date1;


    } else if (data2.getFullYear = data2.getFullYear) {
        diff = date1 - date2;
    } else {
        diff = date1 - date2;
    }
    diff = Math.floor(diff / 86400000);
    return diff + " Giorni";
   }

   /*
   SETTIMO ESERCIZIO
   */

   // crea una funzione che dato un array di date, calcoli la data più alta

   let dateArray = [new Date(2015, 11, 24), new Date(1997, 12, 15), new Date(2020, 4, 7)];

   function maxData(array) {
       let dataMax = dateArray[0].getTime();
       for (let x in dateArray) {
            if (dateArray[x].getTime() > dataMax) dataMax = dateArray[x].getTime();
       }
       return new Date(dataMax);
   }

