"use strict";

function myFunction(firstColor, secondColor) {
  // container dei bottoni
  var myContainer = document.getElementById("container");

  // container dei p
  var myTextContainer = document.getElementById("textContainer");

  // genero il primo bottone
  var myButton = document.createElement("button");
  myButton.innerText = "Click here";
  myContainer.appendChild(myButton);

  // genero il secondo bottone
  var clearBtn = document.createElement("button");
  clearBtn.innerText = "Reset";
  myContainer.appendChild(clearBtn);

  // gestione colori
  let myLastColor = firstColor;

  var c = 0;
  
  // eventListener per aggiungere un elemento p
  myButton.addEventListener("click", function () {

      c++;
      
      // creo l'elemento
      var myParagraph = document.createElement("p");
      myParagraph.innerText = "Hello world! " + c;
      myParagraph.setAttribute("id", c);
      myParagraph.style.color = myLastColor;


      myTextContainer.appendChild(myParagraph);

      // gestione colori
      if (myLastColor === firstColor) {
          myLastColor = secondColor;
      } 
      else {
          myLastColor = firstColor;
      }
  });
  
  // eventListener su container padre
  myTextContainer.addEventListener("click", function (event) {
        
    alert("ciao sono l'elemento" + event.target.getAttribute("id"));

  });

  // eventListener per cancellazione nodi
  clearBtn.addEventListener("click", function() {
    // utilizzo il while mettendo come condizione il fatto che la lunghezza dei childNodes 
    // sia uguale a zero in modo da non avere problemi con la rimozione degli elementi 
    // a causa del cambiamento degli index
    while (myTextContainer.childNodes.length > 0) {
      myTextContainer.childNodes[0].remove();
    }
  });

}

myFunction("green", "yellow");
