'use strict';

// PRIMO ESERCIZIO

/* Scrivi una funzione che ritorna una funziona che ritorna il numero che è stato passato alla prima funzione.
es test(7)()   7.
*/

function firstFunction(num) {
    return function() {
        return num;
    } 
}
var display = firstFunction(15);
//console.log(display()); 


// SECONDO ESERCIZIO

/* Scrivi una funzione che ritorni una funzione che ritorni il numero
incrementato che è stato passato alla prima funzione. 
Ogni volta che la si chiama il numero viene incrementato di uno
*/

function secondFunction(num) {
    return function() {
        num++;
        return num;
    }
}

var display2 = secondFunction(14);
//console.log(display2());

// TERZO ESERCIZIO

// Scrivi una funzione che ritorna un numero e lo incrementi ogni volta che viene chiamata (IFFE)

var display3 = (function thirdFunction() {
    var num = 10;
    return function() {
        num++;
        return num;
        }
})();

// QUARTO ESERCIZIO

/* Scrivi una funzione che ogni volta che viene chiamata ritorni 
il numero successivo della serie di fibonacci
*/

var display4 = (function fibonacci() {
    var x = 1;
    var xBefore = 1;
    console.log(x);
    console.log(x);
    return function() { 
        var y = xBefore;
        xBefore = x;
        x += y;  
        return x;    
    }
})();