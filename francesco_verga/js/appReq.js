"use strict";

/* funzione autodichiarante che mi controlla di avere i valori richiesti
*  in localStorage, se non ci sono li inserisce
*/

if (localStorage.getItem("post") == null) {
    (function sendGet() {
        console.log("inserito!");
        $.ajax({
            type: "GET",
            url: "https://jsonplaceholder.typicode.com/users"
        })
        .done(function(response) {
            for ( let i = 0; i < response.length; i++) {
               localStorage.setItem("post " + i, response[i].name);
            }
        })
        .fail(function(response) {
            alert("GET fallita con errore: " + response.status);
        })
        ;
        })();
} else {
    console.log("gia presente");
}

//creo la lista ed i bottoni

var $lista = $("<ul></ul>");
var $button = $("<button>Print me the data</button>");
var $button2 = $("<button>Insert data</button>");
var $button3 = $("<button>Print data from LocalStorage</button>")
var $container = $("#container");

//appendo i bottoni e la lista al container <div>

$container.append($button);
$container.append($button2);
$container.append($button3);
$container.append($lista);

//aggiungo ai bottoni i rispettivi actionListener

$button.on("click", function () {
    sendGet();
});

$button2.on("click", function() {
    sendPost();
})

$button3.on("click", function() {
    let l = localStorage.length - 1;
    for ( let i = 0; i < l; i++) {
        var $nuovoLi = $("<li> <strong>nome:</strong> " + 
        localStorage.getItem("post " + i) + "</li>");
        $lista.append($nuovoLi);
     }
})

/* definisco le funzioni che andranno utilizzate negli actionListener
*  dei bottoni
*/

// funzione HTML GET con JQuery

function sendGet() {
$.ajax({
    type: "GET",
    url: "https://jsonplaceholder.typicode.com/users"
})
.done(function(response) {
    for ( let i = 0; i < response.length; i++) {
       var $nuovoLi = $("<li> <strong>nome:</strong> " + response[i].name + 
       "<br> <strong>Email:</strong> " + response[i].email + "</li>");
       $lista.append($nuovoLi);
    }
})
.fail(function(response) {
    alert("GET fallita con errore: " + response.status);
})
;
}

// funzione HTML POST con JQuery

function sendPost() {
    $.ajax({
        type: "POST",
        url: "https://jsonplaceholder.typicode.com/users",
        dataType: "json",
        data: JSON.stringify({
            name: "Leanne Graham",
            username: "Bret",
            email: "Sincere@april.biz",
            address: {
              street: "Kulas Light",
              suite: "Apt. 556",
              city: "Gwenborough",
              zipcode: "92998-3874",
              geo: {
                lat: "-37.3159",
                lng: "81.1496"
              }
            },
            phone: "1-770-736-8031 x56442",
            website: "hildegard.org",
            company: {
              name: "Romaguera-Crona",
              catchPhrase: "Multi-layered client-server neural-net",
              bs: "harness real-time e-markets",
            }
        })
    })
    .done(function(response) {
        alert("POST effettuata");
    })
    .fail(function(response) {
        alert("POST fallita con errore: " + response.status);
    });
}



