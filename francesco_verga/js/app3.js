'use strict';

/* Tutti gli esercizi sono quotati come commenti cosi da poter decidere al momento quale
testare.
*/

//PRIMO ESERCIZIO

// Inizialmente la pagina è vuota, ma ogni volta che clicco sul pulsante compare una nuova riga di testo.

/*
var n = 0;
var button1 = document.getElementById("btn");
button1.addEventListener("click", function() {
    n++;
    var txt = document.createElement("p");
    txt.innerHTML = "nuova linea " + n;
    document.body.appendChild(txt);
})
*/

//SECONDO ESERCIZIO

/* Inizialmente la pagina è vuota, ma ogni volta che clicco sul pulsante compare una nuova riga di testo, 
ma le righe saranno una volta rosse e una volta blu.
*/

/*
var funzione = (function() {
        var n = 0;
        var txt;
        return function() { 
            n++;
            txt = document.createElement("p");
            if ((n % 2) === 0) {
             txt.style.color = "red";   
            } else {
                txt.style.color = "green";
            }
            document.body.appendChild(txt);
            txt.innerHTML = "nuova linea " + n; 
        }
    })();

var button1 = document.getElementById("btn");
button1.addEventListener("click", funzione);
*/

// TERZO ESERCIZIO

/* Mettere tutto quello fatto nell'esercizio precedente dentro una funzione. 
Quindi invocare la funzione due volte: il risultato deve essere lo stesso dell'esercizio 2.
*/

/*
var funzione = (function() {
    var n = 0;
    var txt;
    return function() { 
        n++;
        txt = document.createElement("p");
        if ((n % 2) === 0) {
         txt.style.color = "red";   
        } else {
            txt.style.color = "green";
        }
        document.body.appendChild(txt);
        txt.innerHTML = "nuova linea " + n; 
    }
})();

var button1 = document.getElementById("btn");
button1.addEventListener("click", function() {
    funzione();
    funzione();
});
*/

// QUARTO ESERCIZIO

/* Fare in modo che la funzione creata nel punto precedente prenda in ingresso due parametri: 
devono essere entrambi delle stringhe che rappresenteranno i due colori. 
Quindi invocare la funzione una unica volta passandogli i colori "green" e "yellow".
*/


var funzione = (function() {
    var n = 0;
    var txt;
    return function(string1, string2) {         // passare alla funzione, come argomenti, i due colori dei paragrafi.
        n++;
        txt = document.createElement("p");
        if ((n % 2) === 0) {
         txt.style.color = string1;   
        } else {
            txt.style.color = string2;
        }
        var container = document.getElementById("container2");
        container.appendChild(txt);
        txt.innerHTML = "nuova linea " + n; 
    }
})();

var button1 = document.getElementById("btn");
button1.addEventListener("click", function() {
    funzione("green", "yellow")
});

// QUINTO ESERCIZIO


/*
*   Modificare l'esercizio precedente in modo inserire un altro pulsante: se lo schiaccio devo cancellare tutte le righe inserite.
*

var newBtn = document.createElement("button");
newBtn.innerText = "Cancella!"
var container2 = document.getElementById("container2");
container2.append(newBtn);
newBtn.addEventListener("click", function() {
    var txt = document.getElementsByTagName("p");
    if (txt.length > 0) {
        var p = container2.getElementsByTagName("p");
        for ( let i = p.length - 1; i <= 0 ; i--) {
         p[i].remove();
        }
    }
})

// SESTO ESERCIZIO

/*
*creare una funzione che prende in ingresso un container e lo pulisce da tutti i suoi figli. 
*Invocarla nell'esercizio precedente quando ci serve pulire il div che contiene le scritte.
*/

function cleanContainer(containerName) {
    var txt = document.getElementsByTagName("p");
    if (txt.length > 0) {
        var container2 = document.getElementById(containerName);
        container2.remove();
    }
}
var newBtn = document.createElement("button");
newBtn.innerText = "Cancella!"
var container = document.getElementById("container");
container.append(newBtn);
newBtn.addEventListener("click", function() {
    cleanContainer("container2")
});

// SETTIMO ESERCIZIO
/*
*   Aggiungere un terzo pulsante. Questo pulsante, quando calcato, cambierà il colore di sfondo del div che contiene il testo.
*

var btn3 = document.createElement("button");
btn3.innerText = "Cambia Sfondo";
container.append(btn3);
btn3.addEventListener("click", function() {
    var container2 = document.getElementById("container2");
    var style = container2.style.backgroundColor = "red";
})

//OTTAVO ESERCIZIO
/*
*   Fare in modo che il terzo pulsante appena aggiunto, se ricalcato una seconda volta, ripristini il colore di sfondo
* del testo a bianco.
*/
var changeColor = (function() {
    var n = 0;
    return function() {       
        n++;
        var container2 = document.getElementById("container2");
        if ((n % 2) === 0) {
            container2.style.backgroundColor = "blue";
        } else {
            container2.style.backgroundColor = "red";
        }
    }
})();
var btn3 = document.createElement("button");
btn3.innerText = "Cambia Sfondo"; 
container.append(btn3);
btn3.addEventListener("click", function() {
   changeColor();
})

// NONO ESERCIZIO
/*
*   Inserire un quarto pulsante "Autodistruzione". Questo pulsante elimina tutto il contenuto della pagina, compreso se stesso.
*/

var btn4 = document.createElement("button");
btn4.innerText = "Autodistruzione!";
container.append(btn4);
btn4.addEventListener("click", function() {
    var body = document.getElementById("body");
    body.remove();
})