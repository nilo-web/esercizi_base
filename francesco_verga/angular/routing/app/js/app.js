"use strict";

var app = angular.module("myApp", ["ngRoute"]);

app.run([
  '$rootScope',
  '$location',
  'authenticate',
  function ($rootScope, $location, authenticate) {
    $rootScope.$on("$routeChangeStart", function (e) {
      if (!authenticate.checkLogin()) {
        console.log("DENY");
        $location.path("/login/");
      } else {
        console.log("allow");
      }
    })
  }
]);

app.config(function ($routeProvider) {
  $routeProvider
    .when("/login/", {
      templateUrl: "/templates/login.html",
      controller: "loginCTRL"
    })
    .when("/registration", {
      templateUrl: "/templates/registration.html",
      controller: "registerCTRL",
    })
    .when("/profile/:username", {
      templateUrl: "/templates/profile.html",
      controller: "profileCTRL",
    })
    .when("/usersPage", {
      templateUrl: "/templates/usersPage.html",
      controller: "usersPageCTRL"
    })
    .otherwise({
      redirectTo: "/login/",
    });
});

// controller register
app.controller("registerCTRL", function ($scope, $location, userService) {

  $scope.authenticate = function (user) {
    $location.path('/profile/' + user.username)
  };

  $scope.validate = (user) => {
    userService.set(user);
    localStorage.setItem("user " + user.username, JSON.stringify(user));
  }
});

// controller profile
app.controller("profileCTRL", function ($scope, $routeParams, userService) {
  $scope.username = $routeParams.username;
  var userData = localStorage.getItem("user " + $scope.username);
  userService.set(userData);
  $scope.user = JSON.parse(userService.get());
});

// controller Users Page
app.controller("usersPageCTRL", function ($scope, $location) {

})


// controller login
app.controller("loginCTRL", function ($scope, authenticate, $location) {
console.log(authenticate)
  $scope.goToRegistration = () => {
    $location.path('/registration');
  }

  $scope.valoreReturn;

  // funzione inutile che uso ora solo per controllare i login
  $scope.login = function (login) {
    $scope.keys = Object.keys(localStorage);
    var length = $scope.keys.length;
    for (let i = 0; i < length; i++) {
      var obj = JSON.parse(localStorage.getItem($scope.keys[i]));
      if ((login.username === obj.username)
        && (login.password === obj.password)) {
        var user = {
          username: "francesco",
          password: "verga"
        }
        authenticate.setCredential(user);
        $scope.valoreReturn = authenticate.checkLogin();
        $location.path('/profile/' + login.username);
        return true;
      }
    }
    $scope.valoreReturn = false;
    return false;
  }

})

// servizi

// servizio per creare un utente e spostarlo da register a profile
app.service("userService", function () {
  this.user = {};
  this.set = (user) => {
    this.user = user;
  }
  this.get = () => {
    return this.user;
  }
});

// service per l'autenticazione
app.service("authenticate", function () {
  this.user = {};
  this.setCredential = (login) => {
    this.user.username = login.username;
    this.user.password = login.password;
  }
  this.checkLogin = () => {

    // ipotetica chiamata ad un API per prendere user e psw dei clienti
    // registrati

    // dentro all'if verrànno verificati i dati inseriti
    if (this.user.username === "francesco" &&
      this.user.password === "verga") {

      return true;

    } else {
      return false;
    }
  }
})

