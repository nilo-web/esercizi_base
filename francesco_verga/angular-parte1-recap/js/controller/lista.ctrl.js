"use strict";

myApp.controller("viewListCtrl", function($scope, filterService, toDoListService ) {

  // creo una variabile con scope che controlla l'array e ne prende i dati
  $scope.items = toDoListService.getData();

  // bindo la variabile searchKey del DOM al valore di ricerca
  $scope.searchKey = filterService.getValue();

  
});
