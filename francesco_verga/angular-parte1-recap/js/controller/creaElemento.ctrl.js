"use strict";

myApp.controller("myController", function ($scope, toDoListService) {

  // creo una funzione che si occupi di creare un elemento che faccia riferimento
  // agli "ng-model" che ho usato sull'html
  $scope.creaElemento = function (obj) {
    var elemento = {
      title: obj.titolo,
      body: obj.content,
    };

    // richiamo il servizio ed il suo metodo "add item" per poter inserire il mio
    // oggetto all'interno di un array
    toDoListService.addItem(elemento);

    // pulisco gli input dopo l'inserimento
    $scope.list.titolo = "";
    $scope.list.content = "";
  }

});

