myApp.controller("filterCtrl", function ($scope, filterService) {

  // funzione richiamata dal bottone di ricerca per settare il valore per il quale
  // filtrare la lista.
  $scope.search = (value) => {

    filterService.set(value);
    
  }


});