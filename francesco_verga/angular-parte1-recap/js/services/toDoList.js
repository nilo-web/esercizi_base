"use strict";

// creo un servizio che si occupa di creare un oggetto con :
myApp.service("toDoListService", function() {
  
  // un array 
  this.list = [];
  
  // metodo che si occupa di aggiungere oggetti all'array
  this.addItem = (item) => {
    this.list.push(item);
  }

  // metodo che si occupa di recuperare i dati dell'array
  this.getData = () => {
    return this.list;
  }
});

// servizio per creare un array di stringhe, ogni stringa corrisponde ad 
// un filtro di ricerca
myApp.service("filterService", function() {
  
  // creo l'array
  this.search = [];

  // metodo per inserire il valore nell'array
  this.set = (value) => {
    this.search.push(value);
  }
  
  // metodo per ritornare il valore.
  this.getValue = () => {
    
    return this.search;
    
  }

});