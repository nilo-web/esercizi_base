"use strict";

/* in questo file sono inseriti tutti i costruttori di prototipi
*/

// uso un costruttore per creare POST con gli attributi che mi servono

function Post(userId, title, body, id) {
 this.userId = userId;
 this.title = title;
 this.body = body;
 this.id = id;
}


// Creo un costruttore per i dati che mi servono delle CARD utenti
function User(name, username, email, id) {
 this.name = name;
 this.username = username;
 this.email = email;
 this.id = id
}


// uso un costruttore per creare Commenti con gli attributi che mi 
// serve mostrare

function Comment(name, email, body) {
 this.name = name;
 this.email = email;
 this.body = body;
}