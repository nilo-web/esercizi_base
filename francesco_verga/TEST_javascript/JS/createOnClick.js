"use strict";

/* Qui sono presenti tutte le funzioni che creano eventi onClick
*  e che vengono richiamate dagli ogetti deferred una volta che 
*  essi sono risolti.
*/


/* funzione usata per aggiungere un evento onClick sulle card.
*  viene istanziata nel momento in cui le card sono state scaricate.
*/

function createOnClickCard() {
 $("div.card").on("click", function () {

  // prendo l'ID della card, da usare nella ricerca dei POST
  let id = (this.id).slice(4);


  // richiamo la funzione per creare i POST
  getPosts(id);


  // controllo la selezione della card
  $("*").not(this).removeClass("card-selected");
  $(this).toggleClass("card-selected");


  // triggero l'animazione del container POST
  $(".invisible").toggleClass("invisible");


  // aggiungo un evento onClick al bottone presente sul 
  // container POST
  $(".bottone").on("click", function () {
   $(".containerPost").addClass("invisible");
   $("*").removeClass("card-selected");
  });
 })
}

/* funzione usata per aggiungere un evento onClick sui POST.
*  viene istanziata nel momento in cui i POST sono state scaricati.
*/

function createOnClickPost() {
 $("div.post").on("click", function () {

  // prendo l'ID del POST da usare nella ricerca dei COMMENT
  let id = (this.id).slice(4);

  cleanContainer("#headerText");
  cleanContainer("#modalBody");
  cleanContainer("#postBody");

  // richiamo la funzione per ricercare i COMMENT ed appenderli
  // al MODAL
  getComment(id);
 })
}

