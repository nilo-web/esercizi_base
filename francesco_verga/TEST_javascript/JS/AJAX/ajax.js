"use strict";

/* In questo file c'è la raccolta di tutte le funzioni usate
*  per chiamare l'API
*/



/* Utilizzo una funzione IFEE per caricare i dati delle card 
*  appena qualcuno carica la pagina
*/

(function getCard() {

 // L'oggetto deferred mi serve per poter caricare gli eventi di click sulle card
 // soltanto una volta che le card sono state create.
 let $defCheck = $.Deferred();

 $defCheck.then(createOnClickCard);
 $defCheck.then(activateClick("div.post"));

 $.ajax({
  type: "GET",
  url: "https://jsonplaceholder.typicode.com/users"
 })
  .done(function (response) {
   let length = response.length - 1;
   for (let i = 0; i <= length; i++) {

    // istanzio un oggetto della classe User           
    var utente = new User(
     response[i].name,
     response[i].username,
     response[i].email,
     response[i].id);

    // uso la funzione per creare le card 
    insertCard(utente);
   }

   // Le card sono state create quindi risolvo il deferred ed eseguo tutte le callback
   $defCheck.resolve();
  })

  // nel caso ci sia un errore mi viene stampato con un alert che 
  // riporta anche il tipo di errore
  .fail(function (error) {
   alert("Chiamata al server non effettuata per errore: " + error.status);
  })
  ;
})()




/* Questa funzione mi cerca i post con "userId" corrispondente a 
*  quello della card selezionata
*/

function getPosts(searchId) {

 /* Creo un oggetto deferred a cui aggiungo le funzioni in callback
 *  le funzioni verranno eseguite solo una volta che i file verranno scaricati
 *  Una volta finito metodo done() richiamando il metodo resolve().
 */
 let $defCheck = $.Deferred();

 
 $defCheck.then(createOnClickPost);
 $defCheck.then(setNumberBadge);
 $defCheck.then(activateClick("div.post"));

 $(document).ajaxSend(function(e, jqxhr, settings) {
  if(settings.url === "https://jsonplaceholder.typicode.com/posts?userId=" +
   searchId ) {
  hideClick("div.post");
 }
 });

 $.ajax({
  type: "GET",
  url: "https://jsonplaceholder.typicode.com/posts",
  data: {
   userId: searchId
  }
 })
   .done(function (response) {

   // richiamo la funzione per pulire il container
   cleanContainer("#containerPosts");

   let length = response.length - 1;
   for (let i = 0; i <= length; i++) {

    // istanzio un oggetto della classe Post           
    var post = new Post(
     response[i].userId,
     response[i].title,
     response[i].body,
     response[i].id
    );

    // uso la funzione per creare i post 
    insertPost(post);
   }

   // risolvo l'oggetto deferred creato e richiamo le funzioni callback
   $defCheck.resolve();
  })

  // nel caso ci sia un errore mi viene stampato con un alert che 
  // riporta anche il tipo di errore
  .fail(function (error) {
   alert("Chiamata al server non effettuata per errore: " + error.status);
  })
  ;
}




/* Questa funzione mi cerca i commenti con "userId" corrispondente a 
*  quello della card selezionata
*/

function getComment(searchId) {

 /* Creo un oggetto deferred a cui aggiungo le funzioni in callback
 *  le funzioni verranno eseguite solo una volta che i file verranno scaricati
 *  Una volta finito metodo done() richiamando il metodo resolve().
 */
 let $defCheck = $.Deferred();

 $defCheck.then(activateClick("div.post"));

 $.ajax({
  type: "GET",
  url: "https://jsonplaceholder.typicode.com/comments",
  data: {
   postId: searchId
  }
 })
  .done(function (response) {

   // richiamo la funzione per pulire il testo
   insertModalText(searchId);


   let length = response.length - 1;
   for (let i = 0; i <= length; i++) {

    // istanzio un oggetto della classe Comment           
    var comment = new Comment(
     response[i].name,
     response[i].email,
     response[i].body,
    );

    // uso la funzione per creare i commenti 
    insertComment(comment);
   }

   // risolvo l'oggetto deferred creato ed eseguo tutte le funzioni di callback
   $defCheck.resolve();
  })

  // nel caso ci sia un errore mi viene stampato con un alert che 
  // riporta anche il tipo di errore
  .fail(function (error) {
   
   alert("Chiamata al server non effettuata per errore: " + error.status);
  })
  ;
}