"use strict";

/* funzione con lo scopo di controllare quanti POST il singolo 
*  utente ha effettuato, contando il numero di children presenti.
*/
function setNumberBadge() {
 var badgeNumber = $("#containerPosts").children().length;
 $("#badgeId").text(badgeNumber);
}



// funzione che cancella ogni cosa presente sul container
function cleanContainer(containerId) {
 
 $(containerId).empty();

}

/* funzione che cancella il testo all'interno del MODAL ed
*  inserisce il testo del post nell'header.
*/
function insertModalText(id) {

  // salvo il testo presente nel post cliccato
  let h2 = ($("#post" + id).children())[0].textContent;
  let p = ($("#post" + id).children())[1].textContent;

  // inserisco il testo del POST selezionato dentro al MODAL
  $("#headerText").append("<strong>" + h2 + "</strong>");
  $("#headerText").append("<br></br>");
  $("#headerText").append(p);
  $("#postBody").append("<p style='font-size: 30px';><strong>Comment:</strong></p>");
 } 


// due funzioni per impedire il click nel caso in cui i dati non 
// siano ancora stati scaricati.
function activateClick(selector) {
 $(selector).each(function() {
  this.style.pointerEvents = "auto";
 });
}

function hideClick(selector) {
 $(selector).each(function (){
  this.style.pointerEvents = 'none'; 
 });
}