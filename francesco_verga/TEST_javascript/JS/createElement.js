"use strict";

/* In questo file sono contenute tutte le funzioni che si occupano di
*  creare un elemento ed appenderlo nel suo container
*/


/* Questa funziona crea un <div> per ogni oggetto di User, 
*  prendendo i dati dall'oggetto stesso che gli viene passato come
*  argomento. (inserita nella funzione getCard())
*/
function insertCard(user) {

    // qui aggiungo la classe per il layout
    var $card = $("<div class='col-md-3 card' id='user" + 

    // qui invece aggiungo l'id dell'user come id del <div>, cosi da rendere più semplice il
    // processo di ricerca dei POST del singolo USER
    user.id + "'>" + 
    "<p>" +   
    user.name + 
    "</p>" + 
    "<p>" + 
    user.username + 
    "</p>" + 
    "<p>" + 
    user.email + 
    "</p></div>");

    // inserisco la card nel suo container
    var $container = $("#containerCard");  
    $container.append($card);
}



/* Questa funzione mi crea dei <div> con i dati dei POST e con
*  classe e id corrispondenti a quelli che desidero. 
*  Mi inserisce il tutto all'interno del container dei POST
*  (richiamo questa funzione da getPost())
*/
function insertPost(post) {

 
 var $post = $("<div class='col-md-12 post' id='post" + // qui aggiungo la classe per il layout
 post.id + "'" + // qui invece aggiungo l'id dell'user come id del <div>
 " data-toggle='modal' data-target='#myModal'>" + 
 "<h2>" +   
 post.title + 
 "</h2>" + 
 "<p>" + 
 post.body + 
 "</p></div>");
 var $container = $("#containerPosts");
 $container.append($post);
}



/* Questa funzione mi crea dei <div> per i commenti con classe e id corrispondenti 
*  a quelli che desidero. 
*  Mi inserisce il tutto all'interno del MODAL. (la richiamo da getComment())
*/
function insertComment(comment) {
 var $comment = $("<div class='col-md-12'>" +  // qui aggiungo la classe per il layout
 "<h2>" +   
 comment.name + 
 "</h2>" + 
 "<p>" + 
 comment.email + 
 "</p>" + 
 "<p>" + 
 comment.body +
 "</p><hr></div>");
 var $container = $("#modalBody");
 $container.append($comment);
}