"use strict";
var $barraLaterale = $("#barraLaterale");

$.ajax({
    type:"GET",

     // url per la chiamata ajax alla quale passo in query string un valore corrispondente all'id dell'utente selezionato
    url:"https://jsonplaceholder.typicode.com/posts?userId="+ utenteSelezionato.id,
}).done(function(elencoPostResponse){

    // resetto il contenuto all'interno del contenitore degli articoli sostituendolo con una stringa vuota
    $barraLaterale.html("");
    
    //bottone X
    var $bottone =$(`
                    <div id="bottoneImmagine">
                        <button type="button"><img id="immagine" src="X.png"></button>
                    </div>`);
    $dettagliUtenteNome.append($bottone);

    $bottone.on("click",function(){
        $barraLaterale.remove();
        $dettagliUtenteNome.remove();
    });
    var count = 0;
    // ciclo sui risultati della chiamata, ossia gli articoli/post dell'utente e per ognuno di essi genero un post
    for(var i=0; i < elencoPostResponse.length; i++){
        var post = elencoPostResponse[i];
        var $cardPosts = $(`
                            <li class="list-group-item">${post.title}</li>                    
        `);
        
        $barraLaterale.append($cardPosts);
        count++;
        var $numPosts = $(`<p>Posts: ${count}</p>`)
    }
    
    $dettagliUtenteNome.append($numPosts)   

}).fail(function(error){
    console.error("error",error);
});