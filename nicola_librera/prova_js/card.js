"use strict";

var $utenti = $("#utenti");
var $barraLaterale = $("#barraLaterale");

// seleziono il contenitore dei dettagli dell'utente
var $contenitoreDettagliUtente = $("#dettagliUtente");

// cerco all'interno del contenitore dei dettagli utente il posto dove inserire il nome
var $dettagliUtenteNome =$contenitoreDettagliUtente.find("h5");

var utenteSelezionato;
var elencoUtenti;

// genero la chiamata http passandogli un oggetto di configurazione
// con l'url e il verbo http
$.ajax({
    type: "GET",
    url: "https://jsonplaceholder.typicode.com/users",
}).done(function(elencoUtenteResponse){
    elencoUtenti = elencoUtenteResponse;

    // ciclo sulla lista e per ogni utente creo ed inserisco nel dom una "card"
    for(var i=0; i < elencoUtenteResponse.length; i++){
        var utente = elencoUtenteResponse[i];

        var $cardUtente = $(`
                <div id="${utente.id}" class="card-group col-sm-3">
                    <div class="card">
                        <div id="cliccato" class="card-body">
                            <h5 class="card-title">${utente.name}</h5>
                            <h6 class="card-title">${utente.username}</h6>
                            <h7 class="card-title">${utente.email}</h7>
                        </div>
                    </div>
                </div>
            `);

        $cardUtente.on("click",function(event){
            console.log("Utente cliccato",event.target);

            if(this.classList.contains("cambiocolore"))
                this.classList.remove("cambiocolore");
            else
                this.classList.add("cambiocolore");

            // cerco tra tutti gli utenti della lista l'utente corrispondente a quello cliccato
            for(var j=0; j<elencoUtenti.length; j++){
                if(elencoUtenti[j].id == this.getAttribute("id"))
                    utenteSelezionato = elencoUtenti[j];
            }

        // una volta trovato l'utente corrispondente al click aggiorno il titolo
        $dettagliUtenteNome.html(utenteSelezionato.name);

        $.ajax({
            type:"GET",

             // url per la chiamata ajax alla quale passo in query string un valore corrispondente all'id dell'utente selezionato
            url:"https://jsonplaceholder.typicode.com/posts?userId="+ utenteSelezionato.id,
        }).done(function(elencoPostResponse){

            // resetto il contenuto all'interno del contenitore degli articoli sostituendolo con una stringa vuota
            $barraLaterale.html("");
            
            //bottone X
            var $bottone =$(`
                            <div id="bottoneImmagine">
                                <button type="button"><img id="immagine" src="X.png"></button>
                            </div>`);
            $dettagliUtenteNome.append($bottone);

            $bottone.on("click",function(){
                $barraLaterale.remove();
                $dettagliUtenteNome.remove();
            });
            var count = 0;
            // ciclo sui risultati della chiamata, ossia gli articoli/post dell'utente e per ognuno di essi genero un post
            for(var i=0; i < elencoPostResponse.length; i++){
                var post = elencoPostResponse[i];
                var $cardPosts = $(`
                                    <li class="list-group-item">${post.title}</li>                    
                `);
                
                $barraLaterale.append($cardPosts);
                count++;
                var $numPosts = $(`<p>Posts: ${count}</p>`)
            }
            
            $dettagliUtenteNome.append($numPosts)   

        }).fail(function(error){
            console.error("error",error);
        });
        
        });
        $utenti.append($cardUtente);
    }   
});