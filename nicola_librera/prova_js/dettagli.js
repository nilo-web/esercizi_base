"use strict";

var $utenti = $("#utenti");

var arrayIstanzeUtente = [];
// seleziono il contenitore dei dettagli dell'utente
var $contenitoreDettagliUtente = $("#dettagliUtente");

// cerco all'interno del contenitore dei dettagli utente il posto dove inserire il nome
var $dettagliUtenteNome =$contenitoreDettagliUtente.find("h5");

function Utente() {
    this.id ="";
    this.name ="";
    this.username ="";
    this.email ="";
    this.$cardUtente = "";
}

Utente.prototype.stampaName = function(){
    console.log("Stampo il nome",this.name + " ");
}

Utente.prototype.stampaUsername = function(){
    console.log("Stampo l'username",this.username + " ");
}

Utente.prototype.stampaEmail = function(){
    console.log("Stampo l'email",this.email + " ");
}

Utente.prototype.stampaId = function(){
    console.log("Stampo l'id",this.id + " ");
}

Utente.prototype.stampaCardUtente = function(){
    console.log("Stampo la card utente",this.$cardUtente + " ");
}

// genero la chiamata http passandogli un oggetto di configurazione con l'url e il verbo http
$.ajax({
type: "GET",
url: "https://jsonplaceholder.typicode.com/users",
}).done(function(elencoUtenteResponse){
//elencoUtenti = elencoUtenteResponse;

// ciclo sulla lista e per ogni utente creo ed inserisco nel dom una "card"
for(var i=0; i < elencoUtenteResponse.length; i++){
    
    

    var u = new Utente();
    u.id = elencoUtenteResponse[i].id;
    u.name = elencoUtenteResponse[i].name;
    u.username = elencoUtenteResponse[i].username;
    u.email = elencoUtenteResponse[i].email;

    u.stampaName();
    u.stampaUsername();
    u.stampaEmail();
    u.stampaId();

    arrayIstanzeUtente.push(u);

    var $cardUtente = $(`
            <div id="${u.id}" class="card-group col-sm-3">
                <div class="card">
                    <div id="cliccato" class="card-body">
                        <h5 class="card-title">${u.name}</h5>
                        <h6 class="card-title">${u.username}</h6>
                        <h7 class="card-title">${u.email}</h7>
                    </div>
                </div>
            </div>
        `);
    $utenti.append($cardUtente);

    $cardUtente.on("click",function(event){
        console.log("Utente cliccato",event.target);

        if(this.classList.contains("cambiocolore"))
            this.classList.remove("cambiocolore");
        else
            this.classList.add("cambiocolore");

        var result = cercaUtente(this.getAttribute("id"));

    // una volta trovato l'utente corrispondente al click aggiorno il titolo
    $dettagliUtenteNome.html(result.name);
    });
}
});

function cercaUtente(id)
{
    var risultatoDaTornare;
// cerco tra tutti gli utenti della lista l'utente corrispondente a quello cliccato
for(var j=0; j<arrayIstanzeUtente.length; j++){
    if(arrayIstanzeUtente[j].id == id)
        risultatoDaTornare = arrayIstanzeUtente[j];
}
return risultatoDaTornare;
}