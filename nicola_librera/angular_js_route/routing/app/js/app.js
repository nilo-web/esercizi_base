"use strict";

var app = angular.module("myApp", ["ngRoute"]);

app.config(function ($routeProvider) {
  $routeProvider
    .when("/", {
      templateUrl: "/templates/registration.html",
      controller: "registrationController",
    })
    .when("/userpage/:username", {
      templateUrl: "/templates/userpage.html",
      controller: "userpageController",
    })
    .otherwise({
      redirectTo: "/",
    });
});

app.controller("registrationController", function ($scope, $location, myUser) {
  console.log($scope);

  $scope.sendData = function (user) {
    myUser.setUser(user);

    $location.path("/userpage/" + user.username);
  };
});

app.controller("userpageController", function ($scope, $routeParams, myUser) {
  $scope.user = $routeParams.username;
});


app.run([
  "$rootScope",
  "$location",
  "authService",
  function ($rootScope, $location, authService) {
    $rootScope.$on("$routeChangeStart", function (event) {
      if (!authService.isLoggedIn()) {
        console.log("DENY", $location, event);
        $location.path("/");
      } 
    });
  },
]);

app.factory("authService", function () {
  var user = true;

  return {
    setUser: function (aUser) {
      user = aUser;
    },
    isLoggedIn: function () {
      return user ? user : false;
    },
  };
});


app.factory("myUser", function () {
  var user = {};

  return {
    setUser: function (user) {
      user = user;
    },
    getUser: function() {

      return user;
    }
  };
});
