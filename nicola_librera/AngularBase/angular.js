"use strict";

//.module si aspetta il nome dichiarato in ng-app nel file html,
//e l'array vuoto [], che è fondamentale
var puntoInizio = angular.module("puntoInizio",[]);

//stiamo facendo una callback perchè passiamo una funzione in un metodo
puntoInizio.controller("controllerUtente",function($scope){
    console.log("sono il controller dell'utente", $scope);
});

//stiamo facendo una callback perchè passiamo una funzione in un metodo
puntoInizio.controller("controllerPost",function($scope){
    console.log("sono il controller del post", $scope);
});

//servizio per prendere gli utenti con una get che poi passiamo al controller
puntoInizio.factory("servizioUtenti", function($http){
  var listaUtenti = [];
  return{
    getUtenti:function(){
     
      if(listaUtenti.length > 0)
        return $http.get("https://jsonplaceholder.typicode.com/users");
    }
  }
});

//servizio per prendere i post della card utente cliccata con una get
puntoInizio.factory("serviziPost", function($http){
  return{
    getPost:function(userId){
      return $http.get("https://jsonplaceholder.typicode.com/posts?userId="+ userId);
    },
  };
});



//stiamo facendo una callback perchè passiamo una funzione in un metodo
puntoInizio.controller("utentiLista",["$scope","servizioUtenti","servizioPost",function($scope,servizioUtenti,servizioPost)
{
  console.log("sono il controller della listaUtenti",$scope);
  $scope.commenti = listaCommenti;
  
  servizioUtenti.getUtenti()
    .then(
        function(response){
            console.log(response);
            $scope.utenti = response.data;
        },
        function(errore){
          alert("Error");
        }
    );

    $scope.prendiIPost =function(userId)
    {
      console.log("click",userId);

      servizioPost.getPost(userId)
      .then(
        function(response){
        console.log(response);
        $scope.utenti = response.data;
      },
      function(errore){
        alert("Error");
      }
    );
 
  //$scope.post = posts;

  //$scope.listaDiPost= [
    
  //]

  $scope.mostraMessaggio = function(id, $event){
    $scope.listaDiPost=[];
    console.log(arguments);
    //alert("La card premuta ha id = "+id+"\nIn console troverai tutti i titoli dei post collegati all'Id= "+id);
    //console.log("I titoli dell'utente "+id+ " sono:");
    for(var i=0; i<posts.length; i++){
      if(id ==posts[i].userId)
        $scope.listaDiPost.push(posts[i]);
    }
  }



  $scope.mostraCommenti = function(id, $event){
    $scope.listaDiCommenti=[];
    console.log(arguments);
    //alert("La card premuta ha id = "+id+"\nIn console troverai tutti i titoli dei post collegati all'Id= "+id);
    //console.log("I commenti dell'utente "+id+ " sono:");
    for(var j=0; j<listaCommenti.length;j++){
      if(id == listaCommenti[j].postId)
        $scope.listaDiCommenti.push(listaCommenti[j]);
    }
  };

}]);
