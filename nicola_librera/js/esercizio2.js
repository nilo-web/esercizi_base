/* 
1-OK -->Inizialmente la pagina è vuota, ma ogni volta che clicco sul pulsante compare una nuova riga
        di testo, ma le righe saranno una volta rosse e una volta blu.
2-OK -->Inserire un altro pulsante: se lo schiaccio devo cancellare tutte le righe inserite.
3-OK -->Creare una funzione che prende in ingresso un container e lo pulisce da tutti i suoi figli. 
4-OK -->Aggiungere un terzo pulsante che una volta cliccato, cambierà il colore di sfondo del div che contiene il testo.
5-OK -->Fare in modo che il terzo pulsante appena aggiunto, se ricalcato una seconda volta, ripristini il colore di sfondo
del testo a bianco.
5-OK -->Inserire un quarto pulsante "Autodistruzione". Questo pulsante elimina tutto il contenuto della pagina, compreso se stesso.
*/

 "use strict"
 
var bottone = document.getElementById("bottone");
var bottone2 = document.getElementById("bottone2");
var bottone3 = document.getElementById("bottone3");
var bottone4 = document.getElementById("bottone4");
var container = document.getElementById("container");
var body = document.body;
var lista = document.getElementById("lista");
var creaTesto = null;

var count = 0;
bottone.addEventListener("click",function(){
   
    creaTesto = document.createElement("li"); //creo una riga <li>
    if(count %2 == 0)
    {
        creaTesto.innerHTML ="<b>Hello world</b>";
        creaTesto.style.color = "red";
        lista.appendChild(creaTesto);
    }
    else
    {
        creaTesto.innerHTML ="<b>Hello world</b>";
        creaTesto.style.color = "blue";
        lista.appendChild(creaTesto);
    }
    count++;
}); 

bottone2.addEventListener("click",function(){
    container.remove();
});

//nel CSS ho inserito una classe .cambiocolore.... nell'if va a vedere se questa classe già è contenuta la rimuove, altrimenti la aggiunge
bottone3.addEventListener("click",function()
{
    if(container.classList.contains("cambiocolore"))
        container.classList.remove("cambiocolore");
    else
        container.classList.add("cambiocolore");
});

bottone4.addEventListener("click",function(){
    body.remove();
});
