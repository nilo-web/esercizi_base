"use strict";

//Ereditarietà e prototipi
function Persona() {
    this.nome ="";
    this.cognome ="";

}

var p = new Persona();
p.nome = "pino";
p.cognome ="pini";
p.eta = 50;


function Persona(nome,cognome,eta) {
    this.nome = nome;
    this.cognome = cognome;
    this.eta = eta;
}

var g = new Persona("giacomo","giacomi");

//il prototype serve per definire delle proprietà comuni per degli oggetti che hanno
//bisogno delle stesse proprietà
Persona.prototype.eta = 0;
Persona.prototype.stampaNome = function(){
    console.log("stampo il nome",this.nome + " " + this.cognome);
}


function Dottore()
{

}
Dottore.prototype = new Persona(); //dottore eredita da persona
Dottore.prototype.stampaNomeDottore = function(){
    console.log("stampaNomeDottore");
};

var d = new Dottore();
d.nome ="Nicola";
d.cognome ="Librera";

var singleton = (function(){
    var instance;
    return{
        getInstance: function(){
            if(!instance){
                instance = {
                    attributo: 123,
                    metodo: function(){

                    }
                }
            }
            return instance;
        }
    }
})();
