"use strict";

var container = document.getElementById("container"); //elemento html
var btn1 = document.getElementById("btn1"); //elemento html

//seleziono l'elemento padre
var lista = document.getElementById("lista");

//creo nuovo elemento
var mioItem  = document.createElement("li");

//gli assegno un contenuto
mioItem.innerHTML ="<b>Nicola</b>";
mioItem.setAttribute("prova","sono il valore");

/*function miaCallback(event){
	alert("cliccato",event.target, this);
};*/

//mioItem.addEventListener("click", miaCallback());

var containerRisultato = document.getElementById("risultato-evento-custom");
var mioEventoCustom = new Event("prova");

containerRisultato.addEventListener("prova",function(){
	console.log("evento custom ok");
});

//aggiungo il nuovo elemento al contenitore
lista.appendChild(mioItem);

lista.style.color = "red";

lista.addEventListener("click",function(e){
	console.log("clicco la lista");
	console.log("this",this);
	console.log("event target", e.target.innerHTML);

	containerRisultato.dispatchEvent(mioEventoCustom);
});

btn1.addEventListener("nuovoevento",function(){
	console.log("evento custom")
});

container.addEventListener("click",function(event){
	event.stoPropagation(); //serve per stoppare la propagazione
	console.log("evento del container", event.target, this);
})

btn1.addEventListener("click",function(event){
	event.stoPropagation();
	console.log("evento del bottone", event.target, this);

	link.dispatchEvent(evento);
});

link.addEventListener("click",function(event){
	event.preventDefault();
	console.log("evento del link")
});

//myFunction("green","yellow");


//inizio jquery
// come parametri si usa la sintassi del css, # per i div, . per le classi
var $container = $("#container"); //variabile jquery, per convenzione si usa il $ nella dichiarazione
var $ul = $("#ul");
var $elementiListaRossi = $("li.rosso"); //se avessi messo solo .rosso, avrebbe preso tutti gli elementi rossi

$elementiListaRossi.addClass("testo-grande");

if($elementiListaRossi.hasClass("testo-grande"))
{
    $elementiListaRossi.removeClass("testo-grande");
}

$elementiListaRossi.attr("data-pigna","pigna");

$container.append("sono appeso");
$container.prepend("<p><b>sono appeso come P</b></p>");

var $template = $(`
    <div>
        <h1>ciao sono un titolo inserito a runtime</h1>
    </div>
`);

$container.append($template);

$container.on("click", function(e)
{
    console.log(e)
});

$.fn.tuttoMaiuscolo = function()
{
    console.log(this);
    this.text(this.text().toUpperCase());
}

var $lista = $("#lista");
var $nuovoElementoLista = $("<li id='4'> nuovo elemento lista </li>");

$nuovoElementoLista.on("click",function(){
	console.log(123);
});

$lista.append($nuovoElementoLista);

var $elementoFiglio = $lista.find("1");

//parente prende il primo nodo padre, se avessi voluto prendere piu padri
//avrei dovuto usare parents ma con un selettore per capire di quanti livelli
//deve salire altrimenti li sale tutti.
var $elementoPrimoPadre = $nuovoElementoLista.parent();
var $elementoPadre = $nuovoElementoLista.parents("div");


/*
//inizio json
var httpReq = new XMLHttpRequest();

httpReq.onreadystatechange = function() //resto in ascolto degli stati
{
	if(httpReq.readyState === 4 && httpReq.status === 200)
	{
		console.log("onreadystatechange",httpReq);
	}
};

httpReq.open("GET","https://jsonplaceholder.typicode.com/posts"); //apro la chiamata settando l'url che deve chiamare
httpReq.send(); //invio la chiamata
*/

//inizio ajax, se voglio utilizzarlo devo commentare il json scritto sopra, e viceversa
$.ajax({
	type: "GET",
	url:"https://jsonplaceholder.typicode.com/posts"
})
.done(function(response){
	console.log("response su chiamata jquery",response);
});


var array = [] ;
