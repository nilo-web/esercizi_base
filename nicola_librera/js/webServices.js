"use strict";

var jqueryCall = $.ajax({
    tyle:"GET",
    url:"https://jsonplaceholder.typicode.com/posts"
});

//chiamate asincrone, vengono eseguite dopo set timeout
jqueryCall.done(function(){
    console.log("call done", response);

});

//chiamate asincrone, vengono eseguite dopo set timeout
jqueryCall.fail(function(){
    console.log("error",error);
});

console.log(1)

setTimeout(1)
{
    setTimeout(function(){
        console.log("timeout done");
    }, 0);

    console.log(2);
}


//chiamata______________________________________________________________________________________________________________________________________________________
var httpReq = new XMLHttpRequest();

//sto in ascolto per lavorare su ogni fase della chiamata
httpReq.onreadystatechange = function (){
    //se la chiamata è conclusa e ha uno stato = 200
    if(httpReq.readyState === 4 && httpReq.status === 200)
        console.log("Done js standard call",httpReq.response);
};

//APRO LA CHIAMATA
httpReq.open("GET","https://jsonplaceholder.typicode.com/posts");

//INVIO LA CHIAMATA
httpReq.send();
//_______________________________________________________________________________________________________________________________________________________________

//_______________________________________________________________________________________________________________________________________________________________
var deferred = $.deferred(); //l'oggetto deferred è una promessa
deferred.then(function(){ //questa è una callback di success
    console.log("Successo: sono stata mantenuta");
},
function(){ //questa è una callback di fail
    console.log("Errore: NO");
});
//______________________________________________________________________________________________________________________________________________________________