"use strict";

var $lista = $("#miaLista");


//JQuery
//genero una chiamata
var chiamataAjax = $.ajax({
    type: "GET",
    url: "https://jsonplaceholder.typicode.com/posts"
});

//resto in ascolto
chiamataAjax.done(function(response){
    console.log("response",response);

    var $nuovoItem = $('<li>'+response[0].title+'</li>');
    
    $lista.append($nuovoItem);
});

//nel caso in cui la chiamata fallisce
chiamataAjax.fail(function(e){
    console.log("ERRORE",e);
});

/*
var chiamataInPost =$.ajax({
    type:"POST",
    url: "https://jsonplaceholder.typicode.com/posts",
    dataType: "json",
    data: JSON.strigify({
        title: "foo",
        body: "bar",
        userId: 1,
    }),
});
*/
/*

//nel caso in cui la chiamata fallisce
chiamataAjax.fail(function(e){
    console.log("ERRORE",e);
});

var chiamataInUser =$.ajax({
    type:"USER",
    url: "https://jsonplaceholder.typicode.com/users",
    dataType: "json",
    data: JSON.strigify({
        title: "foo",
        body: "bar",
        userId: 1,
    }),
});
*/

//il metodo deferred ci fa eseguire un codice in maniere asincrona
console.log("1");
var deferred = $.Deferred();

console.log("2");
deferred.then
(
    function()
    {
        console.log("3 successo");
    },

    function()
    {
        console.log("3 error");
    }
);

console.log("4");
deferred.resolve();
console.log("5");




/*
//Java script puro
//per usare javascript puro, devo commentare il frammento di codice in JQuery e viceversa
var httpReq = new XMLHttpRequest();
httpReq.onreadystatechange = function()
{
    if(httpReq.readyState === 4 && httpReq.status === 200)
    {
        console.log("SI",httpReq.response);
        var response = JSON.parse(httpReq.response)
        var $nuovoItem = $('<li>'+response[0].title+'</li>');
        $lista.append($nuovoItem);
    }
};

httpReq.open("GET","https://jsonplaceholder.typicode.com/posts");
httpReq.send();
*/
