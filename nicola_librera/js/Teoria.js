//VARIABILI ------------------------------------------------------------------------------------------------------------------------
"use strict";

var nomeMaiuscolo = "Nicola".toUpperCase();

//dichiarazione variabili stringhe
var variabileStringa = "Nicola";

//dichiarazione variabili intere
var interoNegativo = -10;
var zero = 0;
var interoPositivo = 10;

//dichiarazioni variabili non intere
var numeroDecimale = 0.52;
var decimaleNegativo = -1.2;

//dichiarazione variabile boolean
var variabileBooleana = true;
var variabileBooleana2 = false;

//dichiarazione variabili null
var x = null;

//dichiarazione variabile undefined
var y;
//----------------------------------------------------------------------------------------------------------------------------------

//MANIPOLAZIONE DEL DOM-------------------------------------------------------------------------------------------------------------
//per manipolare il dom bisogna dichiarare nell'html un id, in questo caso l'id = mioParagrafo
< p id ="mioParagrafo"> Questo è un paragrafo </p>

//nel file js bisogna dichiare una variabile = document.getElementById con l'id dell'html
var p = document.getElementById ("mioParagrafo");

//getElementsByTagName: individua gli elementi di una pagina in base al loro tag.
var listaParagrafi = document.getElementsByTagName("p");

/*querySelector: selezionare gli elementi di una pagina utilizzando i selettori CSS.
Due sono i metodi che consentono questo approccio: querySelector() e querySelectorAll(). 
L’uno restituisce il primo elemento trovato, l’altro l’elenco di tutti gli elementi individuati dal selettore. 
Ad esempio, il seguente codice restituisce l’elenco dei <div> di classe messaggio:*/
var divList = document.querySelectorAll("div.messaggio");

/*Il seguente esempio, invece, seleziona l’elemento con id mioParagrafo, risultando
quindi alternativo a getElementById():*/
var p = document.querySelector("#mioParagrafo");

/*Una volta individuato l’elemento o gli elementi presenti su una pagina, possiamo modificarlo.
Ad esempio, la proprietà innerHTML rappresenta il contenuto HTML di un elemento ed è accessibile sia in lettura che in scrittura.*/
var p = document.getElementById("mioParagrafo");
p.innerHTML = "Testo del paragrafo";

//Aggiungere/rimuovere elementi
var mainDiv = document.getElementById("mainDiv");
var img = document.createElement("img");
var srcAttr = document.createAttribute("src");

srcAttr.value ="default.png";
img.setAttributeNode(srcAttr);
mainDiv.appendChild(img);
//----------------------------------------------------------------------------------------------------------------------------------

//FUNZIONI--------------------------------------------------------------------------------------------------------------------------
//dichiarare una funzione
function nome(argomenti)
{
    //istruzioni
}

//invocare una funzione
nome(valori);
//----------------------------------------------------------------------------------------------------------------------------------

//EVENTI----------------------------------------------------------------------------------------------------------------------------
/*<a href="#" id="test">test</a>*/
/*<script>*/
//definisce la funzione callback
function clickOnTest(event){
console.log('Click su test');
}

// ottiene l'elemento 'test'
var test = document.getElementById('test');
// associa l'evento click alla callback
test.addEventListener('click', clickOnTest);
//</script>
//----------------------------------------------------------------------------------------------------------------------------------

//DICHIARAZIONE DI OGGETTI----------------------------------------------------------------------------------------------------------
var persona = 
{
    "Nome":"Nicola",
    "Cognome":"Librera",
    indirizzo:
    {
        via: "Via Marano",
        numero: 47,
        CAP: 80041,
        citta: "Napoli"
    }
};

console.log(persona.Nome);
console.log(persona.Cognome);
console.log(persona.indirizzo);

//per rimuovere una proprieta
delete persona.Nome;

//creazione di un array
var x = [];

 //oppure
 var x = new Array();
 //oppure
 var x = new Array(5);
 //oppure
 var x = new Array("uno","due",3,5,null,true,false);
 //---------------------------------------------------------------------------------------------------------------------------------