"use strict";

var myApp = angular.module("myApp",[]);

myApp.directive("mySelectCity",function(){
    return{
        restrict:"E",//E=element
        templateUrl:"/mySelectCityTemplate.html",
        scope:{
            elencoCitta:"=cityList"
        }
    }
});

myApp.directive("testDirettiva",function()
{
    return {
        restrict:"E",
        templateUrl:"/testDirettivaTpl.html",
        scope:{
            valore:"=provaValore"
        }
    }
});

myApp.controller("myController",function($scope)
{
    $scope.elencoCitta =[
        {codice:"RM", nome:"Rome", regione:"Lazio"},
        {codice:"LT", nome:"Latina", regione:"Lazio"},
        {codice:"MI", nome:"Milano", regione:"Lombardia"},
        {codice:"NA", nome:"Napoli", regione:"Campania"},
        {codice:"CO", nome:"Come", regione:"Lombardia"},
        {codice:"PA", nome:"Palermo", regione:"Sicilia"},
        {codice:"CA", nome:"Caserta", regione:"Campania"},
        {codice:"AV", nome:"Avellino", regione:"Campania"},
        {codice:"TP", nome:"Trapani", regione:"Sicilia"},
        {codice:"AG", nome:"Agrigento", regione:"Sicilia"},
    ];

$scope.valoreController= "Pino";
$scope.nomeUtente ="Nicola";
});