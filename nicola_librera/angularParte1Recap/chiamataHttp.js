"use strict";

//var myApp = angular.module("myApp", []);

myApp.filter("capitalize", function(){
    return function(value)
    {
        console.log("Valore filtro custom", value);
        var result = value;

        if(text && isNaN(value))
        {
            result = value.charAt(0).toUpperCase() + value.substr(1);
        }
        return result;
    }
});

myApp.factory("servizioPromise", function($q){
    var oggettoDeferred = $q.defer();
    
    setTimeout(function(){
        oggettoDeferred.resolve("pino");
    },2000);

    return oggettoDeferred.promise;
});

myApp.controller("userController",["$scope", "$http","servizioPromise", function($scope,$http, servizioPromise){
    $scope.utente = {nome: "mario", cognome: "rossi"};

    $scope.listaPost;
    $http.get("https://jsonplaceholder.typicode.com/posts")
    .then(
        function(responseData){
            console.log(responseData);
            $scope.listaPost = responseData.data;
        },
        function(errore){

        }
    );

    servizioPromise.then(function(data) {
        console.log("Promessa risolta", data);
    },
    function(error) {}
    );
}]);

