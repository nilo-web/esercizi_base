"use strict"

function PostCard(postObj)
{
    this.userId = postObj.userId;
    this.id= postObj.id;
    this.title = postObj.title;
    this.body = postObj.body;
    this.$tpl = $('<div>class="card-post"></div>');
}

PostCard.prototype.renderPost = function($container)
{
    var contenutoTestualeCard = 
        this.userId + " - " + this.id + " - "  + this.title + " - " + this.body;

    this.$tpl.html(contenutoTestualeCard);

    $container.append(this.$tpl) ;
}