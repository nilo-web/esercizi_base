var myApp = angular.module("myApp", []);

myApp.factory("servizioPromis",function($q){
    var objDeferred = $q.defer();

    setTimeout(function(){
        objDeferred.resolve("pino");
    },2000);

    return objDeferred.promise;
});

myApp.controller("userController",function($scope,$http){
    $scope.utente = {nome : "Mario", cognome: "Rossi"};

    $scope.listPost;

    $http.get("https://jsonplaceholder.typicode.com/posts")
    .then(
        function(responseData){
            console.log(responseData);
            $scope.listPost = responseData.data;
        },
        function(errore){
            console.log("errore then");
        }
    );

    servizioPromise.then(
        function(data) {
            console.log("Promessa risolta",data);
        },
        function(error) {
            console.log("errore promise")
        }
    );

});