/*
sercizi AngularJs
Crea una lista TODO: 
crea un box con 2 input e un bottone:
    una input "titolo" e una "contenuto" e un bottone "salva"

    Al clic del bottone 
        aggiungi il mio elemento TODO 
        ad una lista di cose da fare (todo list) e la visualizzerò al di sotto.

Crea un'altra scatola (di fianco alla prima) 
    in un solo input che servirà da filtro per i miei elementi.
    Quindi scrivendo in questo input filtro a runtime
    la lista per il titolo dei vari elementi.
*/
var listTodo = [];
var Todo = function (title,content)
{
    this.title = title;
    this.content = content;    
}

// my app inizilizzo l'applicazione
var myApp = angular.module("myApp",[]);
    // mycontroller inizializzo il controller
    myApp.controller("myController",function($scope) {
        
        $scope.listTodo = listTodo;
        $scope.Todo = Todo;

        $scope.title = "title";
        $scope.content= "content";

        /* function addToDO 
            param title e content restituiti dagli input
                Crea un obj Todo e lo 
                Aggiunge alla listTodo
            return listTodo
        */
        $scope.addTodo = function(title,content) {
            var todo = new Todo(title,content);
            listTodo.push(todo);

            console.log(listTodo);
            return listTodo;
        };

    })