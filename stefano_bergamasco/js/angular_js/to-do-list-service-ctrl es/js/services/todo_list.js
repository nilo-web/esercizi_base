myApp.service("todoListItemsService", function() {
    
    this.listItems = [];

    this.getItems = function() {
        return this.listItems;
    };

    this.addItem = function(item) {
        this.listItems.push(item);
    }
});