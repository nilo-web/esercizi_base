"use strict";

var app = angular.module("myApp", ["ngRoute"]);

// configurazione
app.config(function ($routeProvider) {
  $routeProvider
    .when("/", {
      templateUrl: "/templates/registration.html",
      controller: "registrationController",
    })
    .when("/userpage/:username", {
      templateUrl: "/templates/userpage.html",
      controller: "userpageController",
    })
    .otherwise({
      redirectTo: "/",
    });
});

app.factory("userService",function($http,$q)
{
  var user = {};

  return{
    getUserByUsername: function(username)
    {
      var promessa = $q.defer();

      $http
        .get("https://jsonplaceholder.typicode.com/users?username=" + username)
        .then(function(response){
          
          user = response.data.length > 0 ? response.data[0] : {};
          promessa.resolve(user)
        }, 
          function(error){
            console.log(error);
        });

        return promessa.promise;
    },
    setUser : function(user){
      user = user;
    }

  }
})


// registration
app.controller("registrationController", function ($scope, $location,userService) {
  console.log($scope);

  $scope.sendData = function (user) {
    console.log("submit", user);

    // aggancio a scope username
    // che corrisponde al route 
    $scope.username = user.username;
    $scope.user ;



    $location.path("/userpage/" + $scope.user.username);
  };
});


// user page
app.controller("userpageController", 
function ($scope, $routeParams ,userService) {
  $scope.user;
  /*
  console.log("username da click", $scope.username);
  $scope.user;

 */
    // richiamo userService
    // faccio la get
    userService.getUserByUsername($routeParams.username)
    .then(function(data){
      $scope.user = data;
      console.log($scope.user);
    }, function(){
      console.log("Errore userpageController call user by username")
    });

});







/*
app.run([
  "$rootScope",
  "$location",
  "authService",
  function ($rootScope, $location, authService) {
    $rootScope.$on("$routeChangeStart", function (event) {
      if (!authService.isLoggedIn()) {
        console.log("DENY", $location, event);
        // event.preventDefault();
        $location.path("/");
      } else {
        console.log("ALLOW");
        // $location.path("/userpage");
      }
    });
  },
]);

app.factory("authService", function () {
  var user = true;

  return {
    setUser: function (aUser) {
      user = aUser;
    },
    isLoggedIn: function () {
      return user ? user : false;
    },
  };
});

*/
