myApp.factory("postService",function($http,$q)
{
    var listPosts = [];

    return{
        
        getPosts : function(userId)
        {
            
            var promessa = $q.defer();

                $http.get("https://jsonplaceholder.typicode.com/posts?userId=" + userId)
                    .then(function(response){
                        console.log(response.data);
                        listPosts = response.data;
                        promessa.resolve(listPosts);
                    },
                        function(error){
                            console.log(error);
                        }
                    );
            
            return promessa.promise;
        },
    }
});