myApp.factory("userService",function($http,$q) 
{
    // array vuoto 
    var listUsers = [];
    // return getUsers
    return{
        // una funzione promessa che se andrà a buon fine
        // risolverà la listUsers a cui verrà assegnata
        // la response della chiamata http
        getUsers : function()
        {
            var promessa = $q.defer();

            // farò la chiamata all'api se la lista è vuota
            if(listUsers.length === 0)
            {
                // chiamata async 
                // $http
                //  .GET(chiamata)
                //  .THEN (manipolazione del dato)
                //  .FAIL (gestione dell'errore)
                $http
                    .get("https://jsonplaceholder.typicode.com/users")
                    .then(function(response){
                        listUsers = response.data;
                        promessa.resolve(listUsers);
                    }, 
                        function(error){
                        console.log(error)
                    });
            // altrimenti resolverò la promessa con la lista già
            // riempita in un precendete chiamata
            }else
            {
                promessa.resolve(listUsers);
            }
            // con la closure return la var $q.defer.promise
            return promessa.promise;
        },
    };

});








    //"https://jsonplaceholder.typicode.com/users"