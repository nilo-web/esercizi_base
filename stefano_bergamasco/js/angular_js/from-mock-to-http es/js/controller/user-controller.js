myApp.controller("userController", [
    "$scope",
    "userService",
    "postService",
    function($scope,userService,postService)
    {
        // aggangio allo scope la lista che
        // riceverà i dati dal servizio
        $scope.listUsers;
        // richiamo il servizio e ne risolvo la promise
        userService.getUsers()
            // se va a buon fine la promessa
            // assegno alla lista nello scope i dati del servizio
            .then(function(data){
                $scope.listUsers = data;
            },// altrimenti ne gestisco l'errore
                function(){
                    console.log("Errore userController")
                }
            );
        // aggangio allo scope una function che
        // prende con param l'userid della utente corrispondente
        // alla card cliccata, in seguito passato a sua volta
        // come param per la function getPosts del servizio
        $scope.getUserByClick = function(userId)
        {
            $scope.postsClass = document.getElementsByClassName("posts").value = "";
            console.log("click",userId);
            // risolvo la promessa del servizio
            // contenente la response dalla chiamata http
            // che assegno alla listUserPosts creata e
            // agganciata allo scope
            postService.getPosts(userId)
                .then(function(response) 
                {
                    $scope.listUserPosts = response;
                },
                function(error){
                    console.log(error);
                });
        }
    },
]);