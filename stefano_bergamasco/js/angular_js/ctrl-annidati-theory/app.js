"use strict"
var myApp = angular.module("myApp", []);

myApp.controller("mioController",function()
{
    console.log("mio ctrl works");

    this.user = {
        name: "gino",
        surname: "gini"
    };
});

myApp.controller("salutaController", function ($scope) {

    $scope.saluta = function(user){
        return "Ciao " + user.name + " " + user.surname ;
    }
});