 // mycontroller inizializzo il controller
    // con $scope e il serviio todoService
    myApp.controller("createItemCtrl",function($scope,saveService) 
    {
        // creazione degli $scope
        $scope.title = "title";
        $scope.content = "content";

        /* function createItem
            params title, content in arrivo dagli input text della view
                creo un obj todo
                inserisco l'obj creato attraverso il method addTodo()
                del todoService
        */
        $scope.createItem = function()
        {
            var todo = 
            {
                title: $scope.title,
                content: $scope.content
            }
            saveService.addTodo(todo);
            console.log("servizio",saveService);
        };

    })