"use strict"
/* Creo un service toService
    instanzio una list nella quale
    attraverso il method addTodo(todo)
    aggiungo il todo passato come param
        restituisce la lista
*/
myApp.service("todoService",function()
{
    this.todoList = [] ;   
    
    this.addTodo = function(todo) {
        this.todoList.push(todo);
        
        return this.todoList;
    };

});