var myApp = angular.module("myApp",[]);


myApp.controller("myController",function($scope)
{
    // toggle boolean click user Card
    $scope.cardIsClicked = false;
    
    //$scope.commentIsClicked = true;

    // scope di model
    $scope.listUsers = listUsers;
    //console.log(listUsers)
    $scope.listPost = listPost;
    //console.log(listPost);
    $scope.listComments = listComments;
    // da usare nel showUserPost()
    $scope.userPosts = [];

    //showUserPost
    $scope.showUserPost = function (user) {
        // invereto il boolean che identifica il click sulla card
        // da spostare successivamente nella chiusura dal containerPosts
        $scope.cardIsClicked = !$scope.cardIsClicked;
        
        console.log($scope.cardIsClicked);
        $scope.userPosts = [];

        for(var i = 0; i < listPost.length; i++)
        {
            if(listPost[i].userId == user.id)
            {
                $scope.userPosts.push(listPost[i]);
            }
        }

        return $scope.userPosts;
    };

    
    /* La function showCommentsPost(user)
        prende come paramentro un post dall ng-repeat
        e restituisce la lista di commenti di quell'post
    
        */
   $scope.commentsPost = [];
   $scope.showCommentsPost = function (post) 
   {
       $scope.commentsPost = [];
       $scope.showUserPost = function (post) {
       // invereto il boolean che identifica il click sull commento
       $scope.commentIsClicked = !$scope.commentIsClicked;
       
       console.log($scope.commentIsClicked);
       $scope.commentsPost = [];

       for(var i = 0; i < listComments.length; i++)
       {
           if(listComments[i].postId == post.id)
           {
               $scope.commentsPost.push(listComments[i]);
           }
       }
       console.log($scope.commentsPost);
       return $scope.commentsPost;
   };

    

   
}});