'use strict'
/*
    Inizialmente la pagina è vuota,
    ma ogni volta che clicco sul pulsante confronta una nuova riga di testo, 
    ma le righe saranno una volta rosse e una volta blu.
*/

var button = document.getElementById("btn");
var div1 = document.getElementById("d1");

var flag = true;

/*
    return lo stato opposto 
    del valore della var flag
*/
function toggle()
{   
    flag = flag ? false : true;

    return flag;  
};


button.addEventListener("click",function()
{
    /* richiamo la funzione toggle
        che ad ogni click cambierà valore
            da true a false e viceversa
    */
    var status = toggle();
   
    // creo un paragrafo come nuovo element 
    var pNew = document.createElement("p");

    var color;

    /* assegno alla var color
        il colore rosso o blue
        a seconda del valore restituito da toggle()

    */
   // color = true ? "red" : "blue";
    if(status === true)
        color = "red"
    else
        color = "blue";

    // imposto il colore al nuovo paragrafo creato
    pNew.style.color = color;
    
    pNew.innerText = "new p color " + color; 

    // appeno al div1 il nuovo paragrafo creato
    div1.appendChild(pNew);
    
});