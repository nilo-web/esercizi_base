'use strict'
/*
Modificare le precedenti operazioni in modo inserire un altro pulsante:
 se lo schiaccio devo cancellare tutte le righe.
 */

var button = document.getElementById("btnAdd");
var div = document.getElementById("div");

// Inserimento btnDelete
var bntDelete = document.createElement("button");
bntDelete.innerText = "Delete";
bntDelete.setAttribute("id","btnDelete");
div.appendChild(bntDelete)

var col1 = "green";
var col2 = "yellow";

var flag = true;


/* return lo stato opposto 
    del valore della var flag
*/
function toggle()
{   
    flag = flag ? false : true;

    return flag;  
    
};

function x(col1,col2)
{
    button.addEventListener("click",function()
    {
        /* richiamo la funzione toggle
            che ad ogni click cambierà valore
                da true a false e viceversa
        */
        var status = toggle();
    
        // creo un paragrafo come nuovo element 
        var pNew = document.createElement("p");

        var color;

        /* assegno alla var color
            il colore dei params col1 o col2
            a seconda del valore restituito da toggle()

        */
        if(status === true)
            color = col1;
        else
            color = col2;

        // imposto il colore al nuovo paragrafo creato
        pNew.style.color = color;

        // inserisco il testo nel new p
        pNew.innerText = "new p color " + color; 

        // appeno al div1 il nuovo paragrafo creato
        div.appendChild(pNew);
        
    });
};

/* Seleziono tutti i p 
    dove ho aggiunto le righe

   aggiungo l'evento sul btnDelete
    ciclando al contrario  elimino tutti i <p>
*/
function deleteRows()
{
    var allP = div.getElementsByTagName("p");

    bntDelete.addEventListener("click",function()
    {
        console.log(allP);
        for(var i = allP.length - 1; i > -1; i --)
            allP[i].remove();
           
    })
  
}

x(col1,col2);
deleteRows();

