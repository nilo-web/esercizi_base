"use strict";

function fromDateToUtc(y,m,d)
{
    return Date.UTC(y,m,d);
}   

function diffDate()
{
    var date1 = new Date();
    var date2 = new Date("02/10/2021");

    var date1Utc = fromDateToUtc
    (
            date1.getFullYear(),
            date1.getMonth(),
            date1.getDate()
    )

    var date2Utc = fromDateToUtc
    (
        date2.getFullYear(),
        date2.getMonth(),
        date2.getDate()
    )    

    var dateDiff = date1Utc - date2Utc;

    var dayInMilliSeconds = 1000 * 60 * 60 * 24;

    var result = Math.floor(dateDiff / dayInMilliSeconds);

    console.log(date1Utc,date2Utc, dateDiff, result);

}

diffDate();