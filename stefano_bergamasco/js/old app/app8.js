/*
es 8
Scrivi una funzione che ritorna una funziona
    che ritorna il numero che è stato passato alla prima funzione.
     test es (7) () // 7.
*/
'use strict'

/* riceve un number come input
    return il number
*/
function firstFunc(param)
{
    var n = param;
    return function()
    {
        return n;
    };
}

var result = firstFunc(5);

console.log(result());
