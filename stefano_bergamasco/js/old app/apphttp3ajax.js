'use strict'
/*
All'avvio della pagina controllare nel localStorage che sia presente il valore con chiave "mieiArticoli".
    
    Se non è presente recuperarli da qui https://jsonplaceholder.typicode.com/users ed
    inserirli nel localStorage.  
    
    Dopo aver preso i dati (dal localStorage o dalla chiamata remota) stamparli a schermo.
*/
var $lista = $("#myList");
var $intestazione = $('#intestazione');

var resp = [];

var checkLs =
(
    function()
    {
        if (!localStorage["mieiArticoli"])
            return true;
        else
            return false
    }
)();

function format()
{
    var $h3 = $('h3');
    var $li = $('.list-group-item');

    if(checkLs)
    {
        // modifica test e classe h3
        $h3.text('From Api')
            .addClass('api');
        /* modifica colore ai <li> create dalla
            chiamata all' API
        */
    }
    else
    {
        // modifica test e classe h3
        $h3.text('From Local Storage')
            .addClass('lc');
        /* modifica colore ai <li> create dal
            LOCAL STORAGE
        */

        // Creazione button per pulire il LOCAL STORAGE
        var $btnClearLs = $('<button>Clear localStorage</button>');
        // aggiunta classe al button 
        $btnClearLs.addClass("btn btn-danger");
        // aggiunta attribute id al button
        $btnClearLs.attr('id','btnClearLs')
        // aggiunta del button creato al div con id intestazione 
        $intestazione.append($btnClearLs);
    }
}

function getUsers()
{
    // richiamo della function di formattazione
    format();

    if (checkLs)
    {
        /* chiamata ajax all'API in caso 
            non esiste la key ricercata
        */
        $.ajax(
        {
            type : "GET",
            url: "https://jsonplaceholder.typicode.com/users"
        })
        .done(function(response)
        {
            // assegnazione all'array resp la response
            resp = response;
            // per ogni utente dell'array resp
            for(var user in resp)
            {
                // aggiunta un <li> con il suo nome
                var $newItem = $('<li>' + resp[user].name + '</li>');
                // aggiunta classe bootstrap per <li>
                $newItem.addClass("list-group-item");
                // aggiunta attributo id al nuovo <li>
                $newItem.attr('id',"newItem");
                // aggiunta del <li> al div con id myList
                $lista.append($newItem);
    
                // richiamo function di formattazione
            }
            /* aggiunta della key "mieiArticoli" nel LOCAL STORAGE
                con il valore della response
            */
            localStorage.setItem("mieiArticoli",JSON.stringify(resp));
        })
        .fail(function(response)
        {
            console.error("Error", error);
        });
    }
    // In caso la key fosse già presente con il valore nel LOCAL STORAGE
    else
    {       
        // recupero dell array di user dal LOCAL STORAGE
        var respFromLs = JSON.parse(localStorage.getItem("mieiArticoli"));
        // per ogni utente dell'array 
        for(var user in respFromLs)
        {
            //creo nuovo <li> con il nome dell'utente
            var $newItem = $('<li>' + respFromLs[user].name + '</li>');
            // aggiunta la classe bootstrap
            $newItem.addClass("list-group-item");
            // aggiunta del <li> al div con id myList
            $lista.append($newItem);
        }
        /* evento sul button di puliza LOCAL STORAGE
            al click viene pulito lo storage
            e ricaricata la pagina così che 
            per recuperare gli utenti si ripeta la chiamata all'API
        */
        $('#btnClearLs').click(function()
        {   
            // pulizia LOCAL STORAGE
            localStorage.clear();
            // ricarica pagina
            location.reload();
        });
    }
    
}

// Richiamo di getUser();
getUsers();
