'use strict'
// Crea una funzione che stampi la data corrente in questo formato, es:
// "martedi 22, febbraio 2021"

function stampa()
{
    var days = 
    [
        "lunedi",
        "martedi",
        "mercoledi",
        "giovedi",
        "venerdi",
        "sabato",
        "domenica",
    ];
    
    var months = 
    [
        "gennaio",
        "febbraio",
        "marzo",
        "aprile",
        "maggio",
        "giugno",
        "luglio",
        "agosto",
        "settembre",
        "ottobre",
        "novembre",
        "dicembre"
    ];

    var today = new Date();

    /*  getDay() 
        recupero il giorno della settimana
        dall' array days - 1 (indice start from 0)
    */
    var currentDay = days[today.getDay() - 1];
    // console.log(days[today.getDay()]);

    /*  getDate() 
        recupero il giorno N del mese
    */
    var currentDayN = today.getDate();

    /*
        getMonth()
        reupero il mese 
    */
    var currentMonth = months[today.getMonth()];

    /*
        getFullYear()
        recupero l'anno
    */
    var currentYear = today.getFullYear();

    // ritorna la concatenazione di giorno mese anno
    return currentDay + " " + currentMonth + " " + currentYear;
}

console.log(stampa());