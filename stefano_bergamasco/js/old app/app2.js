'use strict'
//Fai il reverse di questa stringa: "supercalifragilistichespiralidoso"

var s = "supercalifragilistichespiralidoso";

/* la funzione reverse(s)
    inverte la stringa trovata come param
    utilizzando i metodi
        split("") 
            che sposta i caratteri della stringa
            in un array
        reverse()
            che inverte l'array creato
        join("")
            che unisce i caratteri dell array
            senza usare separatori ""
*/
function reverse(s)
{
    return s.split("").reverse().join("");
}

console.log(reverse(s));

