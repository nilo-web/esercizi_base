'use strict'
// Concatena, senza duplicati, i seguenti array e ordinali per valore crescente

var array1 = [20, 33, 12, 1233];
var array2 = [1, 33, 102, -12];

/* Creo nuovoArray vuoto
    per poter iserire gli elementi 
    confrontati con array1 
    e successivamente 
    da confrontare con array2
*/
var nuovoArray = [];


for(var i = 0; i < array1.length; i++)
{
    // popolo l'arrayItem con gli elementi di array1
    var arrayItem = array1[i];
    /* inserisco in nuovoArray
       gli elementi di array1
        se il nuovoArray(in partenza vuoto)
        non contiene l'elemento di arrayItem di indicizzazione iesima
        cioè
        gli elementi di array1
        questo viene verificato 
        dal return -1 del method indexOf() se non viene trovato
        se invece nuovoArray() contiene l'elemento posto come parametro di indexOf(elemento),
        viene restituito l'indice a cui si trova tale l'elemento
    */
    if(nuovoArray.indexOf(arrayItem) === -1)
        nuovoArray.push(arrayItem);
}

/* stessa cosa tra nuovoArray e array2
     popolando questa volta 
     l'arrayItem con gli elementi di array2
*/
for(var i = 0; i < array2.length; i++)
{
    var arrayItem = array2[i];

    if(nuovoArray.indexOf(arrayItem) === -1)
        nuovoArray.push(arrayItem);
}

/* assegno a result
    il risultato dell'operazione di sorting 
    sugli elementi di nuovoArray
*/
var result = 
    nuovoArray.sort(function(a,b)
    {
       return a - b;
    });


console.log(result);