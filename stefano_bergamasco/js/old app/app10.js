//Scrivi una funzione che ritorna un numero e lo incrementi ogni volta che viene chiamata (IFFE)
'use strict'
/* Assegno a var incr
    il risultato 

    passo alla function(n) anonima un parametro n
    all'interno della stessa 
    creo una var numInterno a cui assegno il valore del parametro

    faccio una closure
        che return un'altra function anonima
            che return l'incremento della var numIntero
            creata nella precedente function anonima
*/
var incr =
    (
        function(param)
        {
            var numInterno = param;
            return function()
            {
                return ++numInterno;
            }
        }
    )(2); // Ricordarsi il parametro da inserire all'IFFE

console.log(incr());
console.log(incr());
console.log(incr());
