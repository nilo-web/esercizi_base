/* Scrivi una funzione che ritorni una funzione
     che ritoni il numero incrementato
      che è stato passato alla prima funzione. 
Ogni volta che la si chiama il numero viene incrementato di uno
*/
'use strict'

/*
    return una function anonima
        che a sua volta return un nInterno
        incrementato ogni volta che viene chiamata la funzione
*/
function contatore(param)
{
    var nInterno = param;

    return function ()
    {
       return ++nInterno;
    };
};

/* assegno ad una variabile result
    la funzione resultNumber(parametro il numero da incrementare)
*/
var result = contatore(4);

/*
    stampo più volte per vedere l'incremento
*/
console.log(result());
console.log(result());
console.log(result());