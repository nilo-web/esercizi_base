'use strict'
/*
All'avvio della pagina controllare nel localStorage che sia presente il valore con chiave "mieiArticoli".
    
    Se non è presente recuperarli da qui https://jsonplaceholder.typicode.com/users ed
    inserirli nel localStorage.  
    
    Dopo aver preso i dati (dal localStorage o dalla chiamata remota) stamparli a schermo.
*/

var $lista = $("#myList");

var httpReq = new XMLHttpRequest();

var resp = [];

/* Controllo nel LS la presenza del valore 
    per la key "mieiArticoli"
*/
//localStorage.clear();
if (!localStorage["mieiArticoli"])
{
    console.log("no value");
    /* alla scatenarsi dell'evento
        controllo la chiamata sia terminata e andata a buon fine
        parso il risultato in JSON e 
            lo inserisco come param nella function per renderizzare il resultato
        inserisco nel Localstorage la risposta
    */
    httpReq.onreadystatechange = function()
    {
        if(httpReq.readyState === 4 && httpReq.status === 200)
        { 
            resp = JSON.parse(httpReq.response);
            console.log("SI",resp);
            
            // renderizzo su pag html
            for(var i = 0 ; i < resp.length; i ++)
            {
                var $newItem = $('<li>' + resp[i].name + '</li>');
                $lista.append($newItem);
             }
            // inserimento nel localStorage
            localStorage.setItem("mieiArticoli",JSON.stringify(resp));
        }
    };
    httpReq.open
        ("GET","https://jsonplaceholder.typicode.com/users");
    httpReq.send();
}
/* Se invece nel Localstorage è gia esistente 
    una key con valore "mieiArticoli"
    recupero le info da renderizzare direttamente dal Localstorage
*/
else
{
    // recupero e parso la string con key "mieiArticoli" dal localstorage
    var respFromLs = JSON.parse(localStorage.getItem("mieiArticoli"));  
    console.log("dal localstorage -> ", respFromLs);
    // renderizzo su pag html
    for(var i = 0 ; i < respFromLs.length; i ++)
    {
        var $newItem = $('<li>' + respFromLs[i].name + '</li>');
        
        $lista.append($newItem);
     }  
}
