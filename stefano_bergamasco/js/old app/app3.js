'use strict'
//Ordina il seguente array con i giorni della settimana in base al giorno della corrente:

var  daysOfWeek  =  
    [ "lunedi" ,  "martedi" ,  "mercoledi" ,  "giovedi" ,  "venerdi" ,  "sabato" ,  "domenica" ];


/* Creo due nuovi array
    Head che conterrà il giorno odierno(today)
    più il seguito dell'array daysOfWeek
    senza
    il giorno precedente a today 
    che verrà invece
    inserito nell' array Tail
*/
var newArrayHead = [];
var newArrayTail = [];

/* creo un nuovo obj di tipo Date today
    con cui svolgere altre operazioni sulla Date
    utili allo svolgimento dell'esercizio
    oltre ad indicare il giorno odierno
*/
var today = new Date();

/*
    attraverso l'indice di today ricavato
    utilizzando il metodo getDay(),
    applicato all'array daysOfweek
    e sottraendo ad esso 1 
    ottengo 
    l'indice del giorno precedente, 
    utile
    per spezzare l'array daysOfWeek 
    e quindi
    inserire quest'ultimo indice nell array Tail 
    attraverso il metodo slice()
*/
var indexCurrent = today.getDay() - 1;

//console.log("index giorno settimana ", today.getDay());
//console.log("index precedente a today ", indexPrev);

/* divido array in 2 parti
    nella 1a parte
    assegno all'array Head
    la porzione dell'array daysOfWeek, 
    a partire
    dall' indice del giorno odierno
    ricavato a riga 41
    e come ultimo elemento 
    l'elemento finale dell'array daysOfWeek
    utilizzando il metodo slice(start, end)
*/
newArrayHead = daysOfWeek.slice(indexCurrent, daysOfWeek.length)
//console.log("array inizio ", newArrayHead);

/* Aggiungo 2a parte mancante
    Nella 2a parte
    assegno all'array Tail 
    la porzione dell'array dayOfWeek restante,
    ovvero i giorni della settimana precedenti al giorno odierno
    a partire 
    dal suo primo indice 0
    fino al indice dell'array daysOfWeek 
    relativo 
    al giorno precedente ad oggi
*/
newArrayTail = daysOfWeek.slice(0,indexCurrent);
//console.log("array fine ", newArrayTail);

// concateno l'array Tail all'array Head
newArrayHead = newArrayHead.concat(newArrayTail);

console.log("riusultato", newArrayHead);