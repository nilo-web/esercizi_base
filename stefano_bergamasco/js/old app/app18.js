'use strict'
/*
Aggiungere un terzo pulsante.
 Questo pulsante, quando calcato, 
 cambierà il colore di sfondo del div che contiene il testo.
*/


var button = document.getElementById("btnAdd");
var div = document.getElementById("div");


// Inserimento btnDelete
var bntDelete = document.createElement("button");
bntDelete.innerText = "Delete";
bntDelete.setAttribute("id","btnDelete");
div.appendChild(bntDelete)

// Inserimento btnColor
var btnColor = document.createElement("button");
btnColor.innerText = "Change Color";
btnColor.setAttribute("id","btnColor");
div.appendChild(btnColor);

var col1 = "green";
var col2 = "yellow";

var flag = true;


/* return lo stato opposto 
    del valore della var flag
*/
function toggle()
{   
    flag = flag ? false : true;

    return flag;  
    
};

/* Seleziono il div a cui voglio cambiare colore
    a seconda del risultato della function toggle()
        cioè true or false
    lo sfondo del div diventa o rosso o bianco
*/
function changeColor()
{
    btnColor.addEventListener("click",function()
    {
        
        var toChange_toDel = document.getElementById("div");
        console.log(toChange_toDel);

        var color = toggle() ? "white" : "red";

        toChange_toDel.style.backgroundColor = color;
    });
};

function x(col1,col2)
{
    button.addEventListener("click",function()
    {
        /* richiamo la funzione toggle
            che ad ogni click cambierà valore
                da true a false e viceversa
        */
        var status = toggle();
    
        // creo un paragrafo come nuovo element 
        var pNew = document.createElement("p");

        var color;

        /* assegno alla var color
            il colore dei params col1 o col2
            a seconda del valore restituito da toggle()

        */
        if(status === true)
            color = col1;
        else
            color = col2;

        // imposto il colore al nuovo paragrafo creato
        pNew.style.color = color;
        // assegno la classe toDel utile per l'eliminazione
        pNew.classList.add("toDel");
        // inserisco il testo nel new p
        pNew.innerText = "new p color " + color; 

        // appeno al div1 il nuovo paragrafo creato
        div.appendChild(pNew);
        
    });
};

/* Seleziono tutti gli Elements 
    con classe "toDel" e li assegno ad una HTMLColletion

    ciclando al contrario  elimino tutti gli elementi
    contenuti nella HTMLCollection

    + div contenitore ritorna bianco
*/
function deleteElement()
{
    
    var toDel = document.getElementsByClassName("toDel");

    console.log(toDel);

    bntDelete.addEventListener("click",function()
    {
        console.log(toDel);
        for(var i = toDel.length - 1; i > -1; i --)
            toDel[i].remove();

        document.getElementById("div").
            style.backgroundColor = "white";
        
            
           
           
    })
 
}

x(col1,col2);
deleteElement();
changeColor();

