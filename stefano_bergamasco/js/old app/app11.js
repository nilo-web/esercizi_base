var fibo =
    (
        function()
        {
            var n1 = 0;
            var n2 = 1;
            var temp =0;
           return function()
            {
                temp = n1 + n2;
                n1 = n2;
                n2 = temp;

                return temp;
            }
        }
    )();

console.log(fibo());
console.log(fibo());
console.log(fibo());
console.log(fibo());