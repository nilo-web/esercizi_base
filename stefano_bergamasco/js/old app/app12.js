'use strict'
/*
Inizialmente la pagina è vuota, ma ogni volta che clicco sul pulsante 
compare una nuova riga di testo.
*/

var button = document.getElementById("btn");
var div1 = document.getElementById("d1");

/* creo un var incr 
    a cui assegno una function(param)
        che avrà come param l'inzio del conteggio(0)
        e restituisce
            l'incremento di tale param
*/
var incr =
    (
        function(param)
        {
            var counter = param;
            return function()
            {
                return ++counter;
            }
        }
    )(0);

/* Allo scatenarsi dell'evento click
    creo un <p>
        all'interno del quale verrà inserito :
            il testo, con un contatore il cui valore
            è restituito dalla var incr() e quindi
            dalle sue callback
    infine appendo al div1 selezionato dal DOM 
    il nuovo <p> creato
*/
button.addEventListener("click", function()
{       
    var pNew = document.createElement("p");
    
    pNew.innerText = "inserimento n: " + incr();
    div1.appendChild(pNew); 
});