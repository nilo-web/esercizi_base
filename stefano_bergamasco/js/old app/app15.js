'use strict'
/*
Fare in modo che la funzione creata nel punto precedente
 prenda in ingresso due parametri: 
 devono essere entrambi delle stringhe che rappresenteranno i due colori. 
 Quindi invocare la funzione una unica volta passandogli i colori "green" e "yellow".
*/

var button = document.getElementById("btn");
var div1 = document.getElementById("d1");

var col1 = "green";
var col2 = "yellow";

var flag = true;

/* return lo stato opposto 
    del valore della var flag
*/
function toggle()
{   
    flag = flag ? false : true;

    return flag;  
    
};

function x(col1,col2)
{
    button.addEventListener("click",function()
    {
        /* richiamo la funzione toggle
            che ad ogni click cambierà valore
                da true a false e viceversa
        */
        var status = toggle();
    
        // creo un paragrafo come nuovo element 
        var pNew = document.createElement("p");

        var color;

        /* assegno alla var color
            il colore dei params col1 o col2
            a seconda del valore restituito da toggle()

        */
        if(status === true)
            color = col1;
        else
            color = col2;

        // imposto il colore al nuovo paragrafo creato
        pNew.style.color = color;
        
        pNew.innerText = "new p color " + color; 

        // appeno al div1 il nuovo paragrafo creato
        div1.appendChild(pNew);
        
    });
};

x(col1,col2);
