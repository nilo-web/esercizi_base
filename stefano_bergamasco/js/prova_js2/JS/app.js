"use strict"

function onClickCard()
{
    var $btnClearPost = $('#btnClearPosts');
    var $postContainer = $('#postsContainer');
    // al click del <div> che ha come id="card"
    $('div.card').on("click", function()
    {
        /* ricerco richiamando con il this 
            il <div> selezionato
            ne estraggo quindi il numero che rappresenta l'id dell utente
            utilizzando lo slice sul valore dell' attributo id 
            che si trova a posizione 4 usando lo slice -> user[N]
        */
       
        // mostro il button per pulire i posts
        $btnClearPost.show();
        $postContainer.show();

        var id = (this.id).slice(4);
        console.log(this);

        // Richiamo il getPost che ritorna il risultato della done ajax
        // contente i post di un user specifico
        getPosts(id);
    });

    $($btnClearPost).on("click", function()
    {
        $btnClearPost.hide();
        $postContainer.hide();
    });
  
}

