"use strict"

// Crazione User per i dati da inserire nelle card
function User(id,name,username,email)
{
    this.id = id;
    this.name = name;
    this.username = username;
    this.email = email;
}

/* Attraverso una IFEE recupero i dati dall'api
    e li inserisco nelle rispettive card
*/

(function getUsers()
{
    // creo un Deferred per la promise
    var $deffered = $.Deferred();
    // resto in ascolto dell'evento onClickCard
    $deffered.then(onClickCard);

    // faccio una get con ajax
    $.ajax({
        type: "GET",
        url: "https://jsonplaceholder.typicode.com/users",
    })
    .done(function(response){
        /* In caso  ready state è 4 e lo status 200
            ciclo la response e creo un obj User per ogni
            user dell'array ritornato
        */
        for(var i = 0; i <= response.length - 1; i++)
        {
            var user = new User(
                response[i].id,
                response[i].name,
                response[i].username,
                response[i].email
            );
            
            //inserisco le card nella pagina
            addCard(user);
        }

        // Risoluzione della promise
        $deffered.resolve();
    })
    .fail(function(response){
        // In caso di fallimento stampo l'errore
        console.log("Error", console.error());
    });

})();

/*
    La funzione prende come input un user
    creato nel done della chiamata ajax
    Crea un div per ognuno di essi
    Appende il contenuto nel container con id="cardContainer"
*/
function addCard(user) {
        /* Creazione del div
            inserimento class=card
            inserimento dell'id corrispondente
                all'id dell'user passato come argomento della fuction
        */
        var $card =
            $(
                "<div class='col-md-4 card' id='user" + user.id + "'>" +
                    "<p>" +  user.name + "</p>" + 
                    "<p>" + user.username + "</p>" + 
                    "<p>" + user.email + "</p></div>"
            );
        
        // appendo il div al containerCard
        var $container = $('#cardContainer');
        $container.append($card);
}


