'use strict'

// Creazione costruttore di un obj Post con attributi
function Post(userId,id,title,body)
{
    this.userId = userId;
    this.id = id;
    this.title = title;
    this.body = body;
}

/*
    Questa funzione prende come parametro un idUser
        cosi quando verrà richiamata
        andrò alla ricerca dei post di uno specifico utente
*/
function getPosts(sId)
{
    // Creazione di un Deffered per il ritorno della promise
    var $deferred = $.Deferred();
    
    // chiamata get con ajax
    $.ajax(
    {
        type: "GET",
        url: "https://jsonplaceholder.typicode.com/posts",
        // assegnazione a proprieta data dell argomento della fuction userId 
        data: 
            {userId: sId}
    })
    .done(function(response)
    {
        /* In caso  ready state è 4 e lo status 200
            ciclo la response e creo un obj Post per ogni
            post restituito dall'array 
        */

         // pulisco i post (x risolvere il problema di una connessione scarsa)
        $('#postsContainer').html("");
        
        for(var i = 0; i < response.length -1; i ++)
        {
            var post = new Post
            (
                response[i].userId,
                response[i].id,
                response[i].title,
                response[i].body
            );
            
          // console.log(post);
            addPost(post);
        }
        // Risoluzione della promise
        $deferred.resolve();


    })
    .fail(function(response){
        // In caso di fallimento stampo l'errore
        console.log("Error", console.error());
    });

};

/* La funzione prende come input un post
    restituito dalla done della chiamata
    Per ogni post crea un <div>
    che contengono dei <p> con le proprietà del post

*/
function addPost(post)
{
    /*
        inserisco nel div classe di formattazione col 
        e l'id che ha come valore l'id del relativo post
    */
   var $post = $(
        "<div class='col-md-12 post' id='post" + post.id + "' >" + 
            "<h2>" + post.title + "</h2>" +
            "<p>" + post.body + "</p>" + 
        "</div>");

    //var $post = $("<div>"+ post.title+"</div>");
    // appendo al conteiner dei post il post creato                
    var $container = $('#postsContainer');
    $container.append($post);

}




