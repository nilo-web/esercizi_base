"use strict";

var myApp = angular.module("myApp", []);

myApp.controller("myController", function($scope) {
    console.log("$scope", $scope);

    $scope.padella = false;

    $scope.name = "-placeholde name-";
    $scope.surname = "-placeholder surname-";
    $scope.username = "-placeholder username-";
    $scope.email = "-placeholder email-";

    $scope.title = "-placeholder title-";
    $scope.text = "-PLACEHOLDER TEXT-";

    $scope.listaUtenti = listaUtenti;
    $scope.listaPost = listaPost;
    $scope.listaCommenti = listaCommenti;
    
    $scope.contatore = 0;
    $scope.contaCommenti = function () {
      for ($scope.post in $scope.listaPost){
        
        for ($scope.commento in $scope.listaCommenti){
          
          if($scope.listaPost.id === $scope.listaCommenti.postId){
            //console.log("AAAAAAAAAAAAAAAAAAAAAA")
            $scope.contatore++;
          }
        }
      }
    }

    $scope.toggleShow = false;
    $scope.btnTxt = "Show User Post";
    $scope.showPost = function(){
      if($scope.toggleShow){
        $scope.toggleShow = false;
        $scope.btnTxt = "Show User Post";
      }
      else{
        $scope.toggleShow = true;
        $scope.btnTxt = "Hide User Post";
      }
    }
});

