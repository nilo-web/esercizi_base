"use strict";

var myApp = angular.module("myApp", []);

myApp.controller("myController", function($scope){
   
    $scope.todoList = [];

    $scope.addElement = function(){
        if($scope.todoList.length===0){
            var todoElement = {
                title: $scope.todoTitle,
                text: $scope.todoText
            }
            $scope.todoList.push(todoElement);
            $scope.todoTitle = "";
            $scope.todoText = "";
        }
        else{
            var duplicate = false;
            for(var i = 0; i < $scope.todoList.length; i++){
                var element = $scope.todoList[i];
                if (element.title==$scope.todoTitle){
                    duplicate = true;
                    break;
                }
                else{
                    duplicate = false;
                }
            }

            if(duplicate){
                console.log("element already present in list");
            }
            else{
                var todoElement = {
                    title: $scope.todoTitle,
                    text: $scope.todoText
                }
                $scope.todoList.push(todoElement);
                $scope.todoTitle = "";
                $scope.todoText = "";
            }
        }
    }
});