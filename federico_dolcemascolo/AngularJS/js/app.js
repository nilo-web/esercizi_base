"use strict";

var myApp = angular.module("myApp", [ngRoute]);

myApp.config(function ($routeProvider){
    $routeProvider.when("/", {
        templateUrl: "/template/login.html",
        controller: "loginController"
    }).when("/profile/:username", {
        templateUrl: "/template/profile.html",
        controller: "profileController"
    }).otherwise({
        redirectTo: "/"
    })
});

myApp.controller("loginCtrl", ["$scope", "$location", function($scope, $location){
    $scope.loggami = function(username){
        $location.path("/profile/" + username);

    }
}]);

myApp.controller("profileCtrl", ["$scope", "$routeParams", function($scope, $routeParams){
    $scope.username = $routeParams.username;
}]);