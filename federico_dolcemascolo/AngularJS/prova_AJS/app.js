"use strict";

var app = angular.module("myApp", []);

app.controller("myController",["$scope", "$http", function($scope, $http){
    $scope.listaUtenti = [];
    $http.get('https://jsonplaceholder.typicode.com/users')
    .then(function(r){
        console.log(r.data);
        for(var i = 0; i < r.data.length; i++){
            $scope.listaUtenti.push(r.data[i]);
            console.log($scope.listaUtenti[i]);
        }
        $scope.listaPost = listaPost;
        $scope.listaCommenti = listaCommenti;
        $scope.displayPost = function (id){
            $scope.tempList = [];
            var c;
            for (var i = 0; i < $scope.listaPost.length; i++){
                if($scope.listaPost[i].userId === id){
                    c = 0;
                    var post = $scope.listaPost[i].title;
                    for (var j = 0; j < $scope.listaCommenti.length; j++){
                        if($scope.listaCommenti[j].postId===$scope.listaPost[i].id){
                            c++;
                        }
                    }
                    $scope.tempList.push({title: post, comment: c});
                }
            }
        }
    })
}])