"use strict";
function Print(string) {console.log(string);}


/*
*   Scrivi una funzione che ritorna una funziona che ritorna il numero che è stato passato alla prima funzione.
    es test(7)() //  7.

*   Scrivi una funzione che ritorni una funzione che ritoni il numero incrementato che è stato passato alla prima funzione. 
    Ogni volta che la si chiama il numero viene incrementato di uno

*   Scrivi una funzione che ritorna un numero e lo incrementi ogni volta che viene chiamata (IFFE)

*   Scrivi una funzione che ogni volta che viene chiamata ritorni il numero successivo della serie di fibonacci
*/

Print("#1 es");
//PRIMO ESERCIZIO
function gibeNumber(numero) {
    var param = numero;
    return function() {
        return param;
    }
}
Print(gibeNumber(7)());


Print("#2 es");
//SECONDO ESERCIZIO
function incremNum(numero){
    var param = numero;
    return function(){
        return param++;
    }
}
var funz = incremNum(3);
for (var i = 0; i < 5; i++) {
    Print(funz());
}


Print("#3 es");
//TERZO ESERCIZIO
var funky = (function() {
    var num = 0;
    return function() {
        num++;
        Print(num);
    }
})();
for (var i = 0; i < 5; i++) {
    funky();
}


Print("#4 es");
//QUARTO ESERCIZIO
var feebonacci = (function funk(){
    var first = 0;
    var second = 1;
    var third;
    return function (){
        third = second + first;
        first = second;
        second = third;
        return first;
    }
})();
var feeb = feebonacci();
Print(feeb);


