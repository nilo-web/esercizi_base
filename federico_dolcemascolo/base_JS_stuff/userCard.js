"use strict";

function UserCard(userObj){
    this.id = userObj.id;
    this.name = userObj.name;
    this.username = userObj.username;
    this.email = userObj.email;
    this.$tpl = $('<div class="card-utente"></div>');
}

UserCard.prototype.renderCard = function(){
    var cardTextCont = `${this.name} - ${this.username}: ${this.email}`
    this.$tpl.html(cardTextCont);
    tits.$tpl.attr("data-id-user", this.id);
    this.$tpl.on("click", this.clickHandler.binf(this));
    $container.append(this.$tpl);
}

UserCard.prototype.clickHandler = function(event){
    console.log("clicked", this.event);
    this.$tpl.toggleClass("active");
}