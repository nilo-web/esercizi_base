"use strict";
function Print(string) {console.log(string);}

/*
*   Inizialmente la pagina è vuota, ma ogni volta che clicco sul pulsante compare una nuova riga di testo,
    ma le righe saranno una volta rosse e una volta blu.

*   Mettere tutto quello fatto nell'esercizio precedente dentro una funzione. 
    uindi invocare la funzione due volte: il risultato deve essere lo stesso dell'esercizio 2.

*   Fare in modo che la funzione creata nel punto precedente prenda in ingresso due parametri:
    devono essere entrambi delle stringhe che rappresenteranno i due colori. Quindi invocare la funzione una unica volta passandogli i colori "green" e "yellow".

*   Modificare l'esercizio precedente in modo inserire un altro pulsante: se lo schiaccio devo cancellare tutte le righe inserite.

*   Creare una funzione che prende in ingresso un container e lo pulisce da tutti i suoi figli. 
    Invocarla nell'esercizio precedente quando ci serve pulire il div che contiene le scritte.

*   Aggiungere un terzo pulsante. Questo pulsante, quando calcato, cambierà il colore di sfondo del div che contiene il testo.

*   Fare in modo che il terzo pulsante appena aggiunto, se ricalcato una seconda volta, ripristini il colore di sfondo
    del testo a bianco.

*   Inserire un quarto pulsante "Autodistruzione". Questo pulsante elimina tutto il contenuto della pagina, compreso se stesso.
*/
var titolo = document.getElementsByTagName("h1")
var bottone = document.getElementById("btn");
var deleteBtn = document.getElementById("dltbtn");
var linesBcb = document.getElementById("linesBckgrnd");
var autodistruzioneBtn = document.getElementById("deleteAll");
var h2 = document.getElementsByTagName("h2");
var container = document.getElementById("container");
var linesContainer = document.getElementById("lines");
var input1 = document.getElementById("input1");
var input2 = document.getElementById("input2");
var count = 0;

//TYPE DESIRED COLORS IN INPUT AREAS AND PRESS BUTTON TO EXECUTE
{
function BTN_Click(color1, color2) {
    console.log(arguments);
    var spanElement = document.createElement("p");
    var textLine;
    
    if (count%2)
    {
        spanElement.style.color = color1;
        textLine = color1;
    }
    else
    {
        spanElement.style.color = color2;
        textLine = color2;
    }

    count++;
    spanElement.innerText = textLine + " " + count;
    linesContainer.appendChild(spanElement);
}
bottone.addEventListener("click", function(){
    var first_color = input1.value;
    var second_color = input2.value;
    return BTN_Click(first_color, second_color);
});
}

//PRESS DELETE BUTTON TO DELETE ALL CREATED LINES
deleteBtn.addEventListener("click", function(){
    var pElement = document.getElementsByTagName("p");
    for (var i = pElement.length - 1; i > -1; i--)
    {
        pElement[i].remove();
    }
    count = 0;
});

//PRESS BUTTON TO SWITCH LINES BACKGROUND COLOR
linesBcb.addEventListener("click", function(){
    var bgColor = linesContainer.style.backgroundColor;
    bgColor == "red" ? linesContainer.style.backgroundColor="white" : linesContainer.style.backgroundColor="red";
});

//PRESS BUTTON TO CLEAR THE PAGE OF EACH CONTAINER ELEMENT
autodistruzioneBtn.addEventListener("click", function(){
    container.remove();
});

