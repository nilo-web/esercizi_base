"use strict";
function Print(string){console.log(string)};


//BASE DOM INTERACTIONS
{

function myFunction(firstColor, secondColor) {
  // container dei bottoni
  var myContainer = document.getElementById("container");

  // container dei p
  var myTextContainer = document.getElementById("textContainer");

  // genero il primo bottone
  var myButton = document.createElement("button");
  myButton.innerText = "Click here";
  myContainer.appendChild(myButton);

  // genero il secondo bottone
  var clearBtn = document.createElement("button");
  clearBtn.innerText = "Reset";
  myContainer.appendChild(clearBtn);

  // gestione colori
  let myLastColor = firstColor;

  var c = 0;
  
  // eventListener per aggiungere un elemento p
  myButton.addEventListener("click", function () {

      c++;

      // creo l'elemento
      var myParagraph = document.createElement("p");
      myParagraph.innerText = "Hello world! " + c;
      myParagraph.setAttribute("id", "paragraph " + c);
      myParagraph.style.color = myLastColor;

      myTextContainer.appendChild(myParagraph);

      // gestione colori
      if (myLastColor === firstColor) {
          myLastColor = secondColor;
      } 
      else {
          myLastColor = firstColor;
      }
  });
  
  myTextContainer.addEventListener("click", function(event) {
    alert("ciao sono l'elemento " + event.target.getAttribute("id"));
  });

  // eventListener per cancellazione nodi
  clearBtn.addEventListener("click", function() {
    // utilizzo il while mettendo come condizione il fatto che la lunghezza dei childNodes 
    // sia uguale a zero in modo da non avere problemi con la rimozione degli elementi 
    // a causa del cambiamento degli index
    while (myTextContainer.childNodes.length > 0) {
      myTextContainer.childNodes[0].remove();
      c = 0;
    }
  });

}

myFunction("green", "yellow");

}

//INTERVAL AND TIMEOUT
{

//Print(1);
//
//var tO = setTimeout(function() {
//  Print(2 + " timeout");
//  clearInterval(iV);
//}, 5000);
//
//Print(3);
//
//var iV = setInterval(function(){
//  Print(4 + " interval");
//}, 1000);

}

//JQUERY INTRO
{

var $container = $("#esempio-jquery");
var $lista = $container.find("ul");
var $elementiListaRossi = $lista.find(".rosso");


$elementiListaRossi.addClass("testo-grande");

if ($elementiListaRossi.hasClass("testo-grande")) {
  $elementiListaRossi.removeClass("testo-grande");
}

$elementiListaRossi.attr("data-pigna", "pigne");

$container.append("sono appeso");
$container.append("<p><b>sono appeso come P</b></p>");

var $template = $(`
  <div>
    <h1>
      ciao sono un titolo inserito a runtime
    </h1>
  </div>
`);

$container.append($template)

$container.on("click", function (e) {
  //console.log(e)
});


$.fn.tuttoMaiuscolo = function() {
  console.log(this);
  this.text(this.text().toUpperCase());
};

}

//HTTP REQUESTS
{

//var dati = {
//  title: "foo",
//  body: "bar",
//  userId: 1
//}
//
//var dati2 = {
//  title: "nic",
//  body: "ola",
//  userId: 8
//}

//Native JS
{


//var httpReq = new XMLHttpRequest();
//httpReq.onreadystatechange = function(){
//  if (httpReq.readyState === 4 && httpReq.status === 201){
//    console.log("SI", httpReq.response);
//  }
//}
//httpReq.open("POST", "https://jsonplaceholder.typicode.com/posts");
//httpReq.setRequestHeader("Content-Type", "application/json")
//httpReq.send(JSON.stringify(dati));
//
//var httpReq2 = new XMLHttpRequest();
//httpReq2.onreadystatechange = function(){
//  if (httpReq2.readyState === 4 && httpReq2.status === 201){
//    console.log("SI", httpReq2.response);
//  }
//}
//httpReq2.open("POST", "https://jsonplaceholder.typicode.com/users");
//httpReq2.setRequestHeader("Content-Type", "application/json")
//httpReq2.send(JSON.stringify(dati2));


}

//JQuery
{


//var $lista = $("#nuovaLista");
//
//var chiamataAjax = $.ajax({
//  type: "GET",
//  url: "https://jsonplaceholder.typicode.com/posts"
//});
//
//chiamataAjax.done(function(response){
//  console.log("risposta", response);
//
//  var $nuovoItem = $('<li>' + response[0].title + '</li>');
//  $lista.append($nuovoItem);
//});
//
//chiamataAjax.fail(function(error){
//  console.log("errore", error);
//});

//var callPost = $.ajax({
//  type: "POST",
//  url: "https://jsonplaceholder.typicode.com/posts",
//  dataType: "json",
//  data: JSON.stringify(dati)
//});
//callPost.done(function(response){
//  console.log(response);
//});
//callPost.fail(function(error){
//  console.log(error);
//});
//
//var callUser = $.ajax({
//  type: "POST",
//  url: "https://jsonplaceholder.typicode.com/users",
//  dataType: "json",
//  data: JSON.stringify(dati2)
//});
//callUser.done(function(response){
//  console.log(response);
//});
//callUser.fail(function(error){
//  console.log(error);
//});


}

}

//DEFERRED
{

//console.log(1);
//
//var deferred = $.Deferred();
//Print(2);
//
//deferred.then(
//  function(){
//    console.log("3 successo");
//  },
//  
//  function(){
//    console.log("3 error");
//  }
//);
//Print(4);
//
//deferred.resolve();
//Print(5);

}

//LOCAL STORAGE
{

localStorage.setItem("prova", "provato");
Print(localStorage.getItem("prova"));
localStorage.removeItem("prova");


////All'avvio della pagina controllare nel localStorage che sia presente il valore con chiave "mieiArticoli". 
////Se non è presente recuperarli da qui https://jsonplaceholder.typicode.com/users ed inserirli nel localStorage. 
////Dopo aver preso i dati (dal localStorage o dalla chiamata remota) stamparli a schermo.

if (localStorage.getItem("mieArticoli") === "Articoli Assortiti"){
  Print("Articoli presenti");
}
else{
  Print("Articoli non presenti, inserimento");
  var $chiamataPost = $.ajax({
    type: "POST",
    url: "https://jsonplaceholder.typicode.com/users",
    dataType: "json",
    data: {
      mieiArticoli: "Articoli Assortiti"
    }
  })
  .done(function (r){
    Print(r.mieiArticoli);
    localStorage.setItem("mieiArticoli", "Articoli Assortiti");
    Print(localStorage.getItem("mieiArticoli"));
  })
  .fail(function(){
    Print("ERROR");
  });
}

}

//SINGLETON
{

var singleton = (function() {
  var instance;

  return{
    getInstance: function() {
      if (!instance) {
        instance = {
          attributo: 123,
          metodo: function(){

          }
        }
      }

      return instance;
    }
  }
})();

}