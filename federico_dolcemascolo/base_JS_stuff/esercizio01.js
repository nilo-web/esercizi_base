'use strict';

//merge without diplicates, sort
var array1 = [20, 33, 12, 1233];
var array2 = [1, 33, 102, -12];

//sort starting from today
var giorniSettimana = ["lunedi", "martedi", "mercoledi", "giovedi", "venerdi", "sabato", "domenica"];

//sort objects based on IDs
var objectsList = [
    { name: "Pino", id: 66 },
    { name: "Rino", id: 16 },
    { name: "Dino", id: 6 },
    { name: "Lino", id: 96 },
    { name: "Gino", id: 26 }
   ];

//reverse string
var stringa = "supercalifragilistichespiralidoso";

// Crea una funzione che stampi la data corrente in questo formato, es: "martedi 22, febbraio 2021"

// Crea una funzione che ritorni la differenza tra 2 date in giorni (usare Date.UTC())

// Crea una funzione che data un array di date ritorni la data più alta
var listaDate = [
    {year: 2021, month: 2, day: 23},
    {year: 1998, month: 9, day: 2},
    {year: 1997, month: 9, day: 10},
    {year: 1980, month: 11, day: 17},
    {year: 2003, month: 3, day: 23}
];


//SOLUZIONI\\\\\\\\\\\\\\\\\\\

function SortArrays(first_array, second_array)
{
    for (var i = 0; i < first_array.length; i++)
    {
        for (var l = 0; l < second_array.length; l++)
        {
            if (first_array[i] == second_array[l])
            {
                second_array.splice(l, 1);
            }
        }
    }
    var newArray = first_array.concat(second_array);
    newArray.sort(function(a, b){return a-b});
    console.log(newArray);
}

function SortWeek(week)
{
    var data = new Date();
    var oggi = data.getDay()
    for (var i = 0; i < week.length; i++)
    {
        if (i == oggi)
        {
            var tempArray = week.slice(0, i - 1);
            var newWeek = week.concat(tempArray);
            newWeek.splice(0, i - 1);
        }
    }
    console.log(week);
    console.log(newWeek);
}

function SortObjList(objList)
{
    var sortedList = objList.sort(function(a, b){return a.id - b.id});
    console.log(sortedList);
}

function ReverseString(string)
{
    console.log("base string: " + string);
    var tempArray = string.split('');
    tempArray.reverse();
    string = tempArray.join('');
    console.log("reversed string: " + string);
}

function PrintToday()
{
    var data = new Date();
    var giorni = ["Lunedì", "Martedì", "Mercoledì", "Giovedì", "Venerdì", "Sabato", "Domenica"];
    var mesi = ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"];
    console.log(`${giorni[data.getDay() - 1]} ${data.getDate()}, ${mesi[data.getMonth()]} ${data.getFullYear()}`);
}

function DateCalculator(date2)
{
    var first_date = Date.UTC(2021, 2, 23);
    var second_date = Date.UTC(date2);

    var differenza;
    first_date>second_date ? differenza=first_date-second_date : differenza=second_date-first_date;
    
    var millToDay = 1000 * 60 * 60 * 24;
    differenza = differenza / millToDay;

    console.log(differenza);
}

function GreatestYear(dateList)
{
    var millDateList = [];
    for (var i = 0; i < dateList.length; i++)
    {
        millDateList.push(Date.UTC(dateList[i].year, dateList[i].month, dateList[i].day));
    }
    millDateList.sort(function(a, b){return a - b});
    //console.log(millDateList);
    
    var greatDate = millDateList[millDateList.length - 1];
    var fullDate = new Date(greatDate);
    fullDate = fullDate.toDateString();
    console.log(fullDate);
}


SortArrays(array1, array2);
SortWeek(giorniSettimana);
SortObjList(objectsList);
ReverseString(stringa)
PrintToday();
DateCalculator(2031, 5, 2);
GreatestYear(listaDate);

