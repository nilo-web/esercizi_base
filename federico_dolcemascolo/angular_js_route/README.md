# AngularJS Base

Un'applicazione base di angularjs a scopo didattico

## Node.js

Per inizializzare questo progetto useremo node.js come package manager (npm).
Npm tiene traccia dei moduli installati in un progetto con il file `package.json`, che risiede nella directory di un progetto e contiene:

- Tutti i moduli necessari per un progetto e le relative versioni installate
- Tutti i metadati per un progetto, come l'autore, la licenza, ecc
- Script che possono essere eseguiti per automatizzare le attività all'interno del progetto

Prima di tutto installiamo node.js e le dipendenze necessarie al progetto

Puoi scaricare ed installare Node.js per il tuo sistema operativo tramite questo link installazione [https://nodejs.org/en/download/][node_download].
Per vedere che l'installazione sia andata a buon fine, da terminale, possiamo usare il comando 

```
node --version
```

Installando Node.js, avremmo in automatico anche [npm][npm] che ci permetterà di scaricare ed installare le nostre dipendenze attraverso il comando. 

```
npm install
```

Questo, leggendo le istruzioni presenti sul `package.json` scaricherà tutti i file necessari per il nostro progetto nella cartella `node_modules` (nb: va sempre esclusa da git).
Oltre a scaricare le dipendenze e file necessari perchè funzionino, node le copierà (perchè glielo abbiamo specificato con uno script nel package.json) nella cartella `app/lib/` del nostro progetto.

## Avviare l'applicazione

Anche se possiamo avviare l'applicazione direttamente nel browser visto che si tratta di un progetto client side, è sempre meglio farlo girare in un server 
http in quanto la maggior parte dei browser e librerie blocca eventuali chiamate provenienti dal file system.

Per avviare l'applicazione digitiamo nel terminale, dalla root del nostro progetto dove risiede il file `package.json` il comando 

```
npm start
```

questo, esseguirà lo script definito nel package.json e creerà un web server locale. 
Pertanto lo potremmo raggiungere dal nostro browser attraverso la url che verrà specificata in console. 
Questo server sarà creato dal tool browser-sync che fa in modo che ogni qual volta si salvi un file della 
nostra applicazione, questa verrà refreshata automaticamente senza il bisogno di farlo a mano.


[node_download]: https://nodejs.org/en/download/
[npm]: https://www.npmjs.com/
