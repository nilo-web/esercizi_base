"use strict";

var app = angular.module("myApp", ["ngRoute"]);

app.config(function ($routeProvider) {
  $routeProvider
    .when("/", {
      templateUrl: "/templates/registration.html",
      controller: "registrationController",
    })
    .when("/userpage/:username", {
      templateUrl: "/templates/userpage.html",
      controller: "userpageController",
    })
    .otherwise({
      redirectTo: "/",
    });
});

app.controller("registrationController", function ($scope, $location) {
  console.log($scope);

  $scope.sendData = function (user) {
    console.log("submit", user);

    $location.path("/userpage/" + user.username);
  };
});

app.directive('menuItem', function($locatio){
  return{
    scope: {
      endPoint: '@',
      label: '@',
    },
    template: '<span>{{label}}</span>',
    link: function($scope, element, attrs){
      element.bind('click', function(){
        $location.path($scope.endPoint);
      });
    }
  }
});

app.controller("userpageController", function ($scope, $routeParams) {
  $scope.username = $routeParams.username;
});

app.run([
  "$rootScope",
  "$location",
  "authService",
  function ($rootScope, $location, authService) {
    $rootScope.$on("$routeChangeStart", function (event) {
      if (!authService.isLoggedIn()) {
        console.log("DENY", $location, event);
        // event.preventDefault();
        $location.path("/");
      } else {
        console.log("ALLOW");
        // $location.path("/userpage");
      }
    });
  },
]);

app.factory("authService", function () {
  var user = true;

  return {
    setUser: function (aUser) {
      user = aUser;
    },
    isLoggedIn: function () {
      return user ? user : false;
    },
  };
});
