"use strict";

function getPostsAJAX() {
  // genero la chiamata http passandogli un oggetto di configurazione
  // con l'url e il verbo http
  var chiamataPerPosts = $.ajax({
    type: "GET",
    url: "https://jsonplaceholder.typicode.com/posts",
  });

  return chiamataPerPosts;
}