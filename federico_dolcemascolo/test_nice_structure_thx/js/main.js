"use strict";


// MALE, DA NON FARE MA ADESSO LO FACCIO PER QUESTIONI DI VELOCITÀ
// wrappo document, come variabile globale (MALE MALE), con jquery
// lo userò come dispatcher generico di eventi
var $d = $(document);

// IFEE: la mia applicaizone la inizializzo in modo da farla lavorare in uno scope protetto
(function () {
  var cards = [];

  // seleziono il contenitore degli utenti
  var $contenitoreListaUtenti = $("#listaUtenti");

  var utenti = getUtentiAJAX();
  var posts = getPostsAJAX();
  utenti.then(
    function (responseListaUtenti) {
      console.log("Chiamata risolta", responseListaUtenti);

      // ciclo sulla lista e per ogni utente creo ed inserisco nel dom una "card"
      for (var i = 0; i < responseListaUtenti.length; i++) {
        var utenteOggettoDellaResponse = responseListaUtenti[i];

        var istanzaCardUtente = new UserCard(utenteOggettoDellaResponse);
        istanzaCardUtente.renderCard($contenitoreListaUtenti);

        cards.push(istanzaCardUtente);

      }
    },
    function (error) {
      alert("Ops la chiamata non ha funzionato");
    }
  );

  $d.on("userSelected", function(evento, userId) {
    console.log("una card è stata cliccata", userId);

    var cardSelezionata;

    for (var i = 0; i < cards.length; i ++) {
        if (cards[i].id === userId) {
            cardSelezionata = cards[i];
        }
    }

    console.log("card selezionata", cardSelezionata)

    DettagliUtente.getInstance().render(cardSelezionata);
  });

})();
