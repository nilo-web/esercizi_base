# Esercizio Javascript e Jquery

*   Concatena, senza duplicati, i seguenti array e ordinali per valore crescente
```javascript
var array1 = [20, 33, 12, 1233];
var array2 = [1, 33, 102, -12];
```
*   Fai il reverse di questa string: “supercalifragilistichespiralidoso”
*   Ordina il seguente array con i giorni della settimana in base al giorno della corrente:
```javascript
var giorniSettimana = ["lunedi", "martedi", "mercoledi", "giovedi", "venerdi", "sabato", "domenica"];
```
*   Ordina il seguente array di oggetti in base all’id:
```javascript
var objectsList = [
 { name: "Pino", id: 66 },
 { name: "Rino", id: 16 },
 { name: "Dino", id: 6 },
 { name: "Lino", id: 96 },
 { name: "Gino", id: 26 }
];

```
*   Crea una funzione che stampi la data corrente in questo formato, es: "martedi 22, febbraio 2021"
*   Crea una funzione che ritorni la differenza tra 2 date in giorni (usare Date.UTC())
*   crea una funzione che data un array di date ritorni la data più alta

*   Scrivi una funzione che ritorna una funziona che ritorna il numero che è stato passato alla prima funzione.
es test(7)() //  7.

*   Scrivi una funzione che ritorni una funzione che ritoni il numero incrementato che è stato passato alla prima funzione. Ogni volta che la si chiama il numero viene incrementato di uno

*   Scrivi una funzione che ritorna un numero e lo incrementi ogni volta che viene chiamata (IFFE)
*   Scrivi una funzione che ogni volta che viene chiamata ritorni il numero successivo della serie di fibonacci

*   Inizialmente la pagina è vuota, ma ogni volta che clicco sul pulsante compare una nuova riga di testo.
*   Inizialmente la pagina è vuota, ma ogni volta che clicco sul pulsante compare una nuova riga di testo, ma le righe saranno una volta rosse e una volta blu.
*   Mettere tutto quello fatto nell'esercizio precedente dentro una funzione. Quindi invocare la funzione due volte: il risultato deve essere lo stesso dell'esercizio 2.
*   Fare in modo che la funzione creata nel punto precedente prenda in ingresso due parametri: devono essere entrambi delle stringhe che rappresenteranno i due colori. Quindi invocare la funzione una unica volta passandogli i colori "green" e "yellow".

*   Modificare l'esercizio precedente in modo inserire un altro pulsante: se lo schiaccio devo cancellare tutte le righe inserite.

*    creare una funzione che prende in ingresso un container e lo pulisce da tutti i suoi figli. Invocarla nell'esercizio precedente quando ci serve pulire il div che contiene le scritte.

*   Aggiungere un terzo pulsante. Questo pulsante, quando calcato, cambierà il colore di sfondo del div che contiene il testo.

*   Fare in modo che il terzo pulsante appena aggiunto, se ricalcato una seconda volta, ripristini il colore di sfondo
del testo a bianco.

*   Inserire un quarto pulsante "Autodistruzione". Questo pulsante elimina tutto il contenuto della pagina, compreso se stesso.


*   All'avvio della pagina controllare nel localStorage che sia presente il valore con chiave "mieiArticoli". 
Se non è presente recuperarli da qui https://jsonplaceholder.typicode.com/users ed inserirli nel localStorage. 
Dopo aver preso i dati (dal localStorage o dalla chiamata remota) stamparli a schermo.





# Esercizio Javascript e Jquery

Attraverso javascript (è possibile usare, se e quando voluto, anche qualsiasi metodo [jQuery](https://api.jquery.com/)) mostrare una lista di utenti.  
Ogni utente deve essere visualizzato in una "card" che ne mostri nome, username ed email.  
Ogni card sarà collegata ad un evento click il quale mostrerà nel lato destro della pagina una sezione nella quale verranno caricati e visualizzati i post degli utenti. 
I dati verrano reperiti tramite questo portale [https://jsonplaceholder.typicode.com/](https://jsonplaceholder.typicode.com/)



Nello specifico:

*   con un click si apre il dettaglio utente e la card dovrà avere un altro colore in quanto è stata selezionata
*   se riclicco sulla card il colore deve tornare normale e il dettaglio utente deve sparire
*   se ho un utente selezionato e clicco su un altro, il dettaglio utente si aggiorna e di conseguenza anche le card 
utente per quanto riguarda i colori di selezione
*   il dettaglio utente deve avere un botton X per nascondere se stesso e deselezionare la card dell'utente



**Bonus tasks:**  

*   Usare [Bootstrap](https://getbootstrap.com/docs/3.4/) per dare uno style al layout
*   Ogni post sarà cliccabile. In quel caso verrà aperta una modale di bootstrap nel quale si vedrà il dettaglio del post e i commenti associati


# Esercizi AngularJs
*   Creare una TODO list: creare un box con 2 input e un bottone: una input "titolo" e una "contenuto" e un bottone "salva"
*   Al click del bottone aggiungo il mio elemento TODO ad una lista di cose da fare (todo list) e la visualizzerò al di sotto.
*   Creare un'altra box (di fianco alla prima) in un solo input che servirà da filtro per i miei elementi. Quindi scrivendo in questa input filtro a runtime la lista per il titolo dei vari elementi. 



**Form angularjs**

*   Creare un form di registrazione che abbia come input: nome, cognome, username, password, accettazione_privacy, sesso.
Il form deve avere un tasto di submit che mandi i dati dell'utente ad un controller. 
*   I campi sono tutti reguired e lo user name deve essere lungo almeno 3 lettere e con lunghezza massima di 6. 
*   Ogni campo deve avere un testo di errore aassociato da mostrare in caso di errore solo dopo che il singolo input è stato modificato.
*   il tasto di submit sarà visibile (o solo cliccabile) solo se il form è valido



# SUGGERIMENTI PROVA FINALE

*   una route per ogni pagina con una otherwise su /login
*   implementare un controllo sulle route: 
se sono loggato posso proseguire, altrimenti effettuo un redirect alla pagina di login (hint: per fare il controllo gestire il tutto con un servizio o rootScope e stare in ascolo dell'evento che precede il cambio pagina  '$rootScope.$on("$routeChangeStart", function(){}')
*   alla login chiamata all'api
*   in caso di success salvo i dato in un servizio
*   usare il servizio $location per arrivare alla route /home