'use strict'
//Scrivi una funzione che ritorna un numero e lo incrementi ogni volta che viene chiamata (IFFE)
var fun = (function (){
    var  x1 =8 ;

    return function(){
        ++x1;
        return x1;
    }

})();

console.log(fun());
console.log(fun());
console.log(fun());