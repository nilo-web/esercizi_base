"use strict";
//Inizialmente la pagina è vuota, ma ogni volta che clicco sul pulsante compare una nuova riga di testo.

var titolo = document.getElementById("titolo");

titolo.style.color = "red";

var container = document.getElementById("container");
var bottone = document.getElementById("btn");
var counter = 0;

var contenuto = "sono il nuovo contenuto numero: ";

bottone.addEventListener("click", function() {
    counter++;

    var nuovoParagrafo = document.createElement("p");
    nuovoParagrafo.innerText = contenuto + counter;
    container.appendChild(nuovoParagrafo);
    
});