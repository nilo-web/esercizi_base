"use strict";
//  Ordina il seguente array di oggetti in base all’id:
var objectsList = [
    { name: "Pino", id: 66 },
    { name: "Rino", id: 16 },
    { name: "Dino", id: 6 },
    { name: "Lino", id: 96 },
    { name: "Gino", id: 26 }
   ];
   var risultato = objectsList.sort( function (a, b){
       return a.id - b.id;
   });
   
   console.log(risultato);