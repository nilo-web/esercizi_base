"use strict"
//Crea una funzione che stampi la data corrente in questo formato, es: "martedi 22, febbraio 2021"
function stampa(){
    var giorni = ["lunedi", "martedi", "mercoledi", "giovedi", "venerdi", "sabato", "domenica"];
    var mesi = ["gennaio", "febraio", "marzo", "aprile", "maggio", "sgiugno", "luglio","agosto","settembre","ottobre","novembre","dicembre"];

    var oggi = new Date();
    var giornoCorrente = giorni[oggi.getDay() - 1];
    var giornoCorrenteNumero = oggi.getDate();
    var meseCorrente = mesi[oggi.getMonth()];
    var annoCorrente = oggi.getUTCFullYear();

    return `${giornoCorrente} ${giornoCorrenteNumero} ${meseCorrente} ${annoCorrente}`;
}
console.log(stampa());