'use strict';

function getUtentiAJAX(){
    var cards = [];
    var chiamatePerUtente = $.ajax({
        type: "GET",
        url: "https://jsonplaceholder.typicode.com/users"
    });

    return chiamatePerUtente;
}

function getPostAJAX(id) {
    // genero la chiamata http passandogli un oggetto di configurazione
    // con l'url e il verbo http
    var chiamataPerPost = $.ajax({
      type: "GET",
      url: "https://jsonplaceholder.typicode.com/posts?userId="+id,
    });
  
    return chiamataPerPost;
  }