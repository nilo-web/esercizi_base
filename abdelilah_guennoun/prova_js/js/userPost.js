"use strict";

function userPost(postObj) {
    this.userId = postObj.userId;
    this.id = postObj.id;
    this.title = postObj.title;
    this.$tpl = $('<li class="post-utente"></li>');
}

    userPost.prototype.renderPost = function($container) {

    var contenutoTitlePost = this.title;

    this.$tpl.html(contenutoTitlePost);

    this.$tpl.attr("data-id-utente", this.id);

    
    $container.append(this.$tpl);
};
