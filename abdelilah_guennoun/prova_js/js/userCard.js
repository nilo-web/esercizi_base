'use strict';

function UserCard(userObj){
    this.id = userObj.id;
    this.name = userObj.name;
    this.username = userObj.username;
    this.email = userObj.email;

    this.$tpl= $("<div class='card-utente'></div>");

}

UserCard.prototype.renderCard = function($container){

    var contenutoCard = "<h4>"+this.name+"</h4>" + "<p>User Name: " + this.username + "</p>" + "<p>Email: " + this.email+"</p>";

    this.$tpl.html(contenutoCard);

    this.$tpl.attr("data-id-utente",this.id);

    this.$tpl.on("click", this.clickHandler.bind(this));
    $container.append(this.$tpl);

};

UserCard.prototype.clickHandler =function(evento){
    //this.$tpl.toggleClass("active");
    $d.trigger("userSelected", this.id);
    

};