

var $d=$(document);
(function(){
    var cards = [];
    var $contenitoreListaUtenti=$("#listautente");

    //var $contenitoreDettagliUtente =$("#dettaglioUte");

    var utenti = getUtentiAJAX();
    utenti.then(function(responseListaUtenti){

        for (var i =0; i <responseListaUtenti.length; i++){
            var utenteOggettoDellaResponse = responseListaUtenti[i];
            var istanziaCardUtenti= new UserCard(utenteOggettoDellaResponse);
            istanziaCardUtenti.renderCard($contenitoreListaUtenti);
            cards.push(istanziaCardUtenti);
            

    }},
    function(errore){
        alert("ops");
    }
    );
    $d.on("userSelected", function(evento, userId){
        console.log('una card è stata clicata', userId);

        var cardSelezionata;

        for (var i =0 ; i < cards.length; i++) {
            if (cards[i].id === userId){
                cardSelezionata = cards[i];
            }
        }
        DettaglioUtente.getInstance().render(cardSelezionata);
    })
})();
