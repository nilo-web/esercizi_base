'use strict';


var DettaglioUtente = (function(){

    var instance;

    var $contenitoreDettagliUtente =$("#dettaglioUte");

    var $contenitoreNomeUtente =$("#nome-utente");

    var $listaPostUtente =$contenitoreDettagliUtente.find("#listaPostUtente");

    return {
        getInstance : function(){
            if (!instance){
                instance ={
                    render : function(useCardSelected){
                        $contenitoreNomeUtente.html(useCardSelected.name);


                    var post = getPostAJAX(useCardSelected.id);
                    post.then(
                    function (responseListaPosts) {
                    
                    $listaPostUtente.html("");
                    
                    for (var i = 0; i < responseListaPosts.length; i++) {
                        var postResponse = responseListaPosts[i];

                        var istanzaPostUtente = new userPost(postResponse);
                        istanzaPostUtente.renderPost($listaPostUtente);

                    }
                    },
                    function (error) {
                    alert("Ops la chiamata -Posts- non ha funzionato");
                     }
                    );
                    },

                };
            }
            return instance;
        },
    };
})();