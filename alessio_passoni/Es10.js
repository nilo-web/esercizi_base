//  Scrivi una funzione che ritorna un numero e lo incrementi ogni volta che viene chiamata (IFFE)
"use strict";

function incr(n){ //IIFE
   var x=n;
  return function(){
      return x++;
  }
}
(function(){
   var a=incr(3),n; //assegno ad a la funzione anonima
   for(let i=0;i<5;i++){
      n=a(); //eseguo a ad ogni ciclo
      console.log(n);
   }

})();