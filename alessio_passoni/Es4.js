/*   Ordina il seguente array di oggetti in base all’id:
```javascript
var objectsList = [
 { name: "Pino", id: 66 },
 { name: "Rino", id: 16 },
 { name: "Dino", id: 6 },
 { name: "Lino", id: 96 },
 { name: "Gino", id: 26 }
];*/
"use strict";
(function () {
    var objectsList = [
        { name: "Pino", id: 66 },
        { name: "Rino", id: 16 },
        { name: "Dino", id: 6 },
        { name: "Lino", id: 96 },
        { name: "Gino", id: 26 }
    ];
    objectsList.sort(function s(a, b) {
        return a.id - b.id //ordino l'oggetto in base all'id
    })
    console.log(objectsList);
})();
