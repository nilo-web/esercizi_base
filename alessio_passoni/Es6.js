//Crea una funzione che ritorni la differenza tra 2 date in giorni (usare Date.UTC())
"use strict";
(function () {
    var dateOne =  Date.UTC(2013, 8, 29);
    var dateTwo =  Date.UTC(2016, 8, 29);
    var diff;
    dateOne > dateTwo ? diff = dateOne - dateTwo : diff = dateTwo - dateOne; //eseguo differenza in 
    //mill tra le date
    var dayMill=1000*60*60*24; //mill in un giorno
    console.log("Sono passati ",Math.floor(diff/dayMill)," giorni"); //stampo i giorni effettivamente passati
})();