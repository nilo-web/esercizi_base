"use strict";
//CON JS PURO

var httpReq = new XMLHttpRequest();
httpReq.onreadystatechange = function () {
  console.log("onreadystatechange", httpReq);


if (httpReq.readyState === 4 && httpReq.status === 200) {
  console.log("Response", httpReq.responseText);
  for(let i=0;i<100;i++){
    var p = document.createElement("p");
    var x = JSON.parse(httpReq.responseText);
    p.innerText=x[i].title; //il title è una  key del JSON
    var div = document.getElementById("textContainer");
    div.append(p);
  }

}

};
httpReq.open("GET", "https://jsonplaceholder.typicode.com/posts");
httpReq.send();


//CON AJAX E JQUERY
/*
var $div = $("#textContainer");


var chiamata = $.ajax({
  type: "GET",
  url: "https://jsonplaceholder.typicode.com/posts"
}).done(function (response) {
  console.log("response", response);
  let i;
  for (i = 0; i < 100; i++) {
    var $elem = $('<p>' + response[i].title + '</p>');
    $div.append($elem);
  }

});*/