/* Ordina il seguente array con i giorni della settimana in base al giorno della corrente:
```javascript
var giorniSettimana = ["domenica","lunedi", "martedi", "mercoledi", "giovedi", "venerdi", "sabato"];
*/

"use strict";
function orderArray(arrDay) {
    var dateToday = new Date(); //creo variabile data oggi
    dateToday = dateToday.getDay(); //prendo il giorno di oggi
    var resto=arrDay.splice(0,dateToday); //tolgo i giorni
    arrDay=arrDay.concat(resto); //li riaggiungo
    console.log(arrDay); //stampo
}


orderArray(["domenica", "lunedi", "martedi", "mercoledi", "giovedi", "venerdi", "sabato"]);