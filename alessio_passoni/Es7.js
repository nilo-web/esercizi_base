//crea una funzione che data un array di date ritorni la data più alta
"use strict";
(function () {
    var arrDate = [new Date('2013-12-8'), new Date('2014-12-8'), new Date('2013-6-8'), new Date('2013-12-23'), new Date('2013-2-8')];
    var max = 0;
    for (let i = 1; i < arrDate.length; i++)
        if (arrDate[i] > arrDate[max])
            max = i;
    console.log(arrDate[max]);
})();