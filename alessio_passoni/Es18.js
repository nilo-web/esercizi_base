/*Aggiungere un terzo pulsante. Questo pulsante, quando calcato, 
cambierà il colore di sfondo del div che contiene il testo.*/
"use strict";

function clearDiv(nomeDiv) {
  var divC = document.getElementById(nomeDiv);
  console.log(divC);
  var allP = divC.getElementsByTagName("p");//elementi p nel div
  var i;
  for (i = allP.length - 1; i >= 0; i--) //ciclo nella lista
    allP[i].remove(); //rimuovo l'iesimo dalla lista genitrice

}



function colora(cont) { //colora il div

  var div = document.getElementById("container");
  div.style.backgroundColor = "black";
}


function f(color1, color2) {

  console.log(color1, color2);

  var myContainer = document.getElementById("container");

  var myButton1 = document.createElement("button");
  myButton1.innerText = "Inserisci";

  var myButton2 = document.createElement("button");
  myButton2.innerText = "Elimina";


  var myButton3 = document.createElement("button");
  myButton3.innerText = "Cambia colore";

  myContainer.appendChild(myButton1);
  myContainer.appendChild(myButton2);
  myContainer.appendChild(myButton3);

  var lastColor = color1;

  myButton1.addEventListener("click", function () {
    console.log("click 1");
    var myP = document.createElement("p");
    myP.innerText = "Ciao";
    myP.style.color = lastColor;
    myContainer.appendChild(myP);

    if (lastColor === color1) {
      lastColor = color2;
    } else {
      lastColor = color1;
    }
  });

  myButton2.addEventListener("click", function () {
    console.log("click 2");
    clearDiv("container"); //chiamo la pulizia

  });

  myButton3.addEventListener("click", function () {
    console.log("click 3");
    colora(); //funzione per colorare in base al contatore
  });
}


f("green", "yellow");

/*ALTERNATIVO
/** Aggiungere un terzo pulsante. * Questo pulsante, quando calcato, 
 * cambierà il colore di sfondo del div che contiene il testo.
"use strict";
var a = incr(0);

function incr(n) { //IIFE di incremento
    var x = n;
    return function () {
        return x++;
    }
}

function creaPar(colore1, colore2) {
    var i = 0;
    while (i < 2) {
        var cont = a();
        var div = document.getElementById("container");
        var color;
        var s = "Sono la riga numero " + cont;
        var p = document.createElement("p");
        p.innerText = s;
        cont % 2 === 0 ? color = colore1 : color = colore2;
        p.style.color = color;
        div.appendChild(p);
        i++;
    }

}

function clearDiv(nomeDiv) {
    var div = document.getElementById(nomeDiv);
    var righe = div.getElementsByTagName("p");
    for (let i = righe.length - 1; i >= 0; i--)
        div.removeChild(righe[i]);
}

function changeDivColor(div){
    div.style.backgroundColor="green";
}

(function () {
    var divContainer = document.getElementById("container");
    var b = document.createElement("button"); //creo un bottone
    var bElim = document.createElement("button");
    var bChangeC = document.createElement("button");
    b.innerText = "Aggiungi riga";
    bElim.innerText = "Elimina righe";
    bChangeC.innerText = "Cambia colore div";
    divContainer.appendChild(b); //appendo il bottone
    divContainer.appendChild(bElim); //appendo il bottone
    divContainer.appendChild(bChangeC);
    b.addEventListener("click", function () { //creo il listener per il click
        creaPar("green", "yellow");

    });

    bElim.addEventListener("click", function () {
        clearDiv("container");
    });

    bChangeC.addEventListener("click", function () {
        changeDivColor(divContainer);
    });
})();*/