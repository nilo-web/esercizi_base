"use strict";
function elimina(elemento) {
  $(elemento + " p").remove();
}



function incr() {
  var n = 0;
  return function () {
    ++n; //incremento
    return n; //ritorno n
  }
}

function doomsday() {
  var elem = document.childNodes; //prendo tutti i nodi del documento
  var i;
  for (i = elem.length - 1; i >= 0; i--)
    elem[i].remove(); //rimozione
}


function myFunction(firstColor, secondColor) {
  // container dei bottoni
  var $myContainer = $("#container");
  var cont = incr();
  // container dei p
  var $myTextContainer = $("#textContainer");
  var divContainer = "#textContainer";

  // genero il primo bottone
  var $myButton = $("<button>Click here</button>");
  $myContainer.append($myButton);

  // genero il secondo bottone
  var $clearBtn = $("<button>Reset</button>");
  $myContainer.append($clearBtn);

  var $colorBtn = $("<button>Change color</button>");
  $myContainer.append($colorBtn);

  var $doomBtn = $("<button>Doomsday!</button>");
  $myContainer.append($doomBtn);
  // gestione colori
  let myLastColor = firstColor;

  var c = 0;

  function changecolor() {
    var color, a = cont();
    a % 2 == 0 ? color = "yellowgreen" : color = "purple";
    $myTextContainer.css("background-color", color);

    console.log(a);
  }

  // eventListener per aggiungere un elemento p
  $myButton.on("click", function () {

    c++;

    // creo l'elemento
    var $myParagraph = $("<p> Hello world! " + c + "</p>");

    $myParagraph.attr("id", c);
    $myParagraph.css({ color: myLastColor });
    $myTextContainer.append($myParagraph);

    // gestione colori
    if (myLastColor === firstColor) {
      myLastColor = secondColor;
    }
    else {
      myLastColor = firstColor;
    }
  });

  $myTextContainer.on("click", function (event) {
    //console.log("elemento cliccato " +  event.target.getAttribute("id"));
    alert("elemento cliccato " + event.target.id);
  });

  // eventListener per cancellazione nodi
  $clearBtn.on("click", function () {


    elimina(divContainer);

  });

  $colorBtn.on("click", function () {
    changecolor();
  });

  $doomBtn.on("click", function () {
    $("*").remove();
  });

}

myFunction("black", "red");






