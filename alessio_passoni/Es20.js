/*Inserire un quarto pulsante "Autodistruzione". Questo pulsante elimina tutto il contenuto della pagina, 
compreso se stesso.*/
"use strict";

function clearDiv(nomeDiv) {
  var divC = document.getElementById(nomeDiv);
  console.log(divC);
  var allP = divC.getElementsByTagName("p");//elementi p nel div
  var i;
  for (i = allP.length - 1; i >= 0; i--) //ciclo nella lista
    allP[i].remove(); //rimuovo l'iesimo dalla lista genitrice

}

function incr() {
  var n = 0;
  return function () {
    ++n; //incremento
    return n; //ritorno n
  }
}

function colora(cont) { //colora il div
  var color;
  var a = cont(); //incrementa a
  var div = document.getElementById("container");
  a % 2 === 0 ? color = "black" : color = "purple"; //cambio colore
  div.style.backgroundColor = color;
}

function doomsday(){
  var elem = document.childNodes; //prendo tutti i nodi del documento
  var i;
  for (i = elem.length - 1; i >= 0; i--)
    elem[i].remove(); //rimozione
}


function f(color1, color2) {

  console.log(color1, color2);

  var cont = incr();
  var myContainer = document.getElementById("container");

  var myButton1 = document.createElement("button");
  myButton1.innerText = "Inserisci";

  var myButton2 = document.createElement("button");
  myButton2.innerText = "Elimina";


  var myButton3 = document.createElement("button");
  myButton3.innerText = "Cambia colore";

  var myButton4 = document.createElement("button");
  myButton4.innerText = "Autodistruzione";

  myContainer.appendChild(myButton1);
  myContainer.appendChild(myButton2);
  myContainer.appendChild(myButton3);
  myContainer.appendChild(myButton4);

  var lastColor = color1;

  myButton1.addEventListener("click", function () {
    console.log("click 1");
    var myP = document.createElement("p");
    myP.innerText = "Ciao";
    myP.style.color = lastColor;
    myContainer.appendChild(myP);

    if (lastColor === color1) {
      lastColor = color2;
    } else {
      lastColor = color1;
    }
  });

  myButton2.addEventListener("click", function () {
    console.log("click 2");
    clearDiv("container"); //chiamo la pulizia

  });

  myButton3.addEventListener("click", function () {
    console.log("click 3");
    colora(cont); //funzione per colorare in base al contatore
  });

  myButton4.addEventListener("click", function () {
    console.log("click 4");
    doomsday(); //funzione autodistruzione
  });
}


f("green", "yellow");