/**   Mettere tutto quello fatto nell'esercizio precedente dentro una 
 funzione. Quindi invocare la funzione due volte: il risultato deve essere lo stesso dell'esercizio 2.*/
 "use strict";

 function incr(n){ //IIFE di incremento
     var x=n; 
     return function(){
         return x++;
     }
 }
 
 function creaPar(f,cont,div){
     var color;
     cont=f(); //incremento il contatore
     cont%2===0 ? color="green":color="blue"; //scelgo il colore
     var s="Sono la riga numero "+cont;
     var p=document.createElement("p"); 
     p.innerText=s;
     p.style.color=color;
     div.appendChild(p); //aggiungo riga
 }
 
 (function () {
     var divContainer=document.getElementById("container"); 
     var b=document.createElement("button"); //creo un bottone
     var n=incr(0),a;
     b.innerText="Aggiungi riga";
     divContainer.appendChild(b); //appendo il bottone
     b.addEventListener("click",function(){ //creo il listener per il click
         creaPar(n,a,divContainer)
     });
 })();