"use strict";

function eliminaVecchiaCard() { //elimino il dettaglio della carta selezionata
    var elem = divDettaglio.childNodes; //prendo tutti i nodi del div
    var i;
    if (elem.length > 0)
        for (i = elem.length - 1; i >= 0; i--)
            elem[i].remove(); //rimozione
};

function creaListaPost(post) { //attacco i post nel dettaglio card
    var divDettaglio = document.getElementById("divDettaglio");
    var uL = document.createElement("ul"); //creo lista post
    divDettaglio.append(uL); //e la appendo al div dettaglio

    for (let i = 0; i < post.length; i++) {
        var child = document.createElement("li"); //creo elemento lista
        child.innerText = post[i].name; //inserisco il post
        uL.appendChild(child);//appendo alla lista
    }
};

function ripristina() { //ripristino colore card quando clicco una seconda card
    var divList = document.querySelectorAll("div.divCard");
    for (let i = 0; i < divList.length; i++)
        divList[i].style.backgroundColor = "limegreen";

};

function vediDettaglio(utenti, utentePremuto) { //funzione per vedere il dettaglio card
    eliminaVecchiaCard(); //elimino quella presente
    //recupero nome utente poi farò una get con id per trovare i post
    var divDettaglio = document.getElementById("divDettaglio");
    var btnClose = document.createElement("button"); //bottone X per chiudere la carta
    var nome = utenti[utentePremuto.target.getAttribute("idutente")].name; //a seconda del target cliccato...
    //...inserisco il nome per il dettaglio
    var pNome = document.createElement("h2");
    //rendo visibile il div dettaglio
    divDettaglio.style.visibility = "visible";
    //setto e appendo i vari figli
    btnClose.innerText = "X";
    divDettaglio.append(btnClose);
    //inserisco una break line tra nome e post
    pNome.innerHTML = nome + "<br>";
    divDettaglio.append(pNome);
    //chiamo il servizio per recuperare i post in base all'user cliccato
    var servizioPost = chiamataPost(utentePremuto.target.getAttribute("idutente")); //chiamata per recuperare i dati
    servizioPost.then(
        //successo
        function (response) {

            //dati presi, creare lista post
            creaListaPost(response);
            //evento click pulsante chiusura
            btnClose.addEventListener("click", function (e) {
                //nascondo il div
                ripristina(); //ripristino i colori di background card
                divDettaglio.style.visibility = "hidden";
                var elem = divDettaglio.childNodes; //prendo tutti i nodi del div
                var i;
                for (i = elem.length - 1; i >= 0; i--)
                    elem[i].remove(); //rimozione
            });
        },

        // errore
        function (error) {
            console.log("Errore nel recupero post");
        }
    );


};

function creaLista(risposta) {
    //creo la lista di utenti coi div
    var lista = document.getElementById("divLista");
    var child;
    for (let i = 0; i < risposta.length; i++) {
        //creo card settando la classe e attributi
        child = document.createElement("div");
        child.setAttribute("class", "divCard");
        child.setAttribute("style", "background-color: limegreen;");
        child.setAttribute("idutente", risposta[i].id - 1); //attributo che mi serve per recuperare posts
        //inserisco testo card
        child.innerText = risposta[i].name + "\n" + risposta[i].username + "\n" + risposta[i].email;
        lista.append(child);
    }

    lista.addEventListener("click", function (e) {
        //uso un event delegation sulla lista
        if (e.target != lista) {
            //controllo se la card è selezionata o meno
            if(e.target.style.backgroundColor!=='pink'){ //pink è il colore per la card selezionata
                ripristina(); //ripristino il colore di background
                e.target.style.backgroundColor = "pink"; //cambio colore
                vediDettaglio(risposta, e); //apro dettaglio
            }
            else
            { //la carta è già selezionata
                eliminaVecchiaCard(); //chiudo la scheda dettaglio
                ripristina(); //ripristino il colore di background
            }
        }

    });
};