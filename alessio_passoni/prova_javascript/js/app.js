"use strict";
(function(){
    //main del sito
    var servizioPersone = chiamataPersone(); //chiamata per recuperare i dati
    servizioPersone.then(
      //successo
      function (response) {
        //dati presi, creare lista
        creaLista(response);
      },
    
      // errore
      function (error) {
        console.log("Errore nel recupero dati");
      }
    );
    
})();