
var xhr = new window.XMLHttpRequest(); //variabile globale per tenere gli eventi
/*function checkPending() {
  console.log($.active);
  return $.active;
}*/
// chiamata asincrona per recuperare i dati delle persone
function chiamataPersone() {
  //uso una promessa
  var deferred = $.Deferred();

  if (localStorage.getItem("datiPersone")) {
    deferred.resolve(JSON.parse(localStorage.getItem("datiPersone"))); //se presenti prendo da localstorage
  } else {
    //kill ajax se ne avevo pregressa
      xhr.abort();
    //faccio la chiamata
    $.ajax({
      type: "GET",
      url: "https://jsonplaceholder.typicode.com/users",
      xhr: function () {
        return xhr; //ritorno per tenere se nel caso devo eliminare la richiesta
      }
    }).then(
      function (response) {
        //successo, salvo in localStorage
        localStorage.setItem("datiPersone", JSON.stringify(response));

        //sciolgo la promessa
        deferred.resolve(response);

      },
      function (error) {
        console.log("Aborted");
      }
    );
  }
  return deferred;
}

