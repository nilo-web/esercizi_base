var xhr = new window.XMLHttpRequest(); //tengo traccia delle richieste
function chiamataPost(id) {

    //sistemo l'indice
    id = parseInt(id) + 1;
    //uso una promessa
    var deferred = $.Deferred();
    //kill ajax se pendente
      xhr.abort();
      //eseguo chiamata
    ajaxPost = $.ajax({
      type: "GET",
      url: "https://jsonplaceholder.typicode.com/comments?postId=" + id,
      xhr: function () {
        return xhr;
      }
    }).then(
      function (response) {
        //sciolgo la promessa
        deferred.resolve(response);
  
      },
      function (error) {
        console.log("Aborted");
      }
    );
    return deferred;
  
  }