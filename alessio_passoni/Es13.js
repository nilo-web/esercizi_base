/*Inizialmente la pagina è vuota, ma ogni volta che clicco sul pulsante compare una
 nuova riga di testo, ma le righe saranno una volta rosse e una volta blu.*/
 "use strict";

 var bottone=document.getElementById("btn"); //id bottone
 var titolo=document.getElementById("titolo");//id titolo
 var i=0;
 var color; //variabile per il colore
     
 var s = "nuova riga";//creo il testo
 bottone.addEventListener("click",function(){
     i%2===0 ? color="red" : color="yellow"; //righe pari rosse e dispari gialle
     var p = document.createElement("p"); //creo un p
     p.innerText=s;//appendo il testo al paragrafo
     p.style.color=color;//assegno colore al paragrafo
     titolo.appendChild(p);//attacco il paragrafo al titolo
     i++;//incremento
 });

 /* ALTRO METODO
 /*Inizialmente la pagina è vuota, ma ogni volta che clicco sul pulsante 
compare una nuova riga di testo, ma le righe saranno una volta rosse e una volta blu.
"use strict";

function incr(n){ //IIFE di incremento
    var x=n; 
    return function(){
        return x++;
    }
}

(function () {
    var divContainer=document.getElementById("container"); 
    var b=document.createElement("button"); //creo un bottone
    var n=incr(0),a;
    var color;
    b.innerText="Aggiungi riga";
    divContainer.appendChild(b); //appendo il bottone
    b.addEventListener("click",function(){ //creo il listener per il click
        a=n(); //incremento il contatore
        a%2===0 ? color="green":color="blue"; //scelgo il colore
        var s="Sono la riga numero "+a;
        var p=document.createElement("p"); 
        p.innerText=s;
        p.style.color=color;
        divContainer.appendChild(p); //aggiungo riga
    });
})();*/