/*   Scrivi una funzione che ritorni una funzione che ritoni il numero incrementato che è 
 stato passato alla prima funzione. Ogni volta che la si chiama il numero viene incrementato di uno*/
 "use strict";
 function incr(n){ //IIFE
    var x=n;
   return function(){
       return x++;
   }
}
(function(){
    var a=incr(3),n; //assegno ad a la funzione anonima
    for(let i=0;i<5;i++){
       n=a(); //eseguo a ad ogni ciclo
       console.log(n);
    }

})();