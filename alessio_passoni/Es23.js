/** *   All'avvio della pagina controllare nel localStorage che sia presente il valore con chiave 
 * "mieiArticoli". Se non è presente recuperarli da qui https://jsonplaceholder.typicode.com/users 
 * ed inserirli nel localStorage. Dopo aver preso i dati (dal localStorage o dalla chiamata remota) 
 * stamparli a schermo.*/
"use strict";

function chiamata(){ //chiamata per recuperare users
    console.log("Non esiste");
    var $chiam=$.ajax({
        type: "GET",
        datatype:"json",
        url:"https://jsonplaceholder.typicode.com/users "
    }).done(function (response){
        console.log(response);
        localStorage.setItem("mieiArticoli",JSON.stringify(response));
        stampaValori();
    })
}

function stampaValori(){ //metto i valori in una lista
    var s=JSON.parse(localStorage.getItem("mieiArticoli"));
    var lista=document.getElementById("miaLista");
    for(let i=0;i<s.length;i++){
        var nameLi=document.createElement("li");
        nameLi.innerText=s[i].name;
        lista.appendChild(nameLi);
    }
        
}

(function () {
    if (localStorage.getItem("mieiArticoli") != null)
        stampaValori();
    else {
        chiamata();
        
    }


})();