// Inizialmente la pagina è vuota, ma ogni volta che clicco sul pulsante compare una nuova riga di testo.
"use strict";

function incr(n){ //IIFE di incremento
    var x=n; 
    return function(){
        return x++;
    }
}

(function () {
    var divContainer=document.getElementById("container"); 
    var b=document.createElement("button"); //creo un bottone
    var n=incr(0);
    b.innerText="Aggiungi riga";
    divContainer.appendChild(b); //appendo il bottone
    b.addEventListener("click",function(){ //creo il listener per il click
        var s="Sono la riga numero "+n();
        var p=document.createElement("p");
        p.innerText=s;
        divContainer.appendChild(p);
    });
})();