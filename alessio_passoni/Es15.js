/*Fare in modo che la funzione creata nel punto precedente prenda in ingresso due parametri: 
devono essere entrambi delle stringhe che rappresenteranno i due colori.
 Quindi invocare la funzione una unica volta passandogli i colori "green" e "yellow".*/
 "use strict";
 var i = 0;
 function creacolora(verde, giallo) {
     var bottone = document.getElementById("btn"); //id bottone
     var titolo = document.getElementById("titolo");//id titolo
     var color; //variabile per il colore
     var s = "nuova riga";//creo il testo
 
     function lavora() { //funzione che crea p, lo colora e lo appende
         i % 2 === 0 ? color = verde : color = giallo; //righe pari verdi e dispari gialle
         var p = document.createElement("p"); //creo un p
         p.innerText = s;//appendo il testo al paragrafo
         p.style.color = color;//assegno colore al paragrafo
         titolo.appendChild(p);//attacco il paragrafo al titolo
         i++;//incremento
     }
 
     bottone.addEventListener("click", function () {
         lavora();
     });
 
     bottone.addEventListener("click", function () {
         lavora();
     });
 }
 
 creacolora("green", "yellow");

 /*
 /**   *   Fare in modo che la funzione creata nel punto precedente prenda 
 * in ingresso due parametri: devono essere entrambi delle stringhe che 
 * rappresenteranno i due colori. Quindi invocare la funzione una unica 
 * volta passandogli i colori "green" e "yellow".
"use strict";
var a = incr(0);

function incr(n) { //IIFE di incremento
    var x = n;
    return function () {
        return x++;
    }
}

function creaPar(colore1, colore2) {
    var i=0;
    while(i<2){
        var cont = a();
        var div = document.getElementById("container");
        var color;
        var s = "Sono la riga numero " + cont;
        var p = document.createElement("p");
        p.innerText = s;
        cont % 2 === 0 ? color = colore1 : color = colore2;
        p.style.color = color;
        div.appendChild(p);
        i++;
    }
    
}

(function () {
    var divContainer = document.getElementById("container");
    var b = document.createElement("button"); //creo un bottone

    b.innerText = "Aggiungi riga";
    divContainer.appendChild(b); //appendo il bottone
    b.addEventListener("click", function () { //creo il listener per il click
        creaPar("green", "yellow");

    });
})();*/