"use strict";

app.factory("servizioPost", function ($http, $q) {
    return (function () {
        var deferred = $q.defer();
        $http.get('https://jsonplaceholder.typicode.com/posts')
            .then(
                function (response) {
                    //console.log(response.data);
                    deferred.resolve(response.data);
                }
            )

        return deferred.promise;
    })();
});