app.factory("servizioCommenti", function ($http, $q) {
    return (function () {
        var deferred = $q.defer();
        $http.get('https://jsonplaceholder.typicode.com/comments')
            .then(
                function (response) {
                    deferred.resolve(response.data);
                    //console.log(response.data);
                }
            )

        return deferred.promise;
    })();
});