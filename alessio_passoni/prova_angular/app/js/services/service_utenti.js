"use strict";

app.factory("servizioUser", function ($http, $q) {
    return (function () {
        var deferred = $q.defer();
        $http.get('https://jsonplaceholder.typicode.com/users')
            .then(
                function (response) {
                    deferred.resolve(response.data);
                }
            )

        return deferred.promise;
    })();
});