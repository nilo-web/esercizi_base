"use strict";

var app = angular.module("myApp", []);

var visualizzaPost = (function (id, $scope) {
    $scope.elenco = [];//array di oggetti per tenere ogni post il suo commento
    var cont; //contatore commenti per post
    for (var i = 0; i < $scope.post.length; i++) { //ciclo sui post
        if ($scope.post[i].userId === id) {//controllo che l'id user sia uguale a quello userId della variabile
            //console.log($scope.post[i].title); //stampo in console
            cont = 0;
            var post = $scope.post[i].title;
            var pId;
            for (var j = 0; j < $scope.comments.length; j++) { //ciclo su tutti i commenti per trovare quelli del post
                if ($scope.comments[j].postId === $scope.post[i].id) {//controllo che l'id user sia uguale a quello userId della variabile
                    pId=$scope.comments[j].postId;
                    cont++;
                }
            }
            $scope.elenco.push({ titolo: post, comment: cont, postId: pId }); //pusho valori
        }
    }
});

var visualizzaCommenti = (function (id, $scope) {
    //console.log(id);
    var elencoCommenti=[];
    for (var i in $scope.comments)
        if ($scope.comments[i].postId === id)
            elencoCommenti.push($scope.comments[i].body)
            //console.log($scope.comments[i].body);

            alert(elencoCommenti);

});


app.controller("contrUtente", function ($scope, servizioUser, servizioPost, servizioCommenti) {
    $scope.post = [];
    $scope.utenti = [];
    $scope.comments = [];
    servizioUser.then(function (data) {
        //console.log(data);
        for (var i in data)
            $scope.utenti.push(data[i]);
    });
    servizioPost.then(function (data) {
        //console.log(data);
        for (var i in data) {
            $scope.post.push(data[i]);
        }
        servizioCommenti.then(function (data) {
            for (var i in data)
                $scope.comments.push(data[i]);
        })
        $scope.visPost = function (id) { //funzione per visualizzare i post
            visualizzaPost(id, $scope);
        }
        $scope.visCommento = function (id) { //funzione per visualizzare i post
            console.log(id);
            visualizzaCommenti(id, $scope);
        }

    });
});

