
myApp.controller("creaItemCTRL", function($scope, todoListItemsService) {

  console.log("ctrl crea")

  $scope.createNewElement = function() {
    
    var newElement = {
      title: $scope.newTodoItemTitle,
      content: $scope.newTodoItemContent
    };

    console.log("nuovo elemento creato", newElement);

    todoListItemsService.addItem(newElement);

    $scope.newTodoItemContent = "";
    $scope.newTodoItemTitle = "";
  }

});

