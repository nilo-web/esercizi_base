"use strict";

var myApp = angular.module("myApp", []);

myApp.controller("contrUtente", function($scope){

});

myApp.directive("myUser", function () {
    return {
        restrict: 'E',
        templateUrl: "/myUserCardTemplate.html"
    };
});