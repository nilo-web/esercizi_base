"use strict";

var app = angular.module("myApp", ["ngRoute"]);


app.config(function ($routeProvider) {
  $routeProvider
    .when("/", {
      templateUrl: "/templates/registration.html",
      controller: "registrationController",
    })
    .when("/cardutenti/:user", {
      templateUrl: "/templates/cardUtenti.html",
      controller: "detailController",
    })
    .when("/student/:username", {
      templateUrl: "/templates/student.html",
      controller: "studentController",
    })
    .when("/accedi", {
      templateUrl: "/templates/login.html",
      controller: "loginController",
    })
    .when("/logged", {
      templateUrl: "/templates/logged.html",
      controller: "loginController",
    })
    .otherwise({
      redirectTo: "/",
    });
});

app.run(["$rootScope","$location","authService",
  function($rootScope,$location,authService){
    $rootScope.$on("$routeChangeStart", function(event){ //quando cambio view ma prima
      if(!authService.isLoggedIn()){
        console.log("NON SEI LOGGATO",$location,event);
      $location.path("/");
      }
      else{
        console.log("PREGO, AVANTI");
      }

    })   
  }
]);

app.controller("registrationController", function ($scope, $location, dettaglioUtente) {
  $scope.reg = function (user) {
    //console.log(user.name);
    dettaglioUtente.setUtente(user);
    
    $location.path('/cardutenti/' + user.name);
  }  

  $scope.accedi = function () {
    //console.log(user.name);    
    $location.path('/accedi');
  }  
    

});

app.controller("detailController", function ($scope, $routeParams) {
$scope.user=$routeParams.user;

});

app.controller("loginController", function ($scope, $location) {
$scope.logged=function(){
  $location.path("/logged");
}
});

app.controller("studentController", function ($scope, $routeParams) {
  $scope.userame = $routeParams.username;
});


