"use strict";

//USO app.js della funzione 2 colori

function f(color1, color2) {

  console.log(color1, color2);

  var myContainer = document.getElementById("container");

  var myButton1 = document.createElement("button");
  myButton1.innerText = "Inserisci";

  var myButton2 = document.createElement("button");
  myButton2.innerText = "Elimina";

  myContainer.appendChild(myButton1);
  myContainer.appendChild(myButton2);

  var lastColor = color1;

  myButton1.addEventListener("click", function () {
    console.log("click!");
    var myP = document.createElement("p");
    myP.innerText = "Ciao";
    myP.style.color = lastColor;
    myContainer.appendChild(myP);

    if (lastColor === color1) {
      lastColor = color2;
    } else {
      lastColor = color1;
    }
  });

  myButton2.addEventListener("click", function () {
    console.log("click!");
    var myPList = document.getElementsByTagName("p"); //prendo tutti i tag p
    var i;
    
    for (i = myPList.length - 1; i >= 0; i--) //ciclo nella lista
     myPList[i].remove(); //rimuovo l'iesimo dalla lista genitrice
  });
}


f("green", "yellow");

/*
/** Modificare l'esercizio precedente in modo inserire 
 * un altro pulsante: se lo schiaccio devo cancellare
 *  tutte le righe inserite.
"use strict";
var a = incr(0);

function incr(n) { //IIFE di incremento
    var x = n;
    return function () {
        return x++;
    }
}

function creaPar(colore1, colore2) {
    var i=0;
    while(i<2){
        var cont = a();
        var div = document.getElementById("container");
        var color;
        var s = "Sono la riga numero " + cont;
        var p = document.createElement("p");
        p.innerText = s;
        cont % 2 === 0 ? color = colore1 : color = colore2;
        p.style.color = color;
        div.appendChild(p);
        i++;
    }
    
}

(function () {
    var divContainer = document.getElementById("container");
    var b = document.createElement("button"); //creo un bottone
    var bElim=document.createElement("button");
    var i;
    b.innerText = "Aggiungi riga";
    bElim.innerText = "Elimina righe";
    divContainer.appendChild(b); //appendo il bottone
    divContainer.appendChild(bElim); //appendo il bottone

    b.addEventListener("click", function () { //creo il listener per il click
        creaPar("green", "yellow");

    });

    bElim.addEventListener("click", function(){
        var righe=document.getElementsByTagName("p");
        for(i=righe.length-1;i>=0;i--)
            righe[i].remove();
    });
})();*/