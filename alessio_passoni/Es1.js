"use strict";
var array1 = [20, 33, 12, 1233];
var array2 = [1, 33, 102, -12];

/*
//Primo metodo

"use strict";
var array1 = [20, 33, 12, 1233];
var array2 = [1, 33, 102, -12];

//Come avevo fatto io

//concateno i due array
var concat = array1.concat(array2);
//ordino l'array unito
concat.sort(function s(a, b) { return a - b });
//elimino il duplicato
var i;
var nArr = [concat[0]]; //creo nuovo array con primo elemento della concatenazione
for (var i = 1; i < concat.length; i++) { //faccio partire il nuovo array da 1
    if (concat[i] != concat[i - 1]) //controllo che l'elemento da aggiungere non sia uguale al precedente
    //della concatenazione
        nArr.push(concat[i]); //aggiungo elemento
}
console.log(nArr);

*/


/*  Concatena, senza duplicati, i seguenti array e ordinali per valore crescente
javascript AGGIORNATO*/
"use strict";
function pushElem(arrTot,arrPushed){
    for(let i=0;i<arrPushed.length;i++){
        if(arrTot.indexOf(arrPushed[i])===-1) //controllo se l'indice del valore è presente, altrimenti add
            arrTot.push(arrPushed[i]);
    }
    
}

function ordarray(){
    var array1 = [20, 33, 12, 1233];
    var array2 = [1, 33, 102, -12];
    var nArr=[];
    pushElem(nArr,array1);
    pushElem(nArr,array2)
    nArr=nArr.sort(function (a,b) {return a-b;}); //ordino l'array
    console.log(nArr);
}

ordarray();