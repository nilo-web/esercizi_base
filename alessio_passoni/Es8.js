/*Scrivi una funzione che ritorna una funziona che ritorna il 
numero che è stato passato alla prima funzione. Es test(7)() //  7.*/
"use strict";

function fun1(n){ //IIFE
    return function(){
        return n;
    }
}

(function(){
   console.log(fun1(3)());//Eseguo IIFE
})();