"use strict";

function UserPost(postObj) {
    this.uid = postObj.userId;
    this.id = postObj.id;
    this.title = postObj.title;
    this.$tpl = $('<li class="post-utente"></li>');
}

UserPost.prototype.renderPost = function($container) {
    // creo il contenuto da inserire nella card
    var contenutoDettPost = this.title;

    // inserisco il contenuto post
    this.$tpl.html(contenutoDettPost);

    // aggiunto Id attraverso un attribute magari per futuri commenti popup
    this.$tpl.attr("data-id-utente", this.id);

    //this.$tpl.on("click", this.clickHandler.bind(this));

    // aggiungo post alla lista
    $container.append(this.$tpl);
};

/*
UserCard.prototype.clickHandler = function(evento) {
    console.log("sono stato cliccato ", this, evento);
    // this.$tpl.toggleClass("active");

    $d.trigger("userSelected", this.id);
};*/