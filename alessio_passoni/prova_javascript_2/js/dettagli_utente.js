"use strict";

var DettagliUtente = (function () {

  function svuotaElem($container){ //cancello elementi
    $container.empty();
  }

  var instance;

  // seleziono il contenitore dei dettagli dell'utente
  var $contenitoreDettagliUtente = $("#dettagliUtente");

  // seleziono il bottone chiusura
  var $btnClose = $("#btnClose");


  // cerco all'interno del contenitore dei dettagli utente il posto dove inserire il nome
  var $dettagliUtenteNome = $contenitoreDettagliUtente.find("h2");


  // cerco all'interno del contenitore dei dettagli utente il posto dove inserire i suoi articoli/posts
  var $listaArticoliUtente = $contenitoreDettagliUtente.find(
    "#listaArticoliUtente"
  );

  $btnClose.on("click", function(){
    svuotaElem($dettagliUtenteNome); //svuoto h2
    svuotaElem($listaArticoliUtente); //svuoto lista
  });

  return {
    getInstance: function () {
      if (!instance) {
        instance = {
          render: function (userCardSelected) {
            console.log("User card selezionato", userCardSelected);

            $dettagliUtenteNome.html(userCardSelected.name);

            //SERVIZIO CHE PRENDE I POST UTENTE
            var post = getPostAJAX(userCardSelected.id);
            post.then(
              function (responseListaPost) {
                //Svuoto la lista dai post vecchi
                svuotaElem($listaArticoliUtente);
                // ciclo sulla lista e aggiungo ogni post alla lista
                for (var i = 0; i < responseListaPost.length; i++) {
                  var postResponse = responseListaPost[i];

                  var istanzaPostUtente = new UserPost(postResponse);
                  istanzaPostUtente.renderPost($listaArticoliUtente);

                }
              },
              function (error) {
                alert("Ops la chiamata non ha funzionato");
              }
            );
          },
        };
      }

      return instance;
    },
  };
})();
