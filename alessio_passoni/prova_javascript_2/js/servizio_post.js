"use strict";

function getPostAJAX(id) {
  // genero la chiamata http passandogli un oggetto di configurazione
  // con l'url e il verbo http
  var chiamataPerPost = $.ajax({
    type: "GET",
    url: "https://jsonplaceholder.typicode.com/posts?userId="+id,
  });

  return chiamataPerPost;
}