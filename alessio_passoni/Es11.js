/**  Scrivi una funzione che ogni volta che viene chiamata ritorni il numero 
 * successivo della serie di fibonacci */
"use strict";



var a = (function () { //funzione IIFE
    var primo = 1;
    var secondo = 1;
    var terzo;
    return function () {
        terzo = primo + secondo;
        primo = secondo;
        secondo = terzo;
        return terzo; //ritorno n
    }
})();
console.log(0);
console.log(1);
console.log(1);//i primi tre non sono presenti nella IIFE
console.log(a());//stampo
console.log(a());//stampo
console.log(a());//stampo
console.log(a());//stampo
console.log(a());//stampo
console.log(a());//stampo
console.log(a());//stampo

/* APPROCCIO DIVERSO USANDO UN OGGETTO
//  *   Scrivi una funzione che ogni volta che viene chiamata ritorni il numero 
//successivo della serie di fibonacci
"use strict";

function nextFibo(objNext) {
    objNext.risultato = objNext.primo + objNext.secondo;
    objNext.secondo = objNext.primo;
    objNext.primo = objNext.risultato;
}

(function () {
    var objFibo = { //creo un oggetto con primo secondo e risultato
        "primo": 1,
        "secondo": 0,
        "risultato": 0
    }
    console.log(1);
    for (let i = 0; i < 10; i++) {
        nextFibo(objFibo); //funzione che mi ritorna il prossimo numero della seria
        console.log(objFibo.risultato);
    }

})();
*/