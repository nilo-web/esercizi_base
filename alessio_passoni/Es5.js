//Crea una funzione che stampi la data corrente in questo formato, es: "martedi 22, febbraio 2021"
"use strict";
(function () {
    var dateToday = new Date();
    var arrMesi =
        ["Gennaio", "Febbraio", "Marzo",
            "Aprile", "Maggio", "Giugno",
            "Luglio", "Agosto", "Settembre",
            "Ottobre", "Novembre", "Dicembre"];
    var arrGiorni =
        ["Domenica", "Lunedì", "Martedì",
            "Mercoledì", "Giovedì", "Venerdì",
            "Sabato"];
    var s=arrGiorni[dateToday.getDay()]+' '+dateToday.getDate()+', '+arrMesi[dateToday.getMonth()]+' '+dateToday.getFullYear();
    console.log(s);
})();