app.controller("registrationController", ["$scope","$location","myUser","localStorageUsers","getUsers",
function ($scope, $location, myUser,localStorageUsers,getUsers) {
  if(localStorage.length === 0){ //se localstorage è vuoto prendo dati da json fake
    var s=getUsers.getAllUsers();
    s.then(function (data){
      localStorageUsers.setUsers(data);
  
  }, function (errorResponse) { //ritorno della promise http
      console.log("http Get Error",errorResponse);
      $location.path("/");
  });
  }

    $scope.sendData = function (user) { //registrazione
    myUser.setUser(user); //set utente corrente
    console.log(user);
    localStorageUsers.setUsers(user); //salvo utente in localstorage
    $location.path("/userpage/" + user.username); //cambio template
  };
  $scope.redirect = function (url) {
    $location.path(url);
  };

}]);
  