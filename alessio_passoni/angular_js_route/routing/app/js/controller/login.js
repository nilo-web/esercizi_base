app.controller("loginController", ["$scope","$location","myUser",
function ($scope, $location, myUser) {
  $scope.sendData = function (user) {
    var isReg=myUser.checkUser(user); //controllo che l'utente sia registrato
    if(isReg){
      myUser.setUser(user); //redirect alla userpage
      $location.path("/userpage/" + user.username);
    }
    alert("Non sei registrato"); //redirect alla registrazione
    $location.path("/registration");
  };
  $scope.redirect = function (url) { //funzione che dato l'url cambia il template
    $location.path(url);
  };

}]);
