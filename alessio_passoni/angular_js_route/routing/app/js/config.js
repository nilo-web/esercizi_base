
app.config(function ($routeProvider) {
    $routeProvider
      .when("/registration", { //pagina registrazione
        templateUrl: "/templates/registration.html",
        controller: "registrationController",
      })
      .when("/userpage/:username", { //pagina utente work in progress
        templateUrl: "/templates/userpage.html",
        controller: "userpageController",
      })
      .when("/", { //pagina login
        templateUrl: "/templates/login.html",
        controller: "loginController",
      })
      .otherwise({ //url sbagliato lo porto alla login
        redirectTo: "/",
      });
  });
  