app.run([
    "$rootScope",
    "$location",
    "authService",
    function ($rootScope, $location, authService) {
      $rootScope.$on("$routeChangeStart", function (event) {
        if (!authService.isLoggedIn()) {
          console.log("DENY", $location, event);
          $location.path("/");
        } 
      });
    },
  ]);