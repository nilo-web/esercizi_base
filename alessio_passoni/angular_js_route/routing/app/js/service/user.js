app.service("myUser", function () {
  this.user = {};

  this.setUser = function (u) { //setto l'user
    this.user = u;
  };
  this.checkUser=function(u){ //check se l'utente è nel localstorage
    if (localStorage.getItem(u.username) === null) {
      //console.log("NON SEI REGISTRATO");
      return 0;
    }
    return 1;
  };
  this.getUser = function () {
    return this.user;
  };
  this.updateUser=function(u){ //aggiornamento utente work in progress
    this.user=u;
    return this.user
  };
});