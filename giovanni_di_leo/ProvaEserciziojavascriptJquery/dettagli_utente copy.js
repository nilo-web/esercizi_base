"use strict";

var DettagliUtente = (function () {
  var instance;

  // seleziono il contenitore dei dettagli dell'utente
  var $contenitoreDettagliUtente = $("#dettagliUtente");

  // cerco all'interno del contenitore dei dettagli utente il posto dove inserire il nome
  var $dettagliUtenteNome = $contenitoreDettagliUtente.find("h2");

  // cerco all'interno del contenitore dei dettagli utente il posto dove inserire i suoi articoli/posts
  var $listaArticoliUtente = $contenitoreDettagliUtente.find(
    "#listaArticoliUtente"
  );

  return {
    getInstance: function () {
      if (!instance) {
        instance = {
          render: function (userCardSelected) {
            console.log("User card selezionato", userCardSelected);

            $dettagliUtenteNome.html(userCardSelected.name);
          },
        };
      }

      return instance;
    },
  };
})();
