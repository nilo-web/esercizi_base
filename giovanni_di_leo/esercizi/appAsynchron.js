"use strict";

// il mio script principale. la logica riguardante la presa dei
// viene demandata sempre al servizio
var servizioAsync = chiamata();

// operazioni asincrone
servizioAsync.then(
  // success callback
  function (response) {
    console.log("success", response);
  },

  // error callback
  function (error) {
    console.log("Errore");
  }
);
