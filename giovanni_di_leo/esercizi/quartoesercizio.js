/**   Ordina il seguente array di oggetti in base all’id:
```javascript
var objectsList = [
 { name: "Pino", id: 66 },
 { name: "Rino", id: 16 },
 { name: "Dino", id: 6 },
 { name: "Lino", id: 96 },
 { name: "Gino", id: 26 }
];*/

var objectsList = [
    { name: "Pino", id: 66 },
    { name: "Rino", id: 16 },
    { name: "Dino", id: 6 },
    { name: "Lino", id: 96 },
    { name: "Gino", id: 26 }
   ];

   //questo è il metodo che avrei utilizzato se fosse stato un array di numeri
   var arrayTest = [4,6,3,1,2];

   var risultato = arrayTest.sort(function (a,b) {
    return a-b
   });

   console.log(risultato);

   var risultato = objectsList.sort(function (a,b) {
    return a.id - b.id;
   });

   console.log(risultato);