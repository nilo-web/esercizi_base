
// servizio che si occupa di dare i dati alla mia applicazione.
// ritorno sempre un oggetto asincrono in modo da poterlo prendere
// sempre col metodo .then()
function chiamata() {
    //chiamata per recuperare users
    console.log("metodo chiamata");
  
    var deferred = $.Deferred();
  
    if (localStorage.getItem("mieiArticoli")) {
      deferred.resolve(JSON.parse(localStorage.getItem("mieiArticoli")));
    } else {
      $.ajax({
        type: "GET",
        datatype: "json",
        url: "https://jsonplaceholder.typicode.com/users",
      }).then(
        function (response) {
          console.log(response);
          localStorage.setItem("mieiArticoli", JSON.stringify(response));
  
          deferred.resolve(JSON.parse(localStorage.getItem("mieiArticoli")));
        },
        function (error) {
          console.log("Errore");
        }
      );
    }
  
    return deferred;
  }