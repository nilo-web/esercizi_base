
//----------------------------------------------------------------------
//*   Fai il reverse di questa string: “supercalifragilistichespiralidoso”

var stringa = "supercalifragilistichespiralidoso";

function revString(stringa) {
  //  var spltString = stringa.split("");
  //  var revArray = spltString.reverse();
    return stringa.split("").reverse().join("");
};

console.log(revString(stringa));
