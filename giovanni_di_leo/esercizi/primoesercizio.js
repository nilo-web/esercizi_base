"use strict";

/**   Concatena, senza duplicati, i seguenti array e ordinali per valore crescente
```javascript
var array1 = [20, 33, 12, 1233];
var array2 = [1, 33, 102, -12];
 * 
 */

var array1 = [20, 33, 12, 1233];
var array2 = [1, 33, 102, -12];

var nuovoArray = [];

for(var i= 0; i < array1.length; i++) {
    var arrayItem = array1[i];
    if(nuovoArray.indexOf(arrayItem === -1)){
        nuovoArray.push(arrayItem);
    }
}

for(var i= 0; i < array2.length; i++) {
    var arrayItem = array2[i];
    if(nuovoArray.indexOf(arrayItem === -1)){
        nuovoArray.push(arrayItem);
    }
}

nuovoArray = nuovoArray.sort(function(a,b){
    return a - b ;
});


