"use strict"

/*var titolo = document.getElementById("titolo");
titolo.style.color = "blue";


var container = document.getElementById("container");
var bottone = document.getElementById("btn1");
var contatore = 0;
var contenuto = "Sono il bottone premuto numero: ";
bottone.addEventListener("click" , function () {
    contatore ++;

    var nuovoContenuto = document.createElement("p");
    nuovoContenuto.innerText = contenuto + contatore;
    container.appendChild(nuovoContenuto);
 console.log("sono l'azione associata all'evento");
});


//*   Inizialmente la pagina è vuota, ma ogni volta che clicco sul pulsante compare una nuova riga di testo,
// ma le righe saranno una volta rosse e una volta blu.

var colore = document.getElementById("colore");
var bottoneColorato = document.getElementById("bottonecolorato");

bottoneColorato.addEventListener("click", function(){

    var fraseColorata = document.getElementById("h3");
    //qui metto la condizione per stampare la frase 
    if(fraseColorata){
        
        fraseColorata.style.color = "red";
    } else {
        fraseColorata.style.color = "blue";
    }

    console.log("azione dentro bottone");
});

/*var persona = {
    nome : "Mario",
    cognome : "Rossi",
    nomeCognome: function() {
        return this.nome + " " + this.cognome;
     }
};

var a = {
    //questo è un aggetto
    nome : "Mario",
    cognome : "Rossi",
}

function saluta(nomeCallBack) {
    console.log("Ciao " + nomeCallBack.call(persona));
};

//bind
saluta(persona.nomeCognome.bind(persona));

//DOM ESERCITAZIONE
// per prendere e modificare ciò che che ho all'interno del HTML
var lista = document.getElementById("listaSpeciale");   // torna gli elementi che hanno l'ID specificato
var elementiLista = document.getElementsByTagName("li"); // torna gli elementi che hanno il tag specificato
var lista2 = document.querySelector("#listaSpeciale") // selectorall(per tutti gli elementi)

lista.hasAttribute("")
lista.setsAttribute("data-mioattributo", "pippo");  //il primo è il nome, il secondo "" è il valore
 
//creare nuovo elemento 
var p =document.createElement("p");
p.innerHTML = "asd"; //innerText solo per testo p.innertxt =  "<b> asd</b>"
var testo = document.createTextNode("asd") // creare direttamente un testo
//jquery
 var $lista = $('[data.attribute=123]')*/

 /** */
 /***   Modificare l'esercizio precedente in modo inserire un altro pulsante: se lo schiaccio 
  * devo cancellare tutte le righe inserite. */
 var myContainer = document.getElementById("container");

 var myButton = document.createElement("button");
// creo il pulsante per eliminare tutto
var btnCancella = document.createElement("button");

 myButton.innerText = "Cliccami";
 //scrivo nel bottone 
 btnCancella.innerText = "Cliccami per cancellare";

 myContainer.appendChild(myButton);
 
 //appendo il tutto al bottone
 myContainer.appendChild(btnCancella);

 var lastColor = "red";

 myButton.addEventListener("click" , function(){
     console.log("click");
     var myP = document.createElement("p");
     myP.innerText = "Ciao";
     myP.style.color = lastColor;
     myContainer.appendChild(myP);

     if(lastColor === "red"){
        lastColor = "blue";
     }
     else {
         lastColor = "red";
     }
 });

 btnCancella.addEventListener("click" , function() {
    console.log("click cancella");
   // document.getElementById('cancella').innerHTML = '';
   // removechildren
   //selezionare p e cancellare solo p
   
    var callP = document.getElementsByTagName("p");
    console.log(callP);
    //callP[0].remove();
    for(var i = 0; i<callP.length; i++) {
       callP[i].remove();
    }
    //list.removeChild(list.childNodes[0])

 });
