"use strict";

function UserCard(userObj) {
    this.id = userObj.id;
    this.name = userObj.name;
    this.username = userObj.username;
    this.email = userObj.email;
    this.$tpl = $('<div class="card-utente"></div>');
}

UserCard.prototype.renderCard = function($container) {
    // creo il contenuto da inserire nella card
    var contenutoTestualeCard = this.name + " - " + this.username + ": " + this.email;

    // inserisco il contenuto nella card
    this.$tpl.html(contenutoTestualeCard);

    // gli aggiunto lo userId attraverso un attribute in modo da sapere,
    // succesivamente, che id ha l'utente che è stato cliccato
    this.$tpl.attr("data-id-utente", this.id);

    this.$tpl.on("click", this.clickHandler.bind(this));

    // aggiungo la card nella pagina html all'interno della mia lista che conterrà tutte le card
    $container.append(this.$tpl);
};

UserCard.prototype.clickHandler = function(evento) {
    console.log("sono stato cliccato ", this, evento);
    // this.$tpl.toggleClass("active");

    $d.trigger("userSelected", this.id);
};