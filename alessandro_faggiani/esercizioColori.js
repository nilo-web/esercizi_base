"use strict";

function f(color1, color2) {

  console.log(color1, color2);      //stampo nella console i colori che gli passo

  var myContainer = document.getElementById("container");           //primo contenitore dichiarato in html
  var myNewContainer = document.getElementById("newContainer");     //secondo contenitore dichiarato in html

  var myButton = document.createElement("button");    //Dichiaro il primo bottone,
  myButton.innerText = "Cliccami";                    // con scritto Cliccami
  myContainer.appendChild(myButton);                  //lo metto nella pagina appendendolo in myContainer

  var cancellaRighe = document.createElement("button");     //Dichiaro il secondo bottone,
  cancellaRighe.innerText = "Cancella";                     // con scritto Cancella
  myContainer.appendChild(cancellaRighe);                   //lo metto nella pagina appendendolo in myContainer

  var autodistruzione = document.createElement("button");       //Dichiaro il terzo bottone,
  autodistruzione.innerText = "Autodistruzione";             // con scritto Svuota contenitore
  myContainer.appendChild(autodistruzione);                     //lo metto nella pagina appendendolo in myContainer

  var lastColor = color1;           //Dichiaro la variabile lastColor dove dico che prende il valore di color1

  myButton.addEventListener("click", function () {      //sto dicendo che al click parte la funzione
    console.log("click!");
    var myP = document.createElement("p");              //va a creare un elemento <p> in myNewContainer con scritto Ciao
    myP.innerText = "Ciao";
    myP.style.color = lastColor;                        //gli dico che il mio colore deve essere lastColor
    myNewContainer.appendChild(myP);

    if (lastColor === color1) {                 //Se il mio colore era il primo, allora la seconda riga deve essere il secondo
      lastColor = color2;                       // es. verde, giallo, verde, giallo.... ecc
    } else {
      lastColor = color1;
    }
  });

  cancellaRighe.addEventListener("click", function(){     //Dichiaro una funzione per il bottone cancellaRighe in 
    while (myNewContainer.childNodes.length>0){           // al click mi va a rimuovere tutte le righe in myNewContainer
      myNewContainer.childNodes[0].remove();
    }
  });

  autodistruzione.addEventListener("click", function(){
    myContainer.remove();
    myNewContainer.remove();
  })

var httpReq = new XMLHttpRequest();
httpReq.onreadystatechange = function(){
if(httpReq.readyState === 4 && httpReq.status === 200){
  console.log("SI", httpReq.response);
  var response = JSON.parse(httpReq.response);
}
else{
  console.log("NO");
}
}
}

f("green", "yellow");                      //richiamo la funzione che ho definito
