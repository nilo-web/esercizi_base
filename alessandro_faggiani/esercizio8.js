// Scrivi una funzione che ritorna una funzione che ritorna il numero che è stato passato alla prima funzione. es test(7)() // 7.
"use strict";
function funzioneNumeri(numero){
    var numeroPassato = numero;
    return function (){
        return numeroPassato;
    }
}

var risultato = funzioneNumeri(23);
console.log(risultato());