//Scrivi una funzione che ritorna un numero e lo incrementi ogni volta che viene chiamata (IFFE)
"use strict";
var IFEE = (function (numero){
    var contatore = numero;
    return function(){
        contatore++;
        console.log(contatore);
    }
})(23);

IFEE();
IFEE();
IFEE();