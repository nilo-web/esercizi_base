"use strict";

// NB DIFFERENZA SELEZIONE E CREAZIONE TRA JQUERY E JAVASCRIPT NATIVO
// se passo il selettore a jquery me lo cerca come se usassi il document.getEcc...
// $("p") = document.getElementsByTagName("p")
// se passo una stringa di codice html me lo genera come se usassi document.createElement
// $("<p></p>") = document.createElement("p")

// seleziono il contenitore degli utenti
var $contenitoreListaUtenti = $("#listaUtenti");

// seleziono il contenitore dei dettagli dell'utente
var $contenitoreDettagliUtente = $("#dettagliUtente");

// cerco all'interno del contenitore dei dettagli utente il posto dove inserire il nome
var $dettagliUtenteNome = $contenitoreDettagliUtente.find("h2");

// cerco all'interno del contenitore dei dettagli utente il posto dove inserire i suoi articoli/posts
var $listaArticoliUtente = $contenitoreDettagliUtente.find("#listaArticoliUtente");

// variabile per tenere traccia dell'elenco degli utenti: è un array che contienre una lista di oggetti
var listaUtenti;

// genero la chiamata http passandogli un oggetto di configurazione
// con l'url e il verbo http
var chiamataPerUtenti = $.ajax({
  type: "GET",
  url: "https://jsonplaceholder.typicode.com/users",
});

// promessa mantenuta: success callback - inserisco le card degli utenti nel DOM
chiamataPerUtenti.done(function (rispostaChiamata) {
  // mi salvo in un oggetto globale i miei utenti cosi da renderli disponibili in tutta l'applicazione
  listaUtenti = rispostaChiamata;

  // ciclo sulla lista e per ogni utente creo ed inserisco nel dom una "card"
  for (var i = 0; i < listaUtenti.length; i++) {
    var utenteTmp = listaUtenti[i];

    // creo una card, cioè un elemento html con jquery
    var $card = $('<div class="card-utente"></div>');

    // creo il contenuto da inserire nella card
    var contenutoTestualeCard =
      utenteTmp.name + " - " + utenteTmp.username + ": " + utenteTmp.email;

    // inserisco il contenuto nella card
    $card.html(contenutoTestualeCard);

    // gli aggiunto lo userId attraverso un attribute in modo da sapere,
    // succesivamente, che id ha l'utente che è stato cliccato
    $card.attr("data-id-utente", utenteTmp.id);

    // aggiungo la card nella pagina html all'interno della mia lista che conterrà tutte le card
    $contenitoreListaUtenti.append($card);

    // sto in ascolto di un eventuale click sulla specifica card
    $card.on("click", function (evento) {
      console.log("sono stato cliccato", evento.target);

      // inizializzo una variabile che mi terrà l'utente selezionato
      var utenteSelezionato;

      // cerco tra tutti gli utenti della lista l'utente corrispondente a quello cliccato
      for (var j = 0; j < listaUtenti.length; j++) {
        var utenteTmp = listaUtenti[j];

        // la discriminante per la mia selezione è il valore dell'attributo che ho creato in precedenza: data-id-utente
        if (utenteTmp.id == evento.target.getAttribute("data-id-utente")) {
          utenteSelezionato = utenteTmp;
        }
      }

      // una volta trovato l'utente corrispondente al click aggiorno il titolo
      $dettagliUtenteNome.html(utenteSelezionato.name);

      // url per la chiamata ajax alla quale passo in query string un valore corrispondente all'id dell'utente selezionato
      var urlChiamataPostUtente = "https://jsonplaceholder.typicode.com/posts?userId=" + utenteSelezionato.id; 

      // genero la chiamata http passandogli un oggetto di configurazione
      // con l'url e il verbo http
      var chiamataPerPostUtente = $.ajax({
        type: "GET",
        url: urlChiamataPostUtente,
      });

      // callback di success - promessa mantenuta
      chiamataPerPostUtente.done(function (responseConArticoliUtente) {

        // resetto il contenuto all'interno del contenitore degli articoli sostituendolo con una stringa vuota
        $listaArticoliUtente.html("");
        
        // ciclo sui risultati della chiamata, ossia gli articoli/post dell'utente e per ognuno di essi genero un post
        for (var x = 0; x < responseConArticoliUtente.length; x++) {

          var articoloTmp = responseConArticoliUtente[x];

          var $articoloUtente = $("<li></li>");
          $articoloUtente.html(articoloTmp.title);

          $listaArticoliUtente.append($articoloUtente);
        }
        
      });

      // fallback di errore - promessa non mantenuta
      chiamataPerPostUtente.fail(function(errore) {
        alert("Ops errore nella chiamata dei post dell'utente!");
      });
      
    });
  }
});

// creo una fallback per gestire un eventuale errore
chiamataPerUtenti.fail(function (error) {
  alert("Ops! Qualcosa è andato storto!");
});

