"use strict";

function cardUtenteMain(){
for (var i = 0; i < listaUtenti.length; i++) {
    var utenteTmp = listaUtenti[i];

    // creo una card, cioè un elemento html con jquery
    var $card = $('<div class="card-utente"></div>');

    // creo il contenuto da inserire nella card
    var contenutoTestualeCard =
      utenteTmp.name + " - " + utenteTmp.username + ": " + utenteTmp.email;

    // inserisco il contenuto nella card
    $card.html(contenutoTestualeCard);

    // gli aggiunto lo userId attraverso un attribute in modo da sapere,
    // succesivamente, che id ha l'utente che è stato cliccato
    $card.attr("data-id-utente", utenteTmp.id);

    // aggiungo la card nella pagina html all'interno della mia lista che conterrà tutte le card
    $contenitoreListaUtenti.append($card);

    eventoCardMain();
}
}