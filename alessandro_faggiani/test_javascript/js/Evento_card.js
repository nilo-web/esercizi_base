"use strict";

function eventoCardMain(){
// sto in ascolto di un eventuale click sulla specifica card
$card.on("click", function (evento) {
    console.log("sono stato cliccato", evento.target);

    // inizializzo una variabile che mi terrà l'utente selezionato
    var utenteSelezionato;

    // cerco tra tutti gli utenti della lista l'utente corrispondente a quello cliccato
    for (var j = 0; j < listaUtenti.length; j++) {
      var utenteTmp = listaUtenti[j];

      // la discriminante per la mia selezione è il valore dell'attributo che ho creato in precedenza: data-id-utente
      if (utenteTmp.id == evento.target.getAttribute("data-id-utente")) {
        utenteSelezionato = utenteTmp;
      }
    }

    // una volta trovato l'utente corrispondente al click aggiorno il titolo
    $dettagliUtenteNome.html(utenteSelezionato.name);

    chiamataPostUtentiMain();
});
}