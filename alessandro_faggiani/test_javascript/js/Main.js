"use strict";

// seleziono il contenitore degli utenti
var $contenitoreListaUtenti = $("#listaUtenti");

// seleziono il contenitore dei dettagli dell'utente
var $contenitoreDettagliUtente = $("#dettagliUtente");

// cerco all'interno del contenitore dei dettagli utente il posto dove inserire il nome
var $dettagliUtenteNome = $contenitoreDettagliUtente.find("h2");

// cerco all'interno del contenitore dei dettagli utente il posto dove inserire i suoi articoli/posts
var $listaArticoliUtente = $contenitoreDettagliUtente.find("#listaArticoliUtente");

// variabile per tenere traccia dell'elenco degli utenti: è un array che contienre una lista di oggetti
var listaUtenti;

chiamataUtentiMain();