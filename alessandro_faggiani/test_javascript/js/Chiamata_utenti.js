"use strict";

function chiamataUtentiMain(){
// genero la chiamata http passandogli un oggetto di configurazione
// con l'url e il verbo http
var chiamataPerUtenti = $.ajax({
  type: "GET",
  url: "https://jsonplaceholder.typicode.com/users",
});

chiamataPerUtenti.done(function (rispostaChiamata) {
  // mi salvo in un oggetto globale i miei utenti cosi da renderli disponibili in tutta l'applicazione
  listaUtenti = rispostaChiamata;

  cardUtenteMain();

});

// creo una fallback per gestire un eventuale errore
chiamataPerUtenti.fail(function (error) {
  alert("Ops! Qualcosa è andato storto!");
});
}