"use strict";

function chiamataPostUtentiMain(){
var urlChiamataPostUtente = "https://jsonplaceholder.typicode.com/posts?userId=" + utenteSelezionato.id; 

// genero la chiamata http passandogli un oggetto di configurazione
// con l'url e il verbo http
var chiamataPerPostUtente = $.ajax({
    type: "GET",
    url: urlChiamataPostUtente,
  });

  // callback di success - promessa mantenuta
  chiamataPerPostUtente.done(function (responseConArticoliUtente) {

    // resetto il contenuto all'interno del contenitore degli articoli sostituendolo con una stringa vuota
    $listaArticoliUtente.html("");
    
    // ciclo sui risultati della chiamata, ossia gli articoli/post dell'utente e per ognuno di essi genero un post
    for (var x = 0; x < responseConArticoliUtente.length; x++) {

      var articoloTmp = responseConArticoliUtente[x];

      var $articoloUtente = $("<li></li>");
      $articoloUtente.html(articoloTmp.title);

      $listaArticoliUtente.append($articoloUtente);
    }
    
  });

  // fallback di errore - promessa non mantenuta
  chiamataPerPostUtente.fail(function(errore) {
    alert("Ops errore nella chiamata dei post dell'utente!");
});
}