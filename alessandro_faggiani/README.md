# Esercizio Javascript e Jquery

*   Concatena, senza duplicati, i seguenti array e ordinali per valore crescente
```javascript
var array1 = [20, 33, 12, 1233];
var array2 = [1, 33, 102, -12];
```
*   Fai il reverse di questa string: “supercalifragilistichespiralidoso”
*   Ordina il seguente array con i giorni della settimana in base al giorno della corrente:
```javascript
var giorniSettimana = ["lunedi", "martedi", "mercoledi", "giovedi", "venerdi", "sabato", "domenica"];
```
*   Ordina il seguente array di oggetti in base all’id:
```javascript
var objectsList = [
 { name: "Pino", id: 66 },
 { name: "Rino", id: 16 },
 { name: "Dino", id: 6 },
 { name: "Lino", id: 96 },
 { name: "Gino", id: 26 }
];

```
*   Crea una funzione che stampi la data corrente in questo formato, es: "martedi 22, febbraio 2021"
*   Crea una funzione che ritorni la differenza tra 2 date in giorni (usare Date.UTC())
*   crea una funzione che data un array di date ritorni la data più alta